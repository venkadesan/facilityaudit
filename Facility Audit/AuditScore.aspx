﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Mainpage.Master" AutoEventWireup="true"
    CodeBehind="AuditScore.aspx.cs" Inherits="Facility_Audit.AuditScore" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="col-md-11">
        <div id="crumbs">
            <ul>
                <li class="active"><a href="#">Audit Scors</a></li>
                <li id="linewbtn" runat="server"><a>
                    <asp:LinkButton ID="bNew" OnClick="bNew_Click" runat="server">New</asp:LinkButton></a></li>
            </ul>
        </div>
    </div>
    <div class="main">
        <div class=" col-md-5 col-xs-offset-4 alert alert-big alert-danger alert-dismissable fade in margin-top-10"
            runat="server" id="divmsg" visible="false">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                ×</button>
            <asp:Label runat="server" ID="lblError"></asp:Label>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="tile">
                    <div class="tile cornered margin-top-10">
                        <div class="tile-body nobottompadding">
                            <div class="row form-horizontal">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="">
                                            Sector :</label>
                                        <div class="col-sm-10">
                                            <asp:DropDownList ID="ddlsector" runat="server" AutoPostBack="True" parsley-validation-minlength="1"
                                                parsley-type="email" parsley-minlength="4" parsley-required="true" parsley-trigger="change"
                                                class="form-control parsley-validated chosen-select" kl_virtual_keyboard_secure_input="on"
                                                OnSelectedIndexChanged="ddlsector_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="">
                                            Audit :</label>
                                        <div class="col-sm-10">
                                            <asp:DropDownList ID="ddlaudit" runat="server" AutoPostBack="True" parsley-validation-minlength="1"
                                                parsley-type="email" parsley-minlength="4" parsley-required="true" parsley-trigger="change"
                                                class="form-control parsley-validated chosen-select" kl_virtual_keyboard_secure_input="on"
                                                OnSelectedIndexChanged="ddlaudit_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <asp:Panel ID="pnlnew" runat="server">
                        <!-- row -->
                        <div class="tile-body nobottompadding">
                            <div class="row form-horizontal">
                                <div class="col-md-6">
                                    <form id="basicvalidations" parsley-validate="" role="form" class="form-horizontal">
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label" for="">
                                            Score Name <span class="red-text">*</span> :
                                        </label>
                                        <div class="col-sm-8">
                                            <asp:TextBox ID="txtscorename" runat="server" parsley-validation-minlength="1" parsley-type="email"
                                                parsley-minlength="4" parsley-required="true" parsley-trigger="change" class="form-control parsley-validated"
                                                kl_virtual_keyboard_secure_input="on"> </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label" for="">
                                            Score <span class="red-text">*</span> :
                                        </label>
                                        <div class="col-sm-8">
                                            <asp:TextBox ID="txtscore" runat="server" parsley-validation-minlength="1" parsley-type="email"
                                                parsley-minlength="4" parsley-required="true" parsley-trigger="change" class="form-control parsley-validated"
                                                kl_virtual_keyboard_secure_input="on"> </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label" for="">
                                            Isdefault <span class="red-text">*</span> :
                                        </label>
                                        <div class="col-sm-8">
                                            <asp:CheckBox ID="chkdefault" runat="server" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label" for="">
                                            Is Notapplicable <span class="red-text">*</span> :
                                        </label>
                                        <div class="col-sm-8">
                                            <asp:CheckBox ID="chknotapplicable" runat="server" />
                                        </div>
                                    </div>
                                    </form>
                                </div>
                                <!-- /tile body -->
                            </div>
                            <!--col-6-->
                        </div>
                        <!-- end row -->
                        <!--col-md-12-->
                        <div class="tile-footer color cyan text-muted">
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="col-md-offset-5">
                                        <asp:Button ID="btnsave" class="btn btn-primary" runat="server" Text="Save" OnClick="btnsave_Click" />
                                        <asp:Button ID="btncncl" class="btn btn-default" runat="server" Text="Cancel" OnClick="btncncl_Click" />
                                    </div>
                                </div>
                                <%-- </form>--%>
                            </div>
                        </div>
                        <!-- end col-md-12 -->
                    </asp:Panel>
                    <!--row-->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="tile-body nopadding margin-top-10">
                                <div class="table-responsive" id="divrep" runat="server">
                                    <asp:Repeater ID="rptrscore" DataSourceID="" runat="server" OnItemCommand="rptrscore_ItemCommand"
                                        OnItemDataBound="rptrscore_ItemDataBound">
                                        <HeaderTemplate>
                                            <table width="100%" border="0" cellspacing="0" class="table table-bordered" cellpadding="0">
                                                <tbody>
                                                    <tr class="color green">
                                                        <th width="5%">
                                                            Sl.No
                                                        </th>
                                                        <th>
                                                            Score Name
                                                        </th>
                                                        <th>
                                                            Score
                                                        </th>
                                                        <th>
                                                            Option
                                                        </th>
                                                    </tr>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr id="rid">
                                                <asp:HiddenField ID="hidcompanyid" runat="server" Value='<%# Eval("scoreid") %>' />
                                                <td>
                                                    <asp:Label ID="Label2" runat="server" Text='<%#(((RepeaterItem)Container).ItemIndex+1).ToString() %>' />
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblcmpname" runat="server" Text='<%# Eval("scorename") %>' />
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblcmpshrtname" runat="server" Text='<%# Eval("score") %>' />
                                                </td>
                                                <td class="actions text-center">
                                                    <asp:LinkButton ID="lnkEdit" runat="server" CommandName="Edit" class="btn btn-primary"
                                                        Text="Edit" CommandArgument='<%# Eval("scoreid") %>'></asp:LinkButton>
                                                    <asp:LinkButton ID="lnkDelete" runat="server" class="btn btn-danger" meta:resourcekey="BtnUserDeleteResource1"
                                                        OnClientClick="if ( !confirm('Are you sure you want to delete this user?')) return false;"
                                                        CommandName="Delete" Text="Delete" CommandArgument='<%# Eval("scoreid") %>'></asp:LinkButton>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </table>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--end row-->
                </div>
            </div>
        </div>
    </div>
</asp:Content>
