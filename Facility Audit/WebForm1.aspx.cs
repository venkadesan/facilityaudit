﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BL;
using System.Data;
using FacilityAudit.Code;

namespace Facility_Audit
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        BLTransaction objbltran = new BLTransaction();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindDetails();
            }
        }
        private void BindDetails()
        {
            try
            {

                DataSet dsreport = new DataSet();
                dsreport = objbltran.GetSbuWiseScore(1, 2016, 1, UserSession.GroupID);
                if (dsreport.Tables.Count > 0 && dsreport.Tables[0].Rows.Count > 0 && dsreport.Tables[1].Rows.Count > 0)
                {
                    rptrsbuwise.DataSource = dsreport.Tables[0];
                    rptrsbuwise.DataBind();
                }
                else
                {
                    rptrsbuwise.DataSource = null;
                    rptrsbuwise.DataBind();
                }
            }
            catch (Exception ex)
            {

            }
        }

        protected void rptrlocationwise_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if ((e.Item.ItemType == ListItemType.Item) ||
                  (e.Item.ItemType == ListItemType.AlternatingItem))
                {
                    Label score = (Label)e.Item.FindControl("score");
                    Image imgicon = (Image)e.Item.FindControl("imgicon");
                    if (score != null && imgicon != null)
                    {
                        decimal dscore = Convert.ToDecimal(score.Text);
                        if (dscore >= 83)
                        {
                            imgicon.ImageUrl = "~/AppThemes/images/smile-icon5.png";
                        }
                        else if (dscore < 82 && dscore >= 75)
                        {
                            imgicon.ImageUrl = "~/AppThemes/images/smile-icon4.png";
                        }
                        else
                        {
                            imgicon.ImageUrl = "~/AppThemes/images/smile-icon1.png";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //NotifyMessages(ex.Message, "error");
            }
        }


    }
}