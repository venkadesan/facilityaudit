﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Mainpage.Master" AutoEventWireup="true"
    CodeBehind="TopLevelReport1.aspx.cs" Inherits="Facility_Audit.TopLevelReport1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>
        function OnSbuClick(sbuid) {

            document.getElementById('<%= hdnsbuid.ClientID%>').value = sbuid;
            document.getElementById('<%= btnsbuwise.ClientID%>').click();
        }

        function OnlocationClick(locationid) {
            document.getElementById('<%= hdnlocationid.ClientID%>').value = locationid;
            document.getElementById('<%= btnlocwise.ClientID%>').click();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main" style="margin-left: 20px;">
        <div class=" col-md-5 col-xs-offset-4 alert alert-big alert-danger alert-dismissable fade in margin-top-10"
            runat="server" id="divmsg" visible="false">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                ×</button>
            <asp:Label runat="server" ID="lblError"></asp:Label>
        </div>
        <div class="row">
            <asp:HiddenField ID="hdnsbuid" Value="0" runat="server" />
            <asp:HiddenField ID="hdnlocationid" Value="0" runat="server" />
            <asp:Button ID="btnsbuwise" runat="server" Text="Button" OnClick="btnsbuwise_Click"
                Style="display: none;" />
            <asp:Button ID="btnlocwise" runat="server" Text="Button" OnClick="btnlocwise_Click"
                Style="display: none;" />
            <div class="row">
                <div class="col-md-12" id="div3" runat="server">
                    <div class="tile-body nopadding margin-top-10">
                        <div class="table-responsive">
                            <asp:Repeater ID="rptroverall" DataSourceID="" runat="server">
                                <HeaderTemplate>
                                    <table class="table table-bordered" cellpadding="0">
                                        <tbody>
                                            <tr>
                                                <th width="5%">
                                                    Sl.No
                                                </th>
                                                <th>
                                                    Head
                                                </th>
                                                <th>
                                                    Score
                                                </th>
                                            </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td>
                                            <asp:Label ID="Label2" runat="server" Text='<%#(((RepeaterItem)Container).ItemIndex+1).ToString() %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="sbu" runat="server" Text='<%# Eval("Head") %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="score" runat="server" Text='<%# Eval("score") %>' />
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12" id="divrep" runat="server">
                    <div class="tile-body nopadding margin-top-10">
                        <div class="table-responsive">
                            <asp:Repeater ID="rptrsbuwise" DataSourceID="" runat="server">
                                <HeaderTemplate>
                                    <table class="table table-bordered" cellpadding="0">
                                        <tbody>
                                            <tr>
                                                <th width="5%">
                                                    Sl.No
                                                </th>
                                                <th>
                                                    SBU
                                                </th>
                                                <th>
                                                    Score
                                                </th>
                                            </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr onclick="OnSbuClick('<%# Eval("sbuid") %>')" style="cursor: pointer;">
                                        <td>
                                            <asp:Label ID="Label2" runat="server" Text='<%#(((RepeaterItem)Container).ItemIndex+1).ToString() %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="sbu" runat="server" Text='<%# Eval("sbu") %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="score" runat="server" Text='<%# Eval("Score") %>' />
                                        </td>
                                        <asp:HiddenField ID="sbuid" Value='<%# Eval("sbuid") %>' runat="server" />
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12" id="div1" runat="server">
                    <div class="tile-body nopadding margin-top-10">
                        <div class="table-responsive">
                            <asp:Repeater ID="rptrlocationwise" DataSourceID="" runat="server" OnItemDataBound="rptrlocationwise_ItemDataBound">
                                <HeaderTemplate>
                                    <table class="table table-bordered" cellpadding="0">
                                        <tbody>
                                            <tr>
                                                <th width="5%">
                                                    Sl.No
                                                </th>
                                                <th>
                                                    Location
                                                </th>
                                                <th>
                                                    Score
                                                </th>
                                                <th>
                                                </th>
                                            </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr onclick="OnlocationClick('<%# Eval("LocationID") %>')" style="cursor: pointer;">
                                        <td>
                                            <asp:Label ID="Label2" runat="server" Text='<%#(((RepeaterItem)Container).ItemIndex+1).ToString() %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="location" runat="server" Text='<%# Eval("LocationName") %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="score" runat="server" Text='<%# Eval("Score") %>' />
                                        </td>
                                        <td>
                                            <asp:Image ID="imgicon" runat="server" />
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <asp:Label ID="lbltotalscore" Style="font-weight: bold; font-size: 13px;" runat="server"></asp:Label>
                <asp:Panel ID="Panel1" runat="server" Height="100%" Width="100%">
                    <div class="col-md-12" id="div2" runat="server">
                        <div class="tile-body nopadding margin-top-10">
                            <div class="table-responsive">
                                <asp:Repeater ID="rptrdatewiserpt" DataSourceID="" runat="server" OnItemCommand="rptrdatewiserpt_ItemCommand">
                                    <HeaderTemplate>
                                        <table width="100%" border="0" cellspacing="0" class="table table-bordered" cellpadding="0">
                                            <tbody>
                                                   <tr class="color green">
                                                    <th width="5%">
                                                        Sl.No
                                                    </th>
                                                    <th>
                                                        Date
                                                    </th>
                                                    <th>
                                                        Location
                                                    </th>
                                                    <th>
                                                        SBU
                                                    </th>
                                                    <th>
                                                        Vertical
                                                    </th>
                                                    <th>
                                                        Auditor
                                                    </th>
                                                    <th>
                                                        Score
                                                    </th>
                                                    <th>
                                                    </th>
                                                </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td>
                                                <asp:Label ID="Label2" runat="server" Text='<%#(((RepeaterItem)Container).ItemIndex+1).ToString() %>' />
                                            </td>
                                            <td>
                                                <asp:Label ID="date" runat="server" Text='<%# Eval("Aduitdate") %>' />
                                            </td>
                                            <td>
                                                <asp:Label ID="LocationName" runat="server" Text='<%# Eval("LocationName") %>' />
                                            </td>
                                            <td>
                                                <asp:Label ID="sbuname" runat="server" Text='<%# Eval("sbuname") %>' />
                                            </td>
                                            <td>
                                                <asp:Label ID="vertical" runat="server" Text='<%# Eval("vertical") %>' />
                                            </td>
                                            <td>
                                                <asp:Label ID="AuditorName" runat="server" Text='<%# Eval("AuditorName") %>' />
                                            </td>
                                            <td>
                                                <asp:Label ID="Score" runat="server" Text='<%# Eval("Score") %>' />
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lnldownload" runat="server" CommandName="Download" class="fa fa-2x fa-download"
                                                    Style="text-decoration: none;"></asp:LinkButton>
                                            </td>
                                            <asp:HiddenField ID="locationid" Value='<%# Eval("locationid") %>' runat="server" />
                                            <asp:HiddenField ID="sbuid" Value='<%# Eval("sbuid") %>' runat="server" />
                                            <asp:HiddenField ID="sectionid" Value='<%# Eval("sectorid") %>' runat="server" />
                                            <asp:HiddenField ID="actualdate" Value='<%# Eval("actualdate") %>' runat="server" />
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <!-- tile body -->
                <!-- /tile body -->
            </div>
        </div>
    </div>
</asp:Content>
