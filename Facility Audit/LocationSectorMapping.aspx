﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Mainpage.Master" AutoEventWireup="true"
    CodeBehind="LocationSectorMapping.aspx.cs" Inherits="Facility_Audit.LocationSectorMapping" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="col-md-11">
        <div id="crumbs">
            <ul>
                <li class="active" style="width:250px;"><a href="#">
                    SECTOR - LOCATION MAPPING</a></li>
            </ul>
        </div>
    </div>
    <div class="main">
        <div class=" col-md-5 col-xs-offset-4 alert alert-big alert-danger alert-dismissable fade in margin-top-10"
            runat="server" id="divmsg" visible="false">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                ×</button>
            <asp:Label runat="server" ID="lblError"></asp:Label>
        </div>
        <div class="row">
            <div class="col-md-12">
                <section class="tile">
                 <section class="tile cornered margin-top-10">
        	    	<div class="tile-body nobottompadding">
                      <div class="row form-horizontal">
                        <div class="col-lg-12">
                            <div class="form-group">
                                   <label class="col-sm-2 control-label" for="">Sector</label>
                               <div class="col-sm-10">
                                   <asp:DropDownList ID="ddlsector" runat="server" 
                                     CssClass="form-control chosen-select" AutoPostBack="True" 
                                       onselectedindexchanged="ddlsector_SelectedIndexChanged">
                                   </asp:DropDownList>
                               </div>
                            </div>
                        </div> 
                      
                        <div class="col-lg-12">
                            <div class="form-group">
                                   <label class="col-sm-2 control-label" for="">Locations</label>
                               <div class="col-sm-10">
                                   <asp:ListBox ID="lstlocation" runat="server" CssClass="form-control chosen-select" SelectionMode="Multiple" >
                                   </asp:ListBox>
                               </div>
                            </div>
                        </div> 
                    
                   <h3>Mapped Locations</h1>
                                        <div class="col-md-12">
                        <div class="tile-body nopadding margin-top-10">
                            <div class="table-responsive">
                                <asp:Repeater ID="rptrmappeddet" DataSourceID="" runat="server">
                                    <HeaderTemplate>
                                        <table width="100%" border="0" cellspacing="0" class="table table-bordered" cellpadding="0">
                                            <tbody>
                                               <tr class="color green">
                                                    <th width="5%">
                                                        Sl.No
                                                    </th>
                                                    <th>
                                                        Company Name
                                                    </th>
                                                    <th>
                                                        Location Name
                                                    </th>
                                                    <th></th>                                                    
                                                </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td>
                                                <asp:Label ID="Label2" runat="server" Text='<%#(((RepeaterItem)Container).ItemIndex+1).ToString() %>' />
                                            </td>
                                            <td>
                                                <asp:Label ID="date" runat="server" Text='<%# Eval("companyname") %>' />
                                            </td>
                                            <td>
                                                <asp:Label ID="LocationName" runat="server" Text='<%# Eval("LocationName") %>' />
                                            </td>                                            
                                            <td>
                                              
                                            <asp:CheckBox ID="chcksave" Checked='<%# Eval("isneeded") %>' runat="server" />
                                 
                                            </td>
                                            <asp:HiddenField ID="hdnlocationid" Value='<%# Eval("locationid") %>' runat="server" />                                          
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </div>
                        </div>
                    </div>         

                    </div>
                      </div>
           
            </section>
            <div class="tile-footer color cyan text-muted">
                <div class="row">
                    <div class="col-md-5">
                        <div class="col-md-offset-5">
                            <asp:Button ID="btnsave" class="btn btn-primary" runat="server" Text="Save" 
                                onclick="btnsave_Click" />
                        </div>
                    </div>
                </div>
            </div>
            </section>
            </div>
        </div>
    </div>
</asp:Content>
