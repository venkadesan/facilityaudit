﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BL;
using System.Data;
using System.IO;
using System.Text;
using Newtonsoft.Json;

using OfficeOpenXml;
using OfficeOpenXml.Drawing;
using OfficeOpenXml.Style;
using System.Drawing;
using OfficeOpenXml.Drawing.Chart;
using System.Configuration;
using FacilityAudit.BLL;
using FacilityAudit.Code;
using System.Globalization;

namespace Facility_Audit
{
    public partial class ManagementReport1 : System.Web.UI.Page
    {

        BLTransaction objbltran = new BLTransaction();
        BLGroupcompany objgrpcmp = new BLGroupcompany();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string dFirstDayOfThisMonth = DateTime.Today.AddDays(-(DateTime.Today.Day - 1)).ToString("MM/dd/yyyy");
                string lastOfThisMonth = DateTime.Today.ToString("MM/dd/yyyy");

                this.txtfrmdate.Text = dFirstDayOfThisMonth;
                this.txttodate.Text = lastOfThisMonth;
                if (!txtfrmdate.Text.Contains("/"))
                {
                    char datesplit = txtfrmdate.Text.ToCharArray()[2];
                    txtfrmdate.Text = txtfrmdate.Text.Replace(datesplit, '/');
                    txttodate.Text = txttodate.Text.Replace(datesplit, '/');
                }
                BindLocation();
                BindDetails();
            }
        }


        private void BindDetails()
        {
            try
            {
                lblError.Text = string.Empty;
                divmsg.Visible = false;
                if (txtfrmdate.Text.Trim() != string.Empty && txttodate.Text.Trim() != string.Empty)
                {
                    int locationid = 0;
                    Common.DDVal(ddlocations, out locationid);
                    DateTime dtFDate = new DateTime();
                    DateTime dtTDate = new DateTime();
                    dtFDate = DateTime.ParseExact(txtfrmdate.Text, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                    dtTDate = DateTime.ParseExact(txttodate.Text, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                    DataSet dsreport = new DataSet();
                    dsreport = objbltran.GetDateWiseReport(locationid, dtFDate, dtTDate);
                    if (dsreport.Tables.Count > 0 && dsreport.Tables[0].Rows.Count > 0)
                    {
                        rptrdatewiserpt.DataSource = dsreport.Tables[0];
                        rptrdatewiserpt.DataBind();
                    }
                    else
                    {
                        rptrdatewiserpt.DataSource = null;
                        rptrdatewiserpt.DataBind();
                        NotifyMessages("No Data Found", "info");
                    }
                }
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, "error");
            }
        }


        protected void ddlmonth_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindDetails();
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, "eror");
            }
        }

        protected void ddlyear_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindDetails();
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, "eror");
            }
        }

        protected void lnksearch_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtfrmdate.Text.Trim() != string.Empty && txttodate.Text.Trim() != string.Empty)
                {
                    BindDetails();
                }
                else
                {
                    NotifyMessages("Plz Select Date Range", "error");
                }
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, "error");
            }
        }

        private void BindLocation()
        {
            try
            {
                DataSet ds = new DataSet();
                ds = objgrpcmp.GetTransactionLocation(0, 0, 0);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddlocations.DataSource = ds;
                    ddlocations.DataTextField = "LocationName";
                    ddlocations.DataValueField = "LocationID";
                    ddlocations.DataBind();
                    this.ddlocations.Items.Insert(0, new ListItem("All", "0"));
                }
                else
                {
                    this.ddlocations.Items.Clear();
                }
            }
            catch (Exception ex)
            {
            }
        }


        public void NotifyMessages(string message, string errortype)
        {
            divmsg.Visible = true;
            lblError.Text = message;
            if (errortype == "info")
            {
                divmsg.Attributes.Add("class", "alert alert-success fade in");
            }
            else if (errortype == "error")
            {
                divmsg.Attributes.Add("class", "alert alert-danger fade in");
            }
        }



        private string GetSiteID(string sitename)
        {
            string[] sitedet = sitename.Split('-');
            return sitedet[sitedet.Length - 1];
        }

        public class Observations
        {
            public string JS_SCM { get; set; }
            public string JS_Others { get; set; }
            public string JS_HR { get; set; }
            public string JS_Complaince { get; set; }
            public string JS_Training { get; set; }
            public string JS_Operation { get; set; }
        }

        protected void rptrdatewiserpt_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "Download")
            {
                try
                {
                    HiddenField hdnlocationid = (HiddenField)e.Item.FindControl("locationid");
                    HiddenField hdnsbuid = (HiddenField)e.Item.FindControl("sbuid");
                    HiddenField hdnsectionid = (HiddenField)e.Item.FindControl("sectionid");
                    HiddenField hdnactualdate = (HiddenField)e.Item.FindControl("actualdate");
                    Label Score = (Label)e.Item.FindControl("Score");
                    Label towername = (Label)e.Item.FindControl("towername");
                    if (hdnlocationid != null && hdnsbuid != null && hdnsectionid != null && hdnactualdate != null && Score != null && towername != null)
                    {
                        string Getscored = Score.Text;
                        //string filename = "DateWiseDataReport.xls";
                        //Response.ContentType = "application/vnd.ms-excel";
                        //Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", filename));
                        //Response.Clear();






                        int locationid = Convert.ToInt32(hdnlocationid.Value);
                        int sbuid = Convert.ToInt32(hdnsbuid.Value);
                        int sectorid = Convert.ToInt32(hdnsectionid.Value);
                        DateTime dtactualdate = Convert.ToDateTime(hdnactualdate.Value);
                        DataSet dsRawdatas = new DataSet();
                        dsRawdatas = objbltran.GetRawDatasDateWiseReport(dtactualdate, locationid, sbuid, sectorid, towername.Text);

                        //GeneratePlot();
                        MakeExcel(dsRawdatas, Getscored);

                        //Font font1 = hssfworkbook.CreateFont();
                        //font1.FontHeightInPoints = 14;
                        ////font1.FontHeight = 14;
                        //font1.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.BOLD;
                        //CellStyle rwstyle = hssfworkbook.CreateCellStyle();
                        //rwstyle.Alignment = HorizontalAlignment.CENTER;
                        //rwstyle.ShrinkToFit = true;
                        //rwstyle.SetFont(font1);


                        //Font font2 = hssfworkbook.CreateFont();
                        //font2.FontHeightInPoints = 11;
                        //font2.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.BOLD;
                        //CellStyle rwstyle2 = hssfworkbook.CreateCellStyle();
                        //rwstyle2.ShrinkToFit = true;
                        //rwstyle2.SetFont(font2);

                        //Font font3 = hssfworkbook.CreateFont();
                        //font3.FontHeightInPoints = 11;
                        //CellStyle style1 = hssfworkbook.CreateCellStyle();
                        //style1.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.TAN.index;
                        //style1.ShrinkToFit = true;
                        //style1.FillPattern = FillPatternType.SOLID_FOREGROUND;

                        //style1.BorderBottom = CellBorderType.THIN;
                        //style1.BottomBorderColor = HSSFColor.BLACK.index;
                        //style1.BorderLeft = CellBorderType.THIN;
                        //style1.LeftBorderColor = HSSFColor.BLACK.index;
                        //style1.BorderRight = CellBorderType.THIN;
                        //style1.RightBorderColor = HSSFColor.BLACK.index;
                        //style1.BorderTop = CellBorderType.THIN;
                        //style1.TopBorderColor = HSSFColor.BLACK.index;

                        //style1.SetFont(font3);

                        //float fEnteirAvg = 0;
                        //int icount = 0;
                        //int iTotalScore = 0;
                        //int iTotalWeightage = 0;

                        //if (dsRawdatas.Tables.Count > 0 && dsRawdatas.Tables[0].Rows.Count > 0)
                        //{
                        //    DataTable dtAudit = dsRawdatas.Tables[0];
                        //    DataTable dtCategory = dsRawdatas.Tables[1];
                        //    DataTable dtRawData = dsRawdatas.Tables[2];
                        //    DataRow drHeader = dsRawdatas.Tables[3].Rows[0];

                        //    //Row rowimage = sheet.CreateRow(0);
                        //    //rowimage.CreateCell(0).SetCellValue(string.Empty);


                        //    //Row rowheader1 = sheet.CreateRow(1);
                        //    //// rowheader1.CreateCell(0).SetCellValue(string.Empty);
                        //    //Cell clheader1 = rowheader1.CreateCell(1);
                        //    //clheader1.SetCellValue("DUSTERS TOTAL SOLUTIONS SERVICES PVT LTD");
                        //    //clheader1.CellStyle = rwstyle;
                        //    //rowheader1.HeightInPoints = 40;



                        //    //HSSFPatriarch patriarch = (HSSFPatriarch)sheet.CreateDrawingPatriarch();
                        //    ////create the anchor
                        //    //HSSFClientAnchor anchor;
                        //    //anchor = new HSSFClientAnchor(0, 0, 0, 0, (short)0, 0, (short)0, 0);

                        //    //////anchor = new HSSFClientAnchor(20, 0, 35, 15, 0, 0, 1, 3);
                        //    ////anchor = new HSSFClientAnchor();
                        //    ////anchor.Col1 = 1;
                        //    ////anchor.Col2 = 1;
                        //    //anchor.AnchorType = (short)AnchorType.DONT_MOVE_AND_RESIZE;
                        //    ////anchor.AnchorType = 3;
                        //    ////load the picture and get the picture index in the workbook
                        //    //HSSFPicture picture = (HSSFPicture)patriarch.CreatePicture(anchor, LoadImage(Server.MapPath("AppThemes/images/dts.png"), hssfworkbook));
                        //    ////Reset the image to the original size.
                        //    //picture.Resize();
                        //    //////picture.LineStyle = HSSFPicture.LINESTYLE_DASHDOTGEL;



                        //    Row rowclient = sheet.CreateRow(7);
                        //    rowclient.CreateCell(0).SetCellValue(string.Empty);
                        //    rowclient.CreateCell(1).SetCellValue("Name of the Client");
                        //    rowclient.CreateCell(2).SetCellValue(drHeader["clientname"].ToString());
                        //    rowclient.CreateCell(3).SetCellValue(string.Empty);
                        //    rowclient.GetCell(1).CellStyle = style1;
                        //    rowclient.GetCell(2).CellStyle = style1;
                        //    rowclient.GetCell(3).CellStyle = style1;

                        //    Row rowsite = sheet.CreateRow(8);
                        //    rowsite.CreateCell(0).SetCellValue(string.Empty);
                        //    rowsite.CreateCell(1).SetCellValue("Name of the site");
                        //    rowsite.CreateCell(2).SetCellValue(drHeader["locsitename"].ToString());
                        //    rowsite.CreateCell(3).SetCellValue(string.Empty);
                        //    rowsite.GetCell(1).CellStyle = style1;
                        //    rowsite.GetCell(2).CellStyle = style1;
                        //    rowsite.GetCell(3).CellStyle = style1;

                        //    Row rowssa = sheet.CreateRow(9);
                        //    rowssa.CreateCell(0).SetCellValue(string.Empty);
                        //    rowssa.CreateCell(1).SetCellValue("Site ID");
                        //    rowssa.CreateCell(2).SetCellValue(GetSiteID(drHeader["locsitename"].ToString()));
                        //    rowssa.GetCell(1).CellStyle = style1;
                        //    rowssa.GetCell(2).CellStyle = style1;
                        //    rowssa.CreateCell(3).SetCellValue(string.Empty);
                        //    rowssa.GetCell(3).CellStyle = style1;

                        //    Row rowperson = sheet.CreateRow(10);
                        //    rowperson.CreateCell(0).SetCellValue(string.Empty);
                        //    rowperson.CreateCell(1).SetCellValue("Name of the Client person");
                        //    rowperson.CreateCell(2).SetCellValue(drHeader["clientperson"].ToString());
                        //    rowperson.GetCell(1).CellStyle = style1;
                        //    rowperson.GetCell(2).CellStyle = style1;
                        //    rowperson.CreateCell(3).SetCellValue(string.Empty);
                        //    rowperson.GetCell(3).CellStyle = style1;


                        //    Row rowsbu = sheet.CreateRow(11);
                        //    rowsbu.CreateCell(0).SetCellValue(string.Empty);
                        //    rowsbu.CreateCell(1).SetCellValue("Name of the SBU");
                        //    rowsbu.CreateCell(2).SetCellValue(drHeader["sbuname"].ToString());
                        //    rowsbu.GetCell(1).CellStyle = style1;
                        //    rowsbu.GetCell(2).CellStyle = style1;
                        //    rowsbu.CreateCell(3).SetCellValue(string.Empty);
                        //    rowsbu.GetCell(3).CellStyle = style1;


                        //    Row rowaom = sheet.CreateRow(12);
                        //    rowaom.CreateCell(0).SetCellValue(string.Empty);
                        //    rowaom.CreateCell(1).SetCellValue("Name of the OM / AOM");
                        //    rowaom.CreateCell(2).SetCellValue(drHeader["aom"].ToString());
                        //    rowaom.GetCell(1).CellStyle = style1;
                        //    rowaom.GetCell(2).CellStyle = style1;
                        //    rowaom.CreateCell(3).SetCellValue(string.Empty);
                        //    rowaom.GetCell(3).CellStyle = style1;


                        //    Row rowauditor = sheet.CreateRow(13);
                        //    rowauditor.CreateCell(0).SetCellValue(string.Empty);
                        //    rowauditor.CreateCell(1).SetCellValue("Name of the Auditor");
                        //    rowauditor.CreateCell(2).SetCellValue(drHeader["auditor"].ToString());
                        //    rowauditor.GetCell(1).CellStyle = style1;
                        //    rowauditor.GetCell(2).CellStyle = style1;
                        //    rowauditor.CreateCell(3).SetCellValue(string.Empty);
                        //    rowauditor.GetCell(3).CellStyle = style1;

                        //    Row rowAuditee = sheet.CreateRow(14);
                        //    rowAuditee.CreateCell(0).SetCellValue(string.Empty);
                        //    rowAuditee.CreateCell(1).SetCellValue("Name of the Auditee");
                        //    rowAuditee.CreateCell(2).SetCellValue(drHeader["auditee"].ToString());
                        //    rowAuditee.GetCell(1).CellStyle = style1;
                        //    rowAuditee.GetCell(2).CellStyle = style1;
                        //    rowAuditee.CreateCell(3).SetCellValue(string.Empty);
                        //    rowAuditee.GetCell(3).CellStyle = style1;


                        //    Row rowlastauitdate = sheet.CreateRow(15);
                        //    rowlastauitdate.CreateCell(0).SetCellValue(string.Empty);
                        //    rowlastauitdate.CreateCell(1).SetCellValue("Last audit date");
                        //    rowlastauitdate.CreateCell(2).SetCellValue(drHeader["displastauditdate"].ToString());
                        //    rowlastauitdate.CreateCell(3).SetCellValue("Current Audit Score:");
                        //    rowlastauitdate.GetCell(1).CellStyle = style1;
                        //    rowlastauitdate.GetCell(2).CellStyle = style1;
                        //    rowlastauitdate.GetCell(3).CellStyle = style1;


                        //    Row rowauitdate = sheet.CreateRow(16);
                        //    rowauitdate.CreateCell(0).SetCellValue(string.Empty);
                        //    rowauitdate.CreateCell(1).SetCellValue("Current audit date");
                        //    rowauitdate.CreateCell(2).SetCellValue(drHeader["dispauditdate"].ToString());
                        //    rowauitdate.CreateCell(3).SetCellValue("Last Audit Score :");
                        //    rowauitdate.GetCell(1).CellStyle = style1;
                        //    rowauitdate.GetCell(2).CellStyle = style1;
                        //    rowauitdate.GetCell(3).CellStyle = style1;


                        //    sheet.AutoSizeColumn(0);
                        //    sheet.AutoSizeColumn(1);
                        //    sheet.AutoSizeColumn(2);
                        //    sheet.AutoSizeColumn(3);
                        //    sheet.AutoSizeColumn(4);
                        //    sheet.AutoSizeColumn(5);
                        //    sheet.AutoSizeColumn(6);
                        //    sheet.AutoSizeColumn(7);
                        //    icount = 17;

                        //    for (int iauditcount = 0; iauditcount < dtAudit.Rows.Count; iauditcount++)
                        //    {
                        //        DataRow draudit = dtAudit.Rows[iauditcount];
                        //        int auditid = Convert.ToInt32(draudit["auditid"].ToString());
                        //        string auditname = draudit["auditname"].ToString();
                        //        Row rowAudit = sheet.CreateRow(icount);
                        //        rowAudit.CreateCell(0).SetCellValue(string.Empty);
                        //        rowAudit.GetCell(0).CellStyle = rwstyle;
                        //        Cell clAudit = rowAudit.CreateCell(1);
                        //        clAudit.SetCellValue(auditname);
                        //        clAudit.CellStyle = rwstyle;

                        //        //sheet.AddMergedRegion(new CellRangeAddress(icount, icount, 0, 4));

                        //        icount++;

                        //        Row rowheading = sheet.CreateRow(icount);
                        //        Cell clslno = rowheading.CreateCell(0);
                        //        clslno.SetCellValue("Sl.No");
                        //        Cell clheading1 = rowheading.CreateCell(1);
                        //        clheading1.SetCellValue("Observations");
                        //        //Cell clheading2 = rowheading.CreateCell(2);
                        //        //clheading2.SetCellValue("Weightage");
                        //        Cell clheading2 = rowheading.CreateCell(2);
                        //        clheading2.SetCellValue("Maximum Score");
                        //        Cell clheading3 = rowheading.CreateCell(3);
                        //        clheading3.SetCellValue("Score Obtained");
                        //        Cell clheading4 = rowheading.CreateCell(4);
                        //        clheading4.SetCellValue("Auditor remarks");
                        //        Cell clheading5 = rowheading.CreateCell(5);
                        //        clheading5.SetCellValue("Closure status & Remarks by SBU");

                        //        Cell climgheading6 = rowheading.CreateCell(6);
                        //        climgheading6.SetCellValue("Image");

                        //        clheading1.CellStyle = style1;
                        //        clheading2.CellStyle = style1;
                        //        clheading3.CellStyle = style1;
                        //        clheading4.CellStyle = style1;
                        //        clheading5.CellStyle = style1;
                        //        climgheading6.CellStyle = style1;
                        //        clslno.CellStyle = style1;

                        //        icount++;

                        //        int iscore = 0;
                        //        int iweightage = 0;
                        //        int iSingleAvg = 0;
                        //        int iTotalAvg = 0;
                        //        DataRow[] drCategory = dtCategory.Select("auditid=" + auditid);
                        //        for (int icategroryCount = 0; icategroryCount < drCategory.Length; icategroryCount++)
                        //        {
                        //            int categoryid = Convert.ToInt32(drCategory[icategroryCount]["categoryid"].ToString());
                        //            string categoryname = drCategory[icategroryCount]["categoryname"].ToString();

                        //            Row rowCategory = sheet.CreateRow(icount);
                        //            rowCategory.CreateCell(0).SetCellValue(icategroryCount + 1);
                        //            Cell clCategory = rowCategory.CreateCell(1);
                        //            clCategory.SetCellValue(categoryname);
                        //            clCategory.CellStyle = rwstyle2;
                        //            rowCategory.GetCell(0).CellStyle = rwstyle2;
                        //            icount++;

                        //            DataRow[] drRawDatas = dtRawData.Select("categoryid=" + categoryid);

                        //            int iCategoryScore = 0;
                        //            int iCategoryTotal = 0;

                        //            for (int iRawDatacount = 0; iRawDatacount < drRawDatas.Length; iRawDatacount++)
                        //            {
                        //                string question = drRawDatas[iRawDatacount]["auditqname"].ToString();
                        //                string remarks = drRawDatas[iRawDatacount]["remarks"].ToString();
                        //                int score = Convert.ToInt32(drRawDatas[iRawDatacount]["score"].ToString());
                        //                int weightage = Convert.ToInt32(drRawDatas[iRawDatacount]["weightage"].ToString());
                        //                string scorename = drRawDatas[iRawDatacount]["scorename"].ToString();
                        //                string imgFile = drRawDatas[iRawDatacount]["uploadfilename"].ToString();

                        //                int tScore = 0;

                        //                if (score == -1)
                        //                {
                        //                    score = 0;
                        //                    tScore = -1;
                        //                    weightage = 0;
                        //                }

                        //                iscore += score;
                        //                iweightage += weightage;

                        //                iCategoryScore += score;
                        //                iCategoryTotal += weightage;

                        //                iTotalScore += score;
                        //                iTotalWeightage += weightage;

                        //                iSingleAvg = score * weightage;
                        //                iTotalAvg += iSingleAvg;

                        //                Row rowRawData = sheet.CreateRow(icount);

                        //                Cell clrwSlNo = rowRawData.CreateCell(0);
                        //                clrwSlNo.SetCellValue(iRawDatacount + 1);

                        //                Cell clRawData1 = rowRawData.CreateCell(1);
                        //                clRawData1.SetCellValue(question);


                        //                //Cell clRawData2 = rowRawData.CreateCell(2);
                        //                //clRawData2.SetCellValue(weightage);

                        //                CellStyle stylecell = hssfworkbook.CreateCellStyle();
                        //                //stylecell.ShrinkToFit = true;
                        //                stylecell.Alignment = HorizontalAlignment.RIGHT;

                        //                Cell clRawData2 = rowRawData.CreateCell(2);
                        //                if (tScore == 0)
                        //                {
                        //                    clRawData2.SetCellValue(" " + score);
                        //                    //clRawData2.CellStyle = stylecell;
                        //                }
                        //                else
                        //                {
                        //                    clRawData2.SetCellValue(scorename);
                        //                }
                        //                clRawData2.SetCellType(CellType.STRING);
                        //                //clRawData2.CellStyle = stylecell;

                        //                Cell clRawData3 = rowRawData.CreateCell(3);
                        //                clRawData3.SetCellValue(" " + score);
                        //                if (tScore == -1)
                        //                {
                        //                    clRawData3.SetCellValue(scorename);

                        //                }
                        //                clRawData3.SetCellType(CellType.STRING);
                        //                //clRawData3.CellStyle = stylecell;

                        //                Cell clRawData4 = rowRawData.CreateCell(4);
                        //                clRawData4.SetCellValue(remarks);

                        //                Cell clRawData5 = rowRawData.CreateCell(5);
                        //                clRawData5.SetCellValue(string.Empty);

                        //                if (imgFile.Trim() != string.Empty)
                        //                {
                        //                    Cell clImgRow6 = rowRawData.CreateCell(6);
                        //                    clImgRow6.SetCellValue(imgFile);
                        //                    HSSFHyperlink link = new HSSFHyperlink(HyperlinkType.URL);
                        //                    link.Address = ("http://ifazility.com/facilityaudit/auditimage/" + imgFile);
                        //                    clImgRow6.Hyperlink = (link);
                        //                }


                        //                icount++;

                        //            }

                        //            Row rowCateTotal = sheet.CreateRow(icount);

                        //            Cell clCateTotal1 = rowCateTotal.CreateCell(0);
                        //            clCateTotal1.SetCellValue(string.Empty);

                        //            Cell clCateTotal2 = rowCateTotal.CreateCell(1);
                        //            clCateTotal2.SetCellValue(categoryname);

                        //            //Cell clCateTotal3 = rowCateTotal.CreateCell(2);
                        //            //clCateTotal3.SetCellValue(iweightage);

                        //            Cell clCateTotal3 = rowCateTotal.CreateCell(2);
                        //            clCateTotal3.SetCellValue(iCategoryTotal);

                        //            Cell clCateTotal4 = rowCateTotal.CreateCell(3);
                        //            clCateTotal4.SetCellValue(iCategoryScore);

                        //            Cell clCateTotal5 = rowCateTotal.CreateCell(4);
                        //            clCateTotal5.SetCellValue(string.Empty);

                        //            Cell clCateTotal6 = rowCateTotal.CreateCell(5);
                        //            clCateTotal6.SetCellValue(string.Empty);

                        //            clCateTotal1.CellStyle = style1;
                        //            clCateTotal2.CellStyle = style1;
                        //            clCateTotal3.CellStyle = style1;
                        //            clCateTotal4.CellStyle = style1;
                        //            clCateTotal5.CellStyle = style1;
                        //            clCateTotal6.CellStyle = style1;

                        //            icount++;


                        //        }

                        //        float iScoredAvg = 0;
                        //        iScoredAvg = ((float)(iTotalAvg * 100 / iweightage));

                        //        fEnteirAvg += iScoredAvg;

                        //        Row rowtotal = sheet.CreateRow(icount);

                        //        Cell cltotal1 = rowtotal.CreateCell(0);
                        //        cltotal1.SetCellValue(string.Empty);

                        //        Cell cltotal2 = rowtotal.CreateCell(1);
                        //        cltotal2.SetCellValue("Total Score for Documents & checklists");

                        //        //Cell cltotal3 = rowtotal.CreateCell(2);
                        //        //cltotal3.SetCellValue(iweightage);

                        //        Cell cltotal3 = rowtotal.CreateCell(2);
                        //        cltotal3.SetCellValue(iweightage);

                        //        Cell cltotal4 = rowtotal.CreateCell(3);
                        //        cltotal4.SetCellValue(iscore);

                        //        Cell cltotal5 = rowtotal.CreateCell(4);
                        //        cltotal5.SetCellValue(string.Empty);

                        //        Cell cltotal6 = rowtotal.CreateCell(5);
                        //        cltotal6.SetCellValue(string.Empty);
                        //        icount++;

                        //        Row rowavg = sheet.CreateRow(icount);

                        //        Cell clavg1 = rowavg.CreateCell(0);
                        //        cltotal1.SetCellValue(string.Empty);

                        //        Cell clavg2 = rowavg.CreateCell(1);
                        //        clavg2.SetCellValue("Percentage (%)");

                        //        Cell clavg3 = rowavg.CreateCell(2);
                        //        clavg3.SetCellValue(string.Empty);

                        //        Cell clavg4 = rowavg.CreateCell(3);
                        //        clavg4.SetCellValue(iScoredAvg + "%");

                        //        Cell clavg5 = rowavg.CreateCell(4);
                        //        clavg5.SetCellValue(string.Empty);

                        //        Cell clavg6 = rowavg.CreateCell(5);
                        //        clavg6.SetCellValue(string.Empty);


                        //        cltotal1.CellStyle = style1;
                        //        cltotal2.CellStyle = style1;
                        //        cltotal3.CellStyle = style1;
                        //        cltotal4.CellStyle = style1;
                        //        cltotal5.CellStyle = style1;
                        //        cltotal6.CellStyle = style1;


                        //        clavg1.CellStyle = style1;
                        //        clavg2.CellStyle = style1;
                        //        clavg3.CellStyle = style1;
                        //        clavg4.CellStyle = style1;
                        //        clavg5.CellStyle = style1;
                        //        clavg6.CellStyle = style1;


                        //        icount++;
                        //    }
                        //}

                        //float tAvg = fEnteirAvg / (dsRawdatas.Tables[0].Rows.Count);


                        //Row rowgrndTotal = sheet.CreateRow(icount);

                        //Cell clgrndTotal1 = rowgrndTotal.CreateCell(0);
                        //clgrndTotal1.SetCellValue(string.Empty);

                        //Cell clgrndTotal2 = rowgrndTotal.CreateCell(1);
                        //clgrndTotal2.SetCellValue("GRAND TOTAL SCORE");

                        ////Cell clgrndTotal3 = rowgrndTotal.CreateCell(2);
                        ////clgrndTotal3.SetCellValue(iTotalWeightage);

                        //Cell clgrndTotal3 = rowgrndTotal.CreateCell(2);
                        //clgrndTotal3.SetCellValue(iTotalWeightage);

                        //Cell clgrndTotal4 = rowgrndTotal.CreateCell(3);
                        //clgrndTotal4.SetCellValue(iTotalScore);

                        //Cell clgrndTotal5 = rowgrndTotal.CreateCell(4);
                        //clgrndTotal5.SetCellValue(string.Empty);

                        //Cell clgrndTotal6 = rowgrndTotal.CreateCell(5);
                        //clgrndTotal6.SetCellValue(string.Empty);

                        //icount++;

                        //Row rowgrndavg = sheet.CreateRow(icount);

                        //Cell clgrndavg1 = rowgrndavg.CreateCell(0);
                        //clgrndavg1.SetCellValue(string.Empty);


                        //Cell clgrndavg2 = rowgrndavg.CreateCell(1);
                        //clgrndavg2.SetCellValue("Percentage (%)");

                        ////Cell clgrndavg3 = rowgrndavg.CreateCell(2);
                        ////clgrndavg3.SetCellValue(string.Empty);

                        //Cell clgrndavg3 = rowgrndavg.CreateCell(2);
                        //clgrndavg3.SetCellValue(string.Empty);

                        ////Cell clgrndavg5 = rowgrndavg.CreateCell(4);
                        ////clgrndavg5.SetCellValue(tAvg + "%");

                        //Cell clgrndavg4 = rowgrndavg.CreateCell(3);
                        //clgrndavg4.SetCellValue(Getscored + "%");

                        //Cell clgrndavg5 = rowgrndavg.CreateCell(4);
                        //clgrndavg5.SetCellValue(string.Empty);

                        //Cell clgrndavg6 = rowgrndavg.CreateCell(5);
                        //clgrndavg6.SetCellValue(string.Empty);

                        //icount++;

                        //if (dsRawdatas.Tables[3].Rows.Count > 0)
                        //{
                        //    Row rowgrndfollowupdate = sheet.CreateRow(icount);

                        //    Cell clgrndfollowupdate1 = rowgrndfollowupdate.CreateCell(0);
                        //    clgrndfollowupdate1.SetCellValue(string.Empty);

                        //    Cell clgrndfollowupdate2 = rowgrndfollowupdate.CreateCell(1);
                        //    clgrndfollowupdate2.SetCellValue("Follow-Up Date");

                        //    //Cell clgrndfollowupdate3 = rowgrndfollowupdate.CreateCell(2);
                        //    //clgrndfollowupdate3.SetCellValue(string.Empty);

                        //    Cell clgrndfollowupdate3 = rowgrndfollowupdate.CreateCell(2);
                        //    clgrndfollowupdate3.SetCellValue(dsRawdatas.Tables[3].Rows[0]["dispFollowUpDate"].ToString());

                        //    //Cell clgrndfollowupdate5 = rowgrndfollowupdate.CreateCell(4);
                        //    //clgrndfollowupdate5.SetCellValue(tfollowupdate + "%");

                        //    Cell clgrndfollowupdate4 = rowgrndfollowupdate.CreateCell(3);
                        //    clgrndfollowupdate4.SetCellValue(string.Empty);

                        //    Cell clgrndfollowupdate5 = rowgrndfollowupdate.CreateCell(4);
                        //    clgrndfollowupdate5.SetCellValue(string.Empty);

                        //    Cell clgrndfollowupdate6 = rowgrndfollowupdate.CreateCell(5);
                        //    clgrndfollowupdate6.SetCellValue(string.Empty);
                        //}

                        //decimal scoreGot = Convert.ToDecimal(Getscored);
                        //CellStyle scoreColor = hssfworkbook.CreateCellStyle();
                        //if (scoreGot >= 83)
                        //{
                        //    scoreColor.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.GREEN.index;
                        //    scoreColor.FillPattern = FillPatternType.SOLID_FOREGROUND;
                        //    clgrndavg4.CellStyle = scoreColor;
                        //}
                        //else
                        //{
                        //    scoreColor.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.ORANGE.index;
                        //    scoreColor.FillPattern = FillPatternType.SOLID_FOREGROUND;
                        //    clgrndavg4.CellStyle = scoreColor;
                        //}



                        //sheet.AutoSizeColumn(0);
                        //sheet.AutoSizeColumn(1);
                        //sheet.AutoSizeColumn(2);
                        //sheet.AutoSizeColumn(3);
                        //sheet.AutoSizeColumn(4);
                        //sheet.AutoSizeColumn(5);
                        //sheet.AutoSizeColumn(6);


                        //Response.BinaryWrite(WriteToStream().GetBuffer());
                        //Response.End();
                    }
                }
                catch (Exception ex)
                {
                    NotifyMessages(ex.Message, "error");
                }

            }
        }


        private void GenerateExcel(DataSet dsRawdatas, string Getscored)
        {
            try
            {
                StringBuilder sDatas = new StringBuilder();

                float fEnteirAvg = 0;
                int iTotalScore = 0;
                int iTotalWeightage = 0;
                int iMaxScore = 0;




                if (dsRawdatas.Tables.Count > 0 && dsRawdatas.Tables[0].Rows.Count > 0)
                {
                    sDatas.Append("<table width='100%' cellspacing='0' cellpadding='2' border = '1'>");
                    DataTable dtAudit = dsRawdatas.Tables[0];
                    DataTable dtCategory = dsRawdatas.Tables[1];
                    DataTable dtRawData = dsRawdatas.Tables[2];
                    DataRow drHeader = dsRawdatas.Tables[3].Rows[0];
                    sDatas.Append("<tr><td rowspan='5' style='border: none;' ><img style='height: 54px;width: 100px;' border='0' alt='Dusters' src='http://ifazility.com/facilityaudit/AppThemes/images/JLL%20New.png'></td><td rowspan='4' style='font-size: 24px;border: none;font-weight:bold;' align='center' colspan='6'>DUSTERS TOTAL SOLUTIONS SERVICES PVT LTD</td>");
                    sDatas.Append("<tr></tr>");
                    sDatas.Append("<tr></tr>");
                    sDatas.Append("<tr></tr>");
                    sDatas.Append("<tr></tr>");
                    sDatas.Append("<tr><td colspan='2'>Name of the Client</td><td colspan='3'>" + drHeader["clientname"].ToString() + "</td><td colspan='2'></td></tr>");
                    sDatas.Append("<tr><td colspan='2'>Name of the Site</td><td colspan='3'>" + drHeader["locsitename"].ToString() + "</td><td colspan='2'></td></tr>");
                    sDatas.Append("<tr><td colspan='2'>Site ID</td><td colspan='3'>" + GetSiteID(drHeader["locsitename"].ToString()) + "</td><td colspan='2'></td></tr>");
                    sDatas.Append("<tr><td colspan='2'>Name of the Client person</td><td colspan='3'>" + drHeader["clientperson"].ToString() + "</td><td colspan='2'></td></tr>");
                    sDatas.Append("<tr><td colspan='2'>Name of the SBU</td><td colspan='3'>" + drHeader["sbuname"].ToString() + "</td><td colspan='2'></td></tr>");
                    sDatas.Append("<tr><td colspan='2'>Name of the OM / AOM</td><td colspan='3'>" + drHeader["aom"].ToString() + "</td><td colspan='2'></td></tr>");
                    sDatas.Append("<tr><td colspan='2'>Name of the Auditor</td><td colspan='3'>" + drHeader["auditor"].ToString() + "</td><td colspan='2'></td></tr>");
                    sDatas.Append("<tr><td colspan='2'>Name of the Auditee</td><td colspan='3'>" + drHeader["auditee"].ToString() + "</td><td colspan='2'></td></tr>");
                    sDatas.Append("<tr><td colspan='2'>Last audit date</td><td align='left' colspan='3'>" + drHeader["displastauditdate"].ToString() + "</td><td colspan='2'>Last Audit Score</td></tr>");
                    sDatas.Append("<tr><td colspan='2'>Current audit date</td><td align='left' colspan='3'>" + drHeader["dispauditdate"].ToString() + "</td><td>Current Audit Score</td><td>" + Getscored + "%</td></tr>");

                    for (int iauditcount = 0; iauditcount < dtAudit.Rows.Count; iauditcount++)
                    {
                        DataRow draudit = dtAudit.Rows[iauditcount];
                        int auditid = Convert.ToInt32(draudit["auditid"].ToString());
                        string auditname = draudit["auditname"].ToString();
                        sDatas.Append("<tr><td align='center' style='font-size: 22px;' colspan='7'>" + auditname + "</td></tr>");
                        sDatas.Append("<tr><th style = 'background-color: #FCB133;color:#ffffff'>Sl.No</th><th style = 'background-color: #FCB133;color:#ffffff'>Observations</th><th style = 'background-color: #FCB133;color:#ffffff;width: 5%;'>Maximum <br/> Score</th><th style = 'background-color: #FCB133;color:#ffffff;width: 5%;'>Score <br/> Obtained</th><th style = 'background-color: #FCB133;color:#ffffff;width: 30%;'>Auditor Remarks</th><th style = 'background-color: #FCB133;color:#ffffff'>Remarks by SBU </th><th style = 'background-color: #FCB133;color:#ffffff'>Image</th></tr>");
                        int iscore = 0;
                        int iweightage = 0;
                        int iSingleAvg = 0;
                        int iTotalAvg = 0;
                        int iAuditMax = 0;
                        DataRow[] drCategory = dtCategory.Select("auditid=" + auditid);
                        for (int icategroryCount = 0; icategroryCount < drCategory.Length; icategroryCount++)
                        {
                            int categoryid = Convert.ToInt32(drCategory[icategroryCount]["categoryid"].ToString());
                            string categoryname = drCategory[icategroryCount]["categoryname"].ToString();
                            sDatas.Append("<tr><td align='center'>" + (icategroryCount + 1) + "</td><td style='font-size: 18px;' colspan='6'>" + categoryname + "</td></tr>");
                            DataRow[] drRawDatas = dtRawData.Select("categoryid=" + categoryid);

                            int iCategoryScore = 0;
                            int iCategoryTotal = 0;
                            int iCatMax = 0;

                            for (int iRawDatacount = 0; iRawDatacount < drRawDatas.Length; iRawDatacount++)
                            {
                                string question = drRawDatas[iRawDatacount]["auditqname"].ToString();
                                string remarks = drRawDatas[iRawDatacount]["remarks"].ToString();
                                int score = Convert.ToInt32(drRawDatas[iRawDatacount]["score"].ToString());
                                int weightage = Convert.ToInt32(drRawDatas[iRawDatacount]["weightage"].ToString());
                                string scorename = drRawDatas[iRawDatacount]["scorename"].ToString();
                                string imgFile = drRawDatas[iRawDatacount]["uploadfilename"].ToString();
                                string filelink = string.Empty;
                                if (imgFile.Trim() != string.Empty)
                                {
                                    string[] imgfiles = imgFile.Split('#');
                                    foreach (string files in imgfiles)
                                    {
                                        if (files.Trim() != string.Empty)
                                        {
                                            imgFile = "http://ifazility.com/facilityaudit/auditimage/" + files;
                                            filelink += "<a href='" + imgFile + "'>" + imgFile + "</a><br/>";
                                        }
                                    }
                                }

                                int tScore = 0;
                                int MaxScore = 0;

                                if (score == -1)
                                {
                                    score = 0;
                                    tScore = -1;
                                    weightage = 0;
                                    MaxScore = 0;
                                }
                                else
                                {
                                    MaxScore = 1;
                                }

                                iscore += score;
                                iweightage += weightage;

                                iCategoryScore += score;
                                iCategoryTotal += weightage;

                                iTotalScore += score;
                                iTotalWeightage += weightage;

                                iSingleAvg = score * weightage;
                                iTotalAvg += iSingleAvg;

                                iMaxScore += MaxScore;
                                iCatMax += MaxScore;
                                iAuditMax += MaxScore;

                                string _score = string.Empty;
                                string _maxscore = string.Empty;
                                if (tScore == 0)
                                {
                                    _score = " " + score;
                                    _maxscore = " " + MaxScore;
                                }
                                else
                                {
                                    _score = scorename;
                                    _maxscore = scorename;
                                }
                                sDatas.Append("<tr><td align='center'>" + (iRawDatacount + 1).ToString() + "</td><td>" + question + "</td><td style='width: 5%;' align='center'>" + _maxscore + "</td><td  style='width: 5%;'  align='center'>" + _score + "</td><td>" + remarks + "</td><td style='width: 30%;'></td><td>" + filelink + "</td></tr>");
                            }

                            float _CateWisTot = 0;
                            _CateWisTot = ((float)(iCategoryScore * 100 / iCategoryTotal));




                            string sCatNA = string.Empty;
                            string sCatMaxNA = string.Empty;
                            sCatNA = iCatMax.ToString();
                            sCatMaxNA = iCategoryScore.ToString();
                            if (iCatMax == 0)
                            {
                                sCatNA = "N/A";
                            }
                            if (iCategoryScore == 0)
                            {
                                sCatMaxNA = "N/A";
                            }
                            sDatas.Append("<tr style='font-weight:bold;font-size: 18px;'><td colspan='2'>" + categoryname + "</td><td align='center'>" + sCatNA + "</td><td align='center'>" + sCatMaxNA + "</td><td></td><td></td><td></td></tr>");
                        }
                        float iScoredAvg = 0;
                        iScoredAvg = ((float)(iTotalAvg * 100 / iweightage));
                        fEnteirAvg += iScoredAvg;

                        sDatas.Append("<tr style='font-weight:bold;font-size: 18px;'><td colspan='2'>Total Score for " + auditname + "</td><td align='center'>" + iAuditMax + "</td><td align='center'>" + iscore + "</td><td></td><td></td><td></td></tr>");
                        sDatas.Append("<tr style='font-weight:bold;font-size: 18px;'><td colspan='2'>Percentage (%)</td><td></td><td align='center'>" + iScoredAvg + "%" + "</td><td></td><td></td><td></td></tr>");
                    }
                    float tAvg = fEnteirAvg / (dsRawdatas.Tables[0].Rows.Count);

                    sDatas.Append("<tr style='font-weight:bold;font-size: 18px;'><td colspan='2'>GRAND TOTAL SCORE</td><td align='center'>" + iMaxScore + "</td><td align='center'>" + iTotalScore + "</td><td></td><td></td><td></td></tr>");

                    decimal scoreGot = Convert.ToDecimal(Getscored);
                    string setColour = string.Empty;
                    if (scoreGot >= 83)
                    {
                        setColour = "<td style='background-color: #00FF00;' align='center'>" + Getscored + "%" + "</td>";
                    }
                    else
                    {
                        setColour = "<td style='background-color: #FFBE00;' align='center'>" + Getscored + "%" + "</td>";
                    }
                    sDatas.Append("<tr style='font-weight:bold;font-size: 18px;'><td colspan='2'>Percentage (%)</td><td></td>" + setColour + "<td></td><td></td><td></td></tr>");
                    if (dsRawdatas.Tables[3].Rows.Count > 0)
                    {
                        sDatas.Append("<tr style='font-weight:bold;font-size: 18px;'><td colspan='2'>Follow-Up Date</td><td></td><td>" + dsRawdatas.Tables[3].Rows[0]["dispFollowUpDate"].ToString() + "</td><td></td><td></td><td></td></tr>");
                    }


                    string observations = drHeader["observation"].ToString();
                    string clientremarks = drHeader["feedback"].ToString();
                    observations = observations.Replace('[', ' ');
                    observations = observations.Replace(']', ' ');
                    try
                    {
                        Observations objobservation = JsonConvert.DeserializeObject<Observations>(observations);
                        //                string observationdet = "1. Operations : " + objobservation.JS_Operation + "break" +
                        //"2. Training  : " + objobservation.JS_Training + " break  " +
                        //"3. SCM  : " + objobservation.JS_SCM + " break " +
                        //"4. Compliance  : " + objobservation.JS_Complaince + "break" +
                        //"5. HR  : " + objobservation.JS_HR + "break " +
                        //"6. Others : " + objobservation.JS_Operation;
                        if (objobservation != null)
                        {
                            sDatas.Append("<tr><td align='center' style='font-size: 22px;' colspan='7'>Other Observations</td></tr>");
                            sDatas.Append("<tr style='font-weight:bold;font-size: 15px;'><td colspan='2'>Operations</td><td colspan='5'>" + objobservation.JS_Operation + "</td></tr>");
                            sDatas.Append("<tr style='font-weight:bold;font-size: 15px;'><td colspan='2'>Training</td><td colspan='5'>" + objobservation.JS_Training + "</td></tr>");
                            sDatas.Append("<tr style='font-weight:bold;font-size: 15px;'><td colspan='2'>SCM</td><td colspan='5'>" + objobservation.JS_SCM + "</td></tr>");
                            sDatas.Append("<tr style='font-weight:bold;font-size: 15px;'><td colspan='2'>Compliance</td><td colspan='5'>" + objobservation.JS_Complaince + "</td></tr>");
                            sDatas.Append("<tr style='font-weight:bold;font-size: 15px;'><td colspan='2'>HR</td><td colspan='5'>" + objobservation.JS_HR + "</td></tr>");
                            sDatas.Append("<tr style='font-weight:bold;font-size: 15px;'><td colspan='2'>Others</td><td colspan='5'>" + objobservation.JS_Operation + "</td></tr>");
                        }
                    }
                    catch (Exception) { }
                    sDatas.Append("<tr><td align='center' style='font-size: 22px;' colspan='7'>Client Feedback</td></tr>");
                    sDatas.Append("<tr><td  colspan='7'>" + clientremarks + "</td></tr>");



                    sDatas.Append("</table>");
                    //dvData.InnerHtml = sDatas.ToString();
                    //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "callexport", "<script type='text/javascript'>PrintExce();</script>", false);

                    Response.Clear();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment;filename=DateWiseReport.xls");
                    Response.Charset = "";
                    Response.ContentType = "application/vnd.ms-excel";
                    // string style = @"<style> .textmode { } </style>";
                    //Response.Write(style);
                    Response.Output.Write(sDatas.ToString());
                    Response.Flush();
                    Response.End();
                }
            }
            catch (Exception ex)
            {
            }
        }

        private void MakeExcel(DataSet dsRawdatas, string Getscored)
        {
            try
            {

                string filename = Guid.NewGuid().ToString();
                if (dsRawdatas.Tables[3].Rows.Count > 0)
                {
                    DataRow drfilename = dsRawdatas.Tables[3].Rows[0];
                    filename = drfilename["locsitename"].ToString().Replace(",", string.Empty).Replace("/", string.Empty).Replace(@"\", string.Empty).Replace("<", string.Empty).Replace(">", string.Empty);
                }
                string excelfilepath = Server.MapPath("DownloadExcels/") + filename + ".xlsx";

                FileInfo newFile = new FileInfo(excelfilepath);
                if (newFile.Exists)
                {
                    newFile.Delete();  // ensures we create a new workbook
                    newFile = new FileInfo(excelfilepath);
                }

                using (ExcelPackage package = new ExcelPackage(newFile))
                {
                    // add a new worksheet to the empty workbook
                    ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("DownloadeExcel");

                    //Add the headers

                    StringBuilder sDatas = new StringBuilder();

                    float fEnteirAvg = 0;
                    int iTotalScore = 0;
                    int iTotalWeightage = 0;
                    int iMaxScore = 0;

                    DataTable dtChartdet = new DataTable();
                    dtChartdet.Columns.Add("Categoryname", typeof(string));
                    dtChartdet.Columns.Add("Value", typeof(float));
                    dtChartdet.Columns.Add("auditId", typeof(int));

                    if (dsRawdatas.Tables.Count > 0 && dsRawdatas.Tables[0].Rows.Count > 0)
                    {

                        DataTable dtAudit = dsRawdatas.Tables[0];
                        DataTable dtCategory = dsRawdatas.Tables[1];
                        DataTable dtRawData = dsRawdatas.Tables[2];
                        DataRow drHeader = dsRawdatas.Tables[3].Rows[0];

                        // worksheet.Cells[1, 1].Value = "Image Here";
                        worksheet.Cells[1, 1].Value = "DUSTERS TOTAL SOLUTIONS SERVICES PVT LTD";

                        var headerFont = worksheet.Cells[1, 1].Style.Font;
                        headerFont.Bold = true;
                        headerFont.Size = 18;
                        worksheet.Cells[1, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        worksheet.Cells[1, 1].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                        worksheet.Cells["A1:G4"].Merge = true;

                        FileInfo fileinfo = new FileInfo(Server.MapPath("AppThemes/images/dts.png"));
                        var picture = worksheet.Drawings.AddPicture("logo", fileinfo);
                        picture.SetSize(100, 50);
                        picture.SetPosition(1, 0, 1, 0);

                        worksheet.Cells[5, 1].Value = "Name of the Client";

                        worksheet.Cells[5, 3].Value = drHeader["clientname"].ToString();
                        worksheet.Cells[5, 6].Value = "";



                        worksheet.Cells["A5:B5"].Merge = true;
                        worksheet.Cells["C5:E5"].Merge = true;
                        worksheet.Cells["F5:G5"].Merge = true;

                        int imaxrowcount = 6;

                        worksheet.Cells[imaxrowcount, 1].Value = "Name of the Site";
                        worksheet.Cells[imaxrowcount, 3].Value = drHeader["locsitename"].ToString();
                        worksheet.Cells[imaxrowcount, 6].Value = "";

                        worksheet.Name = drHeader["locsitename"].ToString();

                        worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                        worksheet.Cells["C" + imaxrowcount + ":E" + imaxrowcount + ""].Merge = true;
                        worksheet.Cells["F" + imaxrowcount + ":G" + imaxrowcount + ""].Merge = true;

                        imaxrowcount++;

                        worksheet.Cells[imaxrowcount, 1].Value = "Tower";
                        worksheet.Cells[imaxrowcount, 3].Value = drHeader["towername"].ToString();
                        worksheet.Cells[imaxrowcount, 6].Value = "";

                        worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                        worksheet.Cells["C" + imaxrowcount + ":E" + imaxrowcount + ""].Merge = true;
                        worksheet.Cells["F" + imaxrowcount + ":G" + imaxrowcount + ""].Merge = true;

                        imaxrowcount++;

                        worksheet.Cells[imaxrowcount, 1].Value = "Site ID";
                        worksheet.Cells[imaxrowcount, 3].Value = GetSiteID(drHeader["locsitename"].ToString());
                        worksheet.Cells[imaxrowcount, 6].Value = "";

                        worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                        worksheet.Cells["C" + imaxrowcount + ":E" + imaxrowcount + ""].Merge = true;
                        worksheet.Cells["F" + imaxrowcount + ":G" + imaxrowcount + ""].Merge = true;

                        imaxrowcount++;

                        worksheet.Cells[imaxrowcount, 1].Value = "Name of the Client person";
                        worksheet.Cells[imaxrowcount, 3].Value = drHeader["clientperson"].ToString();
                        worksheet.Cells[imaxrowcount, 6].Value = "";

                        worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                        worksheet.Cells["C" + imaxrowcount + ":E" + imaxrowcount + ""].Merge = true;
                        worksheet.Cells["F" + imaxrowcount + ":G" + imaxrowcount + ""].Merge = true;

                        imaxrowcount++;

                        worksheet.Cells[imaxrowcount, 1].Value = "Name of the SBU";
                        worksheet.Cells[imaxrowcount, 3].Value = drHeader["sbuname"].ToString();
                        worksheet.Cells[imaxrowcount, 6].Value = "";

                        worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                        worksheet.Cells["C" + imaxrowcount + ":E" + imaxrowcount + ""].Merge = true;
                        worksheet.Cells["F" + imaxrowcount + ":G" + imaxrowcount + ""].Merge = true;

                        imaxrowcount++;

                        worksheet.Cells[imaxrowcount, 1].Value = "Name of the OM / AOM";
                        worksheet.Cells[imaxrowcount, 3].Value = drHeader["aom"].ToString();
                        worksheet.Cells[imaxrowcount, 6].Value = "";

                        worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                        worksheet.Cells["C" + imaxrowcount + ":E" + imaxrowcount + ""].Merge = true;
                        worksheet.Cells["F" + imaxrowcount + ":G" + imaxrowcount + ""].Merge = true;

                        imaxrowcount++;

                        worksheet.Cells[imaxrowcount, 1].Value = "Name of the Auditor";
                        worksheet.Cells[imaxrowcount, 3].Value = drHeader["auditor"].ToString();
                        worksheet.Cells[imaxrowcount, 6].Value = "";

                        worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                        worksheet.Cells["C" + imaxrowcount + ":E" + imaxrowcount + ""].Merge = true;
                        worksheet.Cells["F" + imaxrowcount + ":G" + imaxrowcount + ""].Merge = true;

                        imaxrowcount++;

                        worksheet.Cells[imaxrowcount, 1].Value = "Name of the Auditee";
                        worksheet.Cells[imaxrowcount, 3].Value = drHeader["auditee"].ToString();
                        worksheet.Cells[imaxrowcount, 6].Value = "";

                        worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                        worksheet.Cells["C" + imaxrowcount + ":E" + imaxrowcount + ""].Merge = true;
                        worksheet.Cells["F" + imaxrowcount + ":G" + imaxrowcount + ""].Merge = true;

                        imaxrowcount++;


                        worksheet.Cells[imaxrowcount, 1].Value = "Last audit date";
                        worksheet.Cells[imaxrowcount, 3].Value = drHeader["displastauditdate"].ToString();
                        worksheet.Cells[imaxrowcount, 6].Value = "Last Audit Score";
                        worksheet.Cells[imaxrowcount, 7].Value = drHeader["lastscore"].ToString() + "%";

                        worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                        worksheet.Cells["C" + imaxrowcount + ":E" + imaxrowcount + ""].Merge = true;
                        //worksheet.Cells["F13:G13"].Merge = true;

                        imaxrowcount++;

                        worksheet.Cells[imaxrowcount, 1].Value = "Current audit date";
                        worksheet.Cells[imaxrowcount, 3].Value = drHeader["dispauditdate"].ToString();
                        worksheet.Cells[imaxrowcount, 6].Value = "Current Audit Score";
                        worksheet.Cells[imaxrowcount, 7].Value = Getscored + "%";

                        worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                        worksheet.Cells["C" + imaxrowcount + ":E" + imaxrowcount + ""].Merge = true;

                        imaxrowcount++;
                        


                        for (int iauditcount = 0; iauditcount < dtAudit.Rows.Count; iauditcount++)
                        {
                            DataRow draudit = dtAudit.Rows[iauditcount];
                            int auditid = Convert.ToInt32(draudit["auditid"].ToString());
                            string auditname = draudit["auditname"].ToString();

                            worksheet.Cells[imaxrowcount, 1].Value = auditname;
                            worksheet.Cells[imaxrowcount, 1].Style.Font.Bold = true;

                            worksheet.Cells["A" + imaxrowcount + ":G" + imaxrowcount + ""].Merge = true;
                            worksheet.Cells[imaxrowcount, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            imaxrowcount++;

                            worksheet.Cells[imaxrowcount, 1].Value = "Sl.No";
                            worksheet.Cells[imaxrowcount, 2].Value = "Observations";
                            worksheet.Cells[imaxrowcount, 3].Value = "Maximum Score";
                            worksheet.Cells[imaxrowcount, 4].Value = "Score Obtained";
                            worksheet.Cells[imaxrowcount, 5].Value = "Auditor Remarks";
                            worksheet.Cells[imaxrowcount, 6].Value = "Remarks by Sbu";
                            worksheet.Cells[imaxrowcount, 7].Value = "Image";

                            var range = worksheet.Cells[imaxrowcount, 1, imaxrowcount, 7];
                            range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                            range.Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#FCB133"));

                            imaxrowcount++;

                            int iscore = 0;
                            int iweightage = 0;
                            int iSingleAvg = 0;
                            int iTotalAvg = 0;
                            int iAuditMax = 0;
                            DataRow[] drCategory = dtCategory.Select("auditid=" + auditid);
                            for (int icategroryCount = 0; icategroryCount < drCategory.Length; icategroryCount++)
                            {
                                int categoryid = Convert.ToInt32(drCategory[icategroryCount]["categoryid"].ToString());
                                string categoryname = drCategory[icategroryCount]["categoryname"].ToString();

                                worksheet.Cells[imaxrowcount, 1].Value = icategroryCount + 1;
                                worksheet.Cells[imaxrowcount, 2].Value = categoryname;
                                worksheet.Cells[imaxrowcount, 1].Style.Font.Bold = true;
                                worksheet.Cells[imaxrowcount, 2].Style.Font.Bold = true;
                                worksheet.Cells[imaxrowcount, 1].Style.Font.Size = 14;

                                worksheet.Cells["B" + imaxrowcount + ":G" + imaxrowcount + ""].Merge = true;
                                imaxrowcount++;

                                DataRow[] drRawDatas = dtRawData.Select("categoryid=" + categoryid);

                                int iCategoryScore = 0;
                                int iCategoryTotal = 0;
                                int iCatMax = 0;

                                int iCatWisePercentage = 0;
                                int iCatWiseTotal = 0;
                                for (int iRawDatacount = 0; iRawDatacount < drRawDatas.Length; iRawDatacount++)
                                {
                                    string question = drRawDatas[iRawDatacount]["auditqname"].ToString();
                                    string remarks = drRawDatas[iRawDatacount]["remarks"].ToString();
                                    int score = Convert.ToInt32(drRawDatas[iRawDatacount]["score"].ToString());
                                    int weightage = Convert.ToInt32(drRawDatas[iRawDatacount]["weightage"].ToString());
                                    string scorename = drRawDatas[iRawDatacount]["scorename"].ToString();
                                    string imgFile = drRawDatas[iRawDatacount]["uploadfilename"].ToString();
                                    string transactionid = drRawDatas[iRawDatacount]["TransactionId"].ToString();
                                    string filelink = string.Empty;
                                    if (imgFile.Trim() != string.Empty)
                                    {
                                        string filepath = "http://ifazility.com/facilityaudit/ImageLists.aspx";
                                        if (ConfigurationManager.AppSettings["Imaglists"] != null)
                                        {
                                            filepath = ConfigurationManager.AppSettings["Imaglists"].ToString();
                                        }
                                        filelink = filepath + "?TransactionId=" + transactionid;
                                        //string[] imgfiles = imgFile.Split('#');
                                        //foreach (string files in imgfiles)
                                        //{
                                        //    if (files.Trim() != string.Empty)
                                        //    {
                                        //        imgFile = "http://ifazility.com/facilityaudit/auditimage/" + files;
                                        //        //filelink += "<a href='" + imgFile + "'>" + imgFile + "</a><br/>";

                                        //        filelink = "HYPERLINK(\"" + imgFile + "\",\"" + files + "\")";
                                        //    }
                                        //}
                                    }

                                    int tScore = 0;
                                    int MaxScore = 0;

                                    if (score == -1)
                                    {
                                        score = 0;
                                        tScore = -1;
                                        weightage = 0;
                                        MaxScore = 0;
                                    }
                                    else
                                    {
                                        MaxScore = 1;
                                    }

                                    iscore += score;
                                    iweightage += weightage;

                                    iCategoryScore += score;
                                    iCategoryTotal += weightage;

                                    iTotalScore += score;
                                    iTotalWeightage += weightage;

                                    iSingleAvg = score * weightage;
                                    iTotalAvg += iSingleAvg;

                                    iCatWisePercentage = score * weightage;
                                    iCatWiseTotal += iCatWisePercentage;

                                    iMaxScore += MaxScore;
                                    iCatMax += MaxScore;
                                    iAuditMax += MaxScore;

                                    string _score = string.Empty;
                                    string _maxscore = string.Empty;
                                    if (tScore == 0)
                                    {
                                        _score = " " + score;
                                        _maxscore = " " + MaxScore;
                                    }
                                    else
                                    {
                                        _score = scorename;
                                        _maxscore = scorename;
                                    }


                                    worksheet.Cells[imaxrowcount, 1].Value = iRawDatacount + 1;
                                    worksheet.Cells[imaxrowcount, 2].Value = question;

                                    if (question.Length >= 75)
                                    {
                                        worksheet.Cells[imaxrowcount, 2].Style.WrapText = true;
                                    }

                                    //worksheet.Cells[imaxrowcount, 2].Style.WrapText = true;
                                    //worksheet.Cells[imaxrowcount, 3].Value = _maxscore;

                                    int MaxRefId;
                                    int ScorerefID;

                                    bool isNumeric = int.TryParse(_maxscore.Trim(), out MaxRefId);

                                    if (isNumeric)
                                    {
                                        worksheet.Cells[imaxrowcount, 3].Value = MaxRefId;
                                        worksheet.Cells[imaxrowcount, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    }
                                    else
                                    {
                                        worksheet.Cells[imaxrowcount, 3].Value = _maxscore;
                                        worksheet.Cells[imaxrowcount, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    }

                                    isNumeric = int.TryParse(_score.Trim(), out ScorerefID);

                                    if (isNumeric)
                                    {
                                        worksheet.Cells[imaxrowcount, 4].Value = ScorerefID;
                                        worksheet.Cells[imaxrowcount, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    }
                                    else
                                    {
                                        worksheet.Cells[imaxrowcount, 4].Value = _score;
                                        worksheet.Cells[imaxrowcount, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    }

                                    //worksheet.Cells[imaxrowcount, 4].Value = _score;
                                    worksheet.Cells[imaxrowcount, 5].Value = remarks;

                                    if (remarks.Length >= 75)
                                    {
                                        worksheet.Cells[imaxrowcount, 5].Style.WrapText = true;
                                    }

                                    worksheet.Cells[imaxrowcount, 6].Value = string.Empty;

                                    if (imgFile.Trim() != string.Empty)
                                    {
                                        var cell = worksheet.Cells[imaxrowcount, 7];
                                        cell.Hyperlink = new Uri(filelink);
                                        cell.Value = "ImageList";
                                    }

                                    // worksheet.Cells[imaxrowcount, 7].Formula = filelink;
                                    //worksheet.Cells[imaxrowcount, 7].Style.WrapText = true;
                                    imaxrowcount++;
                                }

                                float _CateWisTot = 0;
                                if (iCategoryTotal != 0)
                                {
                                    //_CateWisTot = ((float)(iCategoryScore * 100 / iCategoryTotal));
                                    _CateWisTot = ((float)(iCatWiseTotal * 100 / iCategoryTotal));
                                }
                                else
                                {
                                    _CateWisTot = 0;
                                }

                                dtChartdet.Rows.Add(categoryname, _CateWisTot, auditid);


                                string sCatNA = string.Empty;
                                string sCatMaxNA = string.Empty;
                                sCatNA = iCatMax.ToString();
                                sCatMaxNA = iCategoryScore.ToString();
                                if (iCatMax == 0)
                                {
                                    sCatNA = "N/A";
                                }
                                if (iCategoryScore == 0)
                                {
                                    sCatMaxNA = "N/A";
                                }

                                worksheet.Cells[imaxrowcount, 1].Value = "Sub Total " + categoryname;

                                int _catena;
                                int _catmaxna;
                                bool Numeric = int.TryParse(sCatNA.Trim(), out _catena);

                                if (Numeric)
                                {
                                    worksheet.Cells[imaxrowcount, 3].Value = _catena;
                                    worksheet.Cells[imaxrowcount, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                }
                                else
                                {
                                    worksheet.Cells[imaxrowcount, 3].Value = sCatNA;
                                    worksheet.Cells[imaxrowcount, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                }
                                worksheet.Cells[imaxrowcount, 3].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                worksheet.Cells[imaxrowcount, 3].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#FCB133"));

                                Numeric = int.TryParse(sCatMaxNA.Trim(), out _catmaxna);

                                if (Numeric)
                                {
                                    worksheet.Cells[imaxrowcount, 4].Value = _catmaxna;
                                    worksheet.Cells[imaxrowcount, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                }
                                else
                                {
                                    worksheet.Cells[imaxrowcount, 4].Value = sCatMaxNA;
                                    worksheet.Cells[imaxrowcount, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                }

                                worksheet.Cells[imaxrowcount, 4].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                worksheet.Cells[imaxrowcount, 4].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#FCB133"));

                                //worksheet.Cells[imaxrowcount, 3].Value = sCatNA;
                                //worksheet.Cells[imaxrowcount, 4].Value = sCatMaxNA;

                                worksheet.Cells[imaxrowcount, 1].Style.Font.Bold = true;
                                worksheet.Cells[imaxrowcount, 3].Style.Font.Bold = true;
                                worksheet.Cells[imaxrowcount, 4].Style.Font.Bold = true;
                                worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                                imaxrowcount++;
                            }
                            float iScoredAvg = 0;
                            iScoredAvg = ((float)(iTotalAvg * 100 / iweightage));
                            fEnteirAvg += iScoredAvg;

                            worksheet.Cells[imaxrowcount, 1].Value = auditname;
                            worksheet.Cells[imaxrowcount, 3].Value = iAuditMax;
                            worksheet.Cells[imaxrowcount, 4].Value = iscore;

                            worksheet.Cells[imaxrowcount, 1].Style.Font.Bold = true;
                            worksheet.Cells[imaxrowcount, 3].Style.Font.Bold = true;
                            worksheet.Cells[imaxrowcount, 4].Style.Font.Bold = true;
                            worksheet.Cells[imaxrowcount, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            worksheet.Cells[imaxrowcount, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                            worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                            imaxrowcount++;

                            worksheet.Cells[imaxrowcount, 1].Value = "Percentage (%)";
                            worksheet.Cells[imaxrowcount, 3].Value = string.Empty;
                            worksheet.Cells[imaxrowcount, 4].Value = iScoredAvg + "%";

                            worksheet.Cells[imaxrowcount, 1].Style.Font.Bold = true;
                            worksheet.Cells[imaxrowcount, 4].Style.Font.Bold = true;
                            worksheet.Cells[imaxrowcount, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                            imaxrowcount++;
                        }
                        float tAvg = fEnteirAvg / (dsRawdatas.Tables[0].Rows.Count);

                        worksheet.Cells[imaxrowcount, 1].Value = "GRAND TOTAL SCORE";
                        worksheet.Cells[imaxrowcount, 3].Value = iMaxScore;
                        worksheet.Cells[imaxrowcount, 4].Value = iTotalScore;
                        worksheet.Cells[imaxrowcount, 1].Style.Font.Bold = true;
                        worksheet.Cells[imaxrowcount, 3].Style.Font.Bold = true;
                        worksheet.Cells[imaxrowcount, 4].Style.Font.Bold = true;
                        worksheet.Cells[imaxrowcount, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        worksheet.Cells[imaxrowcount, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        worksheet.Cells[imaxrowcount, 3].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        worksheet.Cells[imaxrowcount, 4].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        worksheet.Cells[imaxrowcount, 3].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#FCB133"));
                        worksheet.Cells[imaxrowcount, 4].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#FCB133"));
                        worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                        imaxrowcount++;

                        decimal scoreGot = Convert.ToDecimal(Getscored);


                        worksheet.Cells[imaxrowcount, 1].Value = "Percentage (%)";
                        worksheet.Cells[imaxrowcount, 3].Value = string.Empty;
                        worksheet.Cells[imaxrowcount, 4].Value = Getscored + "%";
                        worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                        worksheet.Cells[imaxrowcount, 1].Style.Font.Bold = true;
                        worksheet.Cells[imaxrowcount, 4].Style.Font.Bold = true;
                        string setColour = string.Empty;
                        if (scoreGot >= 83)
                        {
                            worksheet.Cells[imaxrowcount, 4].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            worksheet.Cells[imaxrowcount, 4].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#00FF00"));
                        }
                        else
                        {
                            worksheet.Cells[imaxrowcount, 4].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            worksheet.Cells[imaxrowcount, 4].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#FFBE00"));
                        }
                        worksheet.Cells[imaxrowcount, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        imaxrowcount++;



                        if (dsRawdatas.Tables[3].Rows.Count > 0)
                        {
                            worksheet.Cells[imaxrowcount, 1].Value = "Follow-Up Date";
                            worksheet.Cells[imaxrowcount, 3].Value = string.Empty;
                            worksheet.Cells[imaxrowcount, 4].Value = dsRawdatas.Tables[3].Rows[0]["dispFollowUpDate"].ToString();
                            worksheet.Cells[imaxrowcount, 1].Style.Font.Bold = true;
                            worksheet.Cells[imaxrowcount, 4].Style.Font.Bold = true;
                            worksheet.Cells[imaxrowcount, 4].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            worksheet.Cells[imaxrowcount, 4].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#FCB133"));
                            worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                            imaxrowcount++;
                        }


                        string observations = drHeader["observation"].ToString();
                        string clientremarks = drHeader["feedback"].ToString();
                        observations = observations.Replace('[', ' ');
                        observations = observations.Replace(']', ' ');
                        try
                        {
                            Observations objobservation = JsonConvert.DeserializeObject<Observations>(observations);
                            //                string observationdet = "1. Operations : " + objobservation.JS_Operation + "break" +
                            //"2. Training  : " + objobservation.JS_Training + " break  " +
                            //"3. SCM  : " + objobservation.JS_SCM + " break " +
                            //"4. Compliance  : " + objobservation.JS_Complaince + "break" +
                            //"5. HR  : " + objobservation.JS_HR + "break " +
                            //"6. Others : " + objobservation.JS_Operation;
                            if (objobservation != null)
                            {

                                worksheet.Cells[imaxrowcount, 1].Value = "Other Observations";
                                worksheet.Cells["A" + imaxrowcount + ":G" + imaxrowcount + ""].Merge = true;
                                worksheet.Cells[imaxrowcount, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                worksheet.Cells[imaxrowcount, 1].Style.Font.Bold = true;
                                imaxrowcount++;

                                worksheet.Cells[imaxrowcount, 1].Value = "Operations";
                                worksheet.Cells[imaxrowcount, 3].Value = objobservation.JS_Operation;
                                worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                                worksheet.Cells["C" + imaxrowcount + ":G" + imaxrowcount + ""].Merge = true;
                                imaxrowcount++;

                                worksheet.Cells[imaxrowcount, 1].Value = "Training";
                                worksheet.Cells[imaxrowcount, 3].Value = objobservation.JS_Training;
                                worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                                worksheet.Cells["C" + imaxrowcount + ":G" + imaxrowcount + ""].Merge = true;
                                imaxrowcount++;

                                worksheet.Cells[imaxrowcount, 1].Value = "SCM";
                                worksheet.Cells[imaxrowcount, 3].Value = objobservation.JS_SCM;
                                worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                                worksheet.Cells["C" + imaxrowcount + ":G" + imaxrowcount + ""].Merge = true;
                                imaxrowcount++;

                                worksheet.Cells[imaxrowcount, 1].Value = "Compliance";
                                worksheet.Cells[imaxrowcount, 3].Value = objobservation.JS_Complaince;
                                worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                                worksheet.Cells["C" + imaxrowcount + ":G" + imaxrowcount + ""].Merge = true;
                                imaxrowcount++;

                                worksheet.Cells[imaxrowcount, 1].Value = "HR";
                                worksheet.Cells[imaxrowcount, 3].Value = objobservation.JS_HR;
                                worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                                worksheet.Cells["C" + imaxrowcount + ":G" + imaxrowcount + ""].Merge = true;
                                imaxrowcount++;

                                worksheet.Cells[imaxrowcount, 1].Value = "Others";
                                worksheet.Cells[imaxrowcount, 3].Value = objobservation.JS_Operation;
                                worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                                worksheet.Cells["C" + imaxrowcount + ":G" + imaxrowcount + ""].Merge = true;
                                imaxrowcount++;
                            }
                        }
                        catch (Exception) { }

                        worksheet.Cells[imaxrowcount, 1].Value = "Client Feedback";
                        worksheet.Cells[imaxrowcount, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        worksheet.Cells[imaxrowcount, 1].Style.Font.Bold = true;
                        worksheet.Cells["A" + imaxrowcount + ":G" + imaxrowcount + ""].Merge = true;
                        imaxrowcount++;


                        worksheet.Cells[imaxrowcount, 1].Value = clientremarks;
                        worksheet.Cells["A" + imaxrowcount + ":G" + imaxrowcount + ""].Merge = true;
                        imaxrowcount++;


                        worksheet.Cells[1, 1, imaxrowcount - 1, 7].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        worksheet.Cells[1, 1, imaxrowcount - 1, 7].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        worksheet.Cells[1, 1, imaxrowcount - 1, 7].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        worksheet.Cells[1, 1, imaxrowcount - 1, 7].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                        // worksheet.Cells.AutoFitColumns(0);  //Autofit columns for all cells
                        // worksheet.Cells.AutoFitColumns(1);
                        //// worksheet.Cells.AutoFitColumns(2);
                        worksheet.Column(2).Width = 140;
                        worksheet.Column(5).Width = 140;
                        worksheet.Cells.AutoFitColumns(1);
                        worksheet.Cells.AutoFitColumns(2);
                        worksheet.Cells.AutoFitColumns(3);
                        // worksheet.Cells.AutoFitColumns(4);
                        //worksheet.Cells.AutoFitColumns(5);
                        // worksheet.Cells.AutoFitColumns(6);
                        // worksheet.Cells.AutoFitColumns(7);

                        int c1;

                        imaxrowcount = 7;
                        ExcelWorksheet worksheetChart = package.Workbook.Worksheets.Add("CategoryChart");

                        string Alphabetic = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                        int startchar = 0;
                        int colspace = 0;
                        foreach (DataRow drAuditRow in dtAudit.Rows)
                        {
                            int auditid = Convert.ToInt32(drAuditRow["auditid"].ToString());
                            string auditname = drAuditRow["auditname"].ToString();
                            DataRow[] drAuditCategories = dtChartdet.Select("auditid=" + auditid);
                            if (drAuditCategories.Length > 0)
                            {
                                DataTable dtCategoryChart = drAuditCategories.CopyToDataTable();
                                int startvalue = imaxrowcount;

                                char Firstcol = Alphabetic[startchar];
                                char Secondcol = Alphabetic[startchar + 1];

                                foreach (DataRow drchartdet in dtCategoryChart.Rows)
                                {
                                    string categoryname = drchartdet["categoryname"].ToString();
                                    decimal score = Convert.ToDecimal(drchartdet["Value"].ToString());

                                    worksheetChart.Cells["" + Firstcol + imaxrowcount].Value = categoryname;
                                    worksheetChart.Cells["" + Secondcol + imaxrowcount].Value = score;
                                    imaxrowcount++;
                                }

                                var chart = worksheetChart.Drawings.AddChart("chart" + imaxrowcount, eChartType.ColumnClustered3D);
                                var series = chart.Series.Add("" + Secondcol + startvalue + ":" + Secondcol + imaxrowcount, "" + Firstcol + startvalue + ":" + Firstcol + imaxrowcount);
                                //series.HeaderAddress = new ExcelAddress("'Sheet1'!B" + (startvalue - 1));
                                chart.SetSize(510, 300);
                                chart.Title.Text = auditname;
                                chart.SetPosition(7, 0, colspace, 0);
                                colspace += 9;

                                startchar += 2;
                            }
                        }


                        package.Save();

                        Response.ContentType = ContentType;
                        Response.AppendHeader("Content-Disposition", "attachment; filename=" + Path.GetFileName(excelfilepath));
                        Response.WriteFile(excelfilepath);
                        Response.End();

                        //this.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                        //this.Response.AddHeader(
                        //          "content-disposition",
                        //          string.Format("attachment;  filename={0}", "ExcellData.xlsx"));
                        //this.Response.BinaryWrite(package.GetAsByteArray());
                    }

                }

                //FileInfo prevFile = new FileInfo(excelfilepath);
                //if (prevFile.Exists)
                //{
                //    prevFile.Delete();
                //}
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, "error");
            }
        }

        protected void ddlocations_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindDetails();
        }

        //public void GeneratePlot()
        //{
        //    //DataPoint objpt = new DataPoint();
        //    //objpt.XValue = (double)63;       
        //    //objpt.Label="";

        //    DataTable dtchart = new DataTable();
        //    dtchart.Columns.Add("Lable", typeof(string));
        //    dtchart.Columns.Add("value", typeof(double));

        //    for (int icount = 66; icount < 80; icount++)
        //    {
        //        dtchart.Rows.Add("Lable" + icount, (double)icount);
        //    }



        //    using (var ch = new Chart1())
        //    {
        //        Chart1.ChartAreas.Add(new ChartArea());

        //        var s = new Series();
        //        //foreach (var pnt in series) s.Points.Add(pnt);

        //        foreach (DataRow drchar in dtchart.Rows)
        //        {
        //            DataPoint pnt = new DataPoint();
        //            pnt.XValue = Convert.ToDouble(drchar["value"].ToString());
        //            pnt.Label = drchar["Lable"].ToString();
        //            s.Points.Add(pnt);
        //        }

        //        Chart1.Series.Add(
        //        //ch.SaveImage(outputStream, ChartImageFormat.Jpeg);
        //        ch.SaveImage("E:\\venkadesan\\sample.png", System.Drawing.Imaging.ImageFormat.Png);
        //    }
        //}
    }
}