﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FacilityAudit.BLL;
using BL;
using System.Data;
using FacilityAudit.Code;

namespace Facility_Audit
{
    public partial class LocationMailDetails : System.Web.UI.Page
    {
        #region Properties
        private bool IsAdd
        {
            get
            {
                if (!ID.HasValue)
                    return false;
                else if (ID.Value != 0)
                    return false;
                else
                    return true;
            }
        }


        private int? ID
        {
            get
            {
                if (ViewState["id"] == null)
                    ViewState["id"] = 0;
                return (int?)ViewState["id"];
            }

            set
            {
                ViewState["id"] = value;
            }
        }

        private bool DisableStatus
        {
            get
            {
                if (ViewState["disablestatus"] == null)
                    ViewState["disablestatus"] = 0;
                return (bool)ViewState["disablestatus"];
            }

            set
            {
                ViewState["disablestatus"] = value;
            }
        }

        #endregion

        BLAudit objaudit = new BLAudit();
        BLCategory objauditcategory = new BLCategory();
        BLQuestion objq = new BLQuestion();
        BLGroupcompany objgrp = new BLGroupcompany();


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ButtonStatus(Common.ButtonStatus.Cancel);
                loadsbu();
            }
        }
        private void ButtonStatus(Common.ButtonStatus status)
        {
            this.txtcliname.Text = txtclimailid.Text = txtregname.Text = txtregmailid.Text = txtstatename.Text = txtstatemailid.Text =
                txtsbuname.Text = txtsbumailid.Text = txtomname.Text = txtommailid.Text = txtaomname.Text = txtaommailid.Text = string.Empty;
            txtgmopsemail.Text = txtgmopsname.Text = string.Empty;
            txtnationheademail.Text = txtnationheadname.Text = string.Empty;
            //ddlgroup.ClearSelection();

            txtOperationHeadName.Text = string.Empty;
            txtOperationHeadMail.Text = string.Empty;
            txtTrainingname.Text = string.Empty;
            txtTrainingmail.Text = string.Empty;
            txtSCMname.Text = string.Empty;
            txtSCMmail.Text = string.Empty;
            txtCompliancename.Text = string.Empty;
            txtCompliancemail.Text = string.Empty;
            txthrname.Text = string.Empty;
            txthrmail.Text = string.Empty;
            txtOthermail.Text = string.Empty;
            txtOthername.Text = string.Empty;

            if (status == Common.ButtonStatus.New)
            {
                this.pnlnew.Visible = true;
                this.bNew.Enabled = false;
                this.bNew.Visible = false;
                this.btnsave.Enabled = true;
                this.txtregname.Focus();
                this.lblError.Text = "";
                this.divmsg.Visible = false;
                this.linewbtn.Visible = false;
            }
            else if (status == Common.ButtonStatus.Edit)
            {
                this.txtregname.Focus();
                this.pnlnew.Visible = true;
                this.bNew.Enabled = false;
                this.bNew.Visible = false;
                this.btnsave.Enabled = true;
                this.lblError.Text = "";
                this.divmsg.Visible = false;
                this.linewbtn.Visible = false;
            }
            else if (status == Common.ButtonStatus.Cancel)
            {
                this.pnlnew.Visible = false;
                this.bNew.Visible = true;
                this.bNew.Enabled = true;
                this.btnsave.Enabled = false;
                this.ID = 0;
                divrep.Visible = true;
                loadrprtdetails();
                this.lblError.Text = "";
                this.divmsg.Visible = false;
                this.linewbtn.Visible = true;
            }
            else if (status == Common.ButtonStatus.Save)
            {
                this.pnlnew.Visible = false;
                this.bNew.Visible = true;
                this.bNew.Enabled = true;
                this.btnsave.Enabled = false;
                this.ID = 0;
                divrep.Visible = true;
                this.linewbtn.Visible = true;

            }
        }
        protected void btnsave_Click(object sender, EventArgs e)
        {
            try
            {
                //if (this.txtregname.Text == string.Empty)
                //{
                //    NotifyMessages("Enter Regionname", Common.ErrorType.Error);
                //    return;
                //}

                //if (this.txtregmailid.Text == string.Empty)
                //{
                //    NotifyMessages("Enter Region Mailid", Common.ErrorType.Error);
                //    return;
                //}


                //if (this.txtstatename.Text == string.Empty)
                //{
                //    NotifyMessages("Enter Statename", Common.ErrorType.Error);
                //    return;
                //}

                //if (this.txtstatemailid.Text == string.Empty)
                //{
                //    NotifyMessages("Enter State Mailid", Common.ErrorType.Error);
                //    return;
                //}

                int companyid = 0, locationid = 0;
                if (!Common.DDVal(ddlcompany, out companyid))
                {
                    NotifyMessages("Please select company", Common.ErrorType.Error);
                    return;
                }
                if (!Common.DDVal(ddllocation, out locationid))
                {
                    NotifyMessages("Please select Location", Common.ErrorType.Error);
                    return;
                }

                if (this.txtsbuname.Text == string.Empty)
                {
                    NotifyMessages("Enter SBU Name", Common.ErrorType.Error);
                    return;
                }

                if (this.txtsbumailid.Text == string.Empty)
                {
                    NotifyMessages("Enter SBU Mailid", Common.ErrorType.Error);
                    return;
                }
                //if (this.txtomname.Text == string.Empty)
                //{
                //    NotifyMessages("Enter OM Name", Common.ErrorType.Error);
                //    return;
                //}

                //if (this.txtommailid.Text == string.Empty)
                //{
                //    NotifyMessages("Enter OM Mailid", Common.ErrorType.Error);
                //    return;
                //}

                //if (this.txtaomname.Text == string.Empty)
                //{
                //    NotifyMessages("Enter AOM Name", Common.ErrorType.Error);
                //    return;
                //}

                //if (this.txtaommailid.Text == string.Empty)
                //{
                //    NotifyMessages("Enter AOM Mailid", Common.ErrorType.Error);
                //    return;
                //}



                if (this.IsAdd)
                {
                    if (objauditcategory.addloctionmaildetails(companyid, locationid, this.txtregname.Text.Trim(), this.txtregmailid.Text.Trim(),
                          txtstatename.Text.Trim(), txtstatemailid.Text.Trim(), txtsbuname.Text.Trim(), txtsbumailid.Text.Trim(),
                          txtomname.Text.Trim(), txtommailid.Text.Trim(), txtaomname.Text.Trim(), txtaommailid.Text.Trim(),
                          txtcliname.Text.Trim(), txtclimailid.Text.Trim(), txtgmopsname.Text.Trim(), txtgmopsemail.Text.Trim(),
                          txtnationheadname.Text.Trim(), txtnationheademail.Text.Trim(),

                           txtOperationHeadName.Text,
    txtOperationHeadMail.Text,
    txtTrainingname.Text,
    txtTrainingmail.Text,
    txtSCMname.Text,
    txtSCMmail.Text,
    txtCompliancename.Text,
    txtCompliancemail.Text,
    txthrname.Text,
    txthrmail.Text,
    txtOthermail.Text,
    txtOthername.Text,
                          1, DateTime.Now) > 0)
                    {
                        NotifyMessages("Saved successfully", Common.ErrorType.Information);
                    }
                    else
                    {
                        NotifyMessages("Category already exist", Common.ErrorType.Information);
                        return;
                    }
                }
                else if (this.ID != null && this.ID.Value > 0)
                {
                    if (objauditcategory.updatemlocationmail(companyid, locationid, this.txtregname.Text.Trim(), this.txtregmailid.Text.Trim(),
                          txtstatename.Text.Trim(), txtstatemailid.Text.Trim(), txtsbuname.Text.Trim(), txtsbumailid.Text.Trim(),
                          txtomname.Text.Trim(), txtommailid.Text.Trim(), txtaomname.Text.Trim(), txtaommailid.Text.Trim(),
                          txtcliname.Text.Trim(), txtclimailid.Text.Trim(), txtgmopsname.Text.Trim(), txtgmopsemail.Text.Trim(),
                           txtnationheadname.Text.Trim(), txtnationheademail.Text.Trim(),
                            txtOperationHeadName.Text,
    txtOperationHeadMail.Text,
    txtTrainingname.Text,
    txtTrainingmail.Text,
    txtSCMname.Text,
    txtSCMmail.Text,
    txtCompliancename.Text,
    txtCompliancemail.Text,
    txthrname.Text,
    txthrmail.Text,
    txtOthername.Text,
txtOthermail.Text,
                          1, DateTime.Now, Convert.ToInt32(this.ID.Value)) > 0)
                    {
                        NotifyMessages("Updated successfully", Common.ErrorType.Information);
                    }
                    else
                    {
                        NotifyMessages("Category already exist", Common.ErrorType.Information);
                        return;
                    }
                }

                ButtonStatus(Common.ButtonStatus.Save);


            }
            catch { }

        }

        public void NotifyMessages(string message, Common.ErrorType et)
        {
            divmsg.Visible = true;
            lblError.Text = message;
            if (et == Common.ErrorType.Error)
            {

            }
            else if (et == Common.ErrorType.Information)
            {

            }
        }

        protected void btncncl_Click(object sender, EventArgs e)
        {
            ButtonStatus(Common.ButtonStatus.Cancel);
        }

        private void loadrprtdetails()
        {
            try
            {
                DataSet ds = new DataSet();
                int companyid = 0, locationid = 0;
                Common.DDVal(ddlcompany, out companyid);
                Common.DDVal(ddllocation, out locationid);
                ds = objauditcategory.getlocationmailid(0, companyid, locationid, 0);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    divrep.Visible = true;
                    rptrcat.DataSource = ds;
                    rptrcat.DataBind();
                }
                else
                {
                    rptrcat.DataSource = null;
                    rptrcat.DataBind();
                    divrep.Visible = false;
                }
            }
            catch { }
        }

        protected void rptrcat_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Edit")
                {
                    ButtonStatus(Common.ButtonStatus.Edit);
                    this.ID = Convert.ToInt32(e.CommandArgument);
                    BindToCtrls();
                }
                else if (e.CommandName == "Delete")
                {
                    //this.ID = Convert.ToInt32(e.CommandArgument);
                    //DataSet dp = new DataSet();
                    //dp = objq.getmauditquestion(Convert.ToInt32(this.ID.Value), 0, 0);
                    //if (dp.Tables[0].Rows.Count > 0)
                    //{
                    //    NotifyMessages("Cannot be delete as reference exist", Common.ErrorType.Error);
                    //    return;
                    //}
                    //else
                    //{
                    //    objauditcategory.deletemauditcategory(this.ID.Value);
                    //    ButtonStatus(Common.ButtonStatus.Cancel);
                    //    NotifyMessages("Deleted successfully", Common.ErrorType.Information);
                    //}
                }
            }
            catch { }
        }

        public void BindToCtrls()
        {
            try
            {
                DataSet ds = new DataSet();
                int companyid = 0, locationid = 0;
                Common.DDVal(ddlcompany, out companyid);
                Common.DDVal(ddllocation, out locationid);
                ds = objauditcategory.getlocationmailid(1, companyid, locationid, Convert.ToInt32(this.ID.Value));
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    DataRow drdetails = ds.Tables[0].Rows[0];
                    this.txtcliname.Text = drdetails["clientname"].ToString();
                    txtclimailid.Text = drdetails["clientmailid"].ToString();
                    txtregname.Text = drdetails["regionname"].ToString();
                    txtregmailid.Text = drdetails["regionmailid"].ToString();
                    txtstatename.Text = drdetails["statename"].ToString();
                    txtstatemailid.Text = drdetails["statemailid"].ToString();
                    txtsbuname.Text = drdetails["sbuname"].ToString();
                    txtsbumailid.Text = drdetails["sbumailid"].ToString();
                    txtomname.Text = drdetails["omname"].ToString();
                    txtommailid.Text = drdetails["ommailid"].ToString();
                    txtaomname.Text = drdetails["aomname"].ToString();
                    txtaommailid.Text = drdetails["aommailid"].ToString();
                    txtgmopsemail.Text = drdetails["GMOPSMailID"].ToString();
                    txtgmopsname.Text = drdetails["GMOPSName"].ToString();
                    txtnationheademail.Text = drdetails["NationalHeadEmail"].ToString();
                    txtnationheadname.Text = drdetails["NationalHeadName"].ToString();

                    txtOperationHeadName.Text = drdetails["OperationHeadName"].ToString();
                    txtOperationHeadMail.Text = drdetails["OperationHeadMail"].ToString();
                    txtTrainingname.Text = drdetails["TrainingHeadName"].ToString();
                    txtTrainingmail.Text = drdetails["TrainingHeadMail"].ToString();
                    txtSCMname.Text = drdetails["SCMHeadName"].ToString();
                    txtSCMmail.Text = drdetails["SCMHeadMail"].ToString();
                    txtCompliancename.Text = drdetails["ComplianceHeadName"].ToString();
                    txtCompliancemail.Text = drdetails["ComplianceHeadMail"].ToString();
                    txthrname.Text = drdetails["HRHeadName"].ToString();
                    txthrmail.Text = drdetails["HRHeadMail"].ToString();
                    txtOthermail.Text = drdetails["OthersHeadMail"].ToString();
                    txtOthername.Text = drdetails["OthersHeadName"].ToString();
                }
            }
            catch { }
        }

        protected void rptrcat_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

        }

        protected void bNew_Click(object sender, EventArgs e)
        {
            ButtonStatus(Common.ButtonStatus.New);
        }


        private void loadsbu()
        {
            try
            {

                DataSet ds = new DataSet();
                ds = objgrp.getsbubuUserid(UserSession.UserID);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddlsbu.DataSource = ds;
                    ddlsbu.DataTextField = "locationsettingsname";
                    ddlsbu.DataValueField = "locationsettingsid";
                    ddlsbu.DataBind();

                    ddlsbu_SelectedIndexChanged(null, null);
                }

                else
                {
                    ddlsbu.Items.Clear();
                    ddlcompany.Items.Clear();
                    ddllocation.Items.Clear();

                    rptrcat.DataSource = null;
                    rptrcat.DataBind();
                }
            }
            catch { }
        }

        private void loadcompany()
        {
            try
            {
                DataSet ds = new DataSet();

                int sbuid = 0;
                Common.DDVal(ddlsbu, out sbuid);

                //ds = objgrp.getcompanybysbu(sbuid);
                ds = objgrp.getcompanybysbuGroup(sbuid, UserSession.GroupID);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    ds.Tables[0].DefaultView.ToTable(true, "CompanyName");
                    ddlcompany.DataSource = ds;
                    ddlcompany.DataTextField = "CompanyName";
                    ddlcompany.DataValueField = "CompanyID";
                    ddlcompany.DataBind();

                    ddlcompany_SelectedIndexChanged(null, null);
                }
                else
                {
                    divmsg.Visible = false;
                    ddlcompany.Items.Clear();
                    ddllocation.Items.Clear();

                    rptrcat.DataSource = null;
                    rptrcat.DataBind();
                }
            }
            catch { }
        }

        private void loadlocation()
        {
            try
            {
                ButtonStatus(Common.ButtonStatus.Cancel);
                DataSet ds = new DataSet();
                // ds = objgrpcmp.getlocationbycompany(Convert.ToInt32(ddlsbu.SelectedValue));

                int sbuid, companyid = 0, locationid = 0;
                Common.DDVal(ddlsbu, out sbuid);
                Common.DDVal(ddlcompany, out companyid);
                Common.DDVal(ddllocation, out locationid);

                ds = objgrp.getlocationbysbu(sbuid, companyid);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddllocation.DataSource = ds;
                    ddllocation.DataTextField = "LocationName";
                    ddllocation.DataValueField = "LocationID";
                    ddllocation.DataBind();

                    ddllocation_SelectedIndexChanged(null, null);
                }
                else
                {
                    ddllocation.Items.Clear();
                    rptrcat.DataSource = null;
                    rptrcat.DataBind();
                    divmsg.Visible = false;
                }
            }
            catch { }
        }

        protected void ddlsbu_SelectedIndexChanged(object sender, EventArgs e)
        {
            ButtonStatus(Common.ButtonStatus.Cancel);
            loadcompany();
            loadlocation();
        }

        protected void ddlcompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            ButtonStatus(Common.ButtonStatus.Cancel);
            loadlocation();
        }

        protected void ddllocation_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                loadrprtdetails();
            }
            catch { }
        }
    }
}