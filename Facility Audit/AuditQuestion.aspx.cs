﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FacilityAudit.BLL;
using System.Data;
using FacilityAudit.Code;
using BL;
using OfficeOpenXml;
using System.IO;
using OfficeOpenXml.DataValidation;
using System.Data.OleDb;

namespace Facility_Audit
{
    public partial class AuditQuestion : System.Web.UI.Page
    {
        #region Properties
        private bool IsAdd
        {
            get
            {
                if (!ID.HasValue)
                    return false;
                else if (ID.Value != 0)
                    return false;
                else
                    return true;
            }
        }


        private int? ID
        {
            get
            {
                if (ViewState["id"] == null)
                    ViewState["id"] = 0;
                return (int?)ViewState["id"];
            }

            set
            {
                ViewState["id"] = value;
            }
        }

        private bool DisableStatus
        {
            get
            {
                if (ViewState["disablestatus"] == null)
                    ViewState["disablestatus"] = 0;
                return (bool)ViewState["disablestatus"];
            }

            set
            {
                ViewState["disablestatus"] = value;
            }
        }

        #endregion

        BLAudit objaudit = new BLAudit();
        BLCategory objcat = new BLCategory();
        BLQuestion objquestion = new BLQuestion();
        BLAuditRating objrating = new BLAuditRating();
        BLAuditscore objscore = new BLAuditscore();
        BLGroupcompany objgrpcmp = new BLGroupcompany();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                loadsector();
                loadaudit();
                loadcategory();
                loadrating();
                loadweightage();
                loadrprtdetails();
                ButtonStatus(Common.ButtonStatus.Cancel);
            }
        }

        private void loadsector()
        {
            try
            {
                DataSet ds = new DataSet();
                //ds = objgrp.getsector();
                ds = objgrpcmp.GetSectorwithGroup(UserSession.GroupID);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddlsector.DataSource = ds;
                    ddlsector.DataTextField = "locationsectorname";
                    ddlsector.DataValueField = "locationsectorid";
                    ddlsector.DataBind();
                    lblsector.Text = ddlsector.SelectedItem.Text;
                }
            }
            catch
            {

            }
        }

        private void loadaudit()
        {
            try
            {
                DataSet ds = new DataSet();
                ds = objaudit.getmaudit(0, 0, Convert.ToInt32(ddlsector.SelectedValue));

                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddlaudit.DataSource = ds;
                    ddlaudit.DataTextField = "auditname";
                    ddlaudit.DataValueField = "auditid";
                    ddlaudit.DataBind();
                    divrep.Visible = true;
                }
                else
                {
                    divrep.Visible = false;
                }
            }
            catch
            {

            }
        }

        private void loadcategory()
        {
            try
            {
                DataSet ds = new DataSet();
                ds = objcat.getmauditcategory(Convert.ToInt32(ddlaudit.SelectedValue), 0, 0);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddlauditcat.DataSource = ds;
                    ddlauditcat.DataTextField = "categoryname";
                    ddlauditcat.DataValueField = "categoryid";
                    ddlauditcat.DataBind();
                }
            }
            catch
            {

            }
        }

        private void loadrating()
        {
            try
            {
                DataSet ds = new DataSet();
                ds = objrating.getmauditrating(Convert.ToInt32(ddlaudit.SelectedValue), 0, 0);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddlrating.DataSource = ds;
                    ddlrating.DataTextField = "rating";
                    ddlrating.DataValueField = "ratingid";
                    ddlrating.DataBind();
                }
            }
            catch
            {

            }
        }

        private void loadweightage()
        {
            try
            {
                DataSet ds = new DataSet();
                ds = objrating.getmauditweightage(Convert.ToInt32(ddlaudit.SelectedValue), 0, 0);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddlweightage.DataSource = ds;
                    ddlweightage.DataTextField = "weightage";
                    ddlweightage.DataValueField = "weightageid";
                    ddlweightage.DataBind();
                }
            }
            catch
            {

            }
        }

        protected void bNew_Click(object sender, EventArgs e)
        {
            ButtonStatus(Common.ButtonStatus.New);
        }

        private void loadrprtdetails()
        {
            try
            {
                DataSet ds = new DataSet();
                ds = objquestion.getmauditquestion(Convert.ToInt32(ddlauditcat.SelectedValue), 0, 0);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    divrep.Visible = true;
                    rptrquestion.DataSource = ds;
                    rptrquestion.DataBind();
                }
                else
                {
                    divrep.Visible = false;
                }
            }
            catch
            {

            }
        }

        protected void rptrquestion_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Edit")
                {
                    ButtonStatus(Common.ButtonStatus.Edit);
                    this.ID = Convert.ToInt32(e.CommandArgument);
                    BindToCtrls();
                }
                else if (e.CommandName == "Delete")
                {
                    this.ID = Convert.ToInt32(e.CommandArgument);
                    objquestion.deletemauditquestion(this.ID.Value);

                    NotifyMessages("Deleted successfully", Common.ErrorType.Information);
                    ButtonStatus(Common.ButtonStatus.Cancel);
                }
                loadrprtdetails();
            }
            catch
            {

            }
        }

        protected void btnsave_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.txtquestion.Text == string.Empty)
                {
                    NotifyMessages("Enter Question", Common.ErrorType.Error);
                    return;
                }

                if (this.IsAdd)
                {
                    if (objquestion.saveauditquestion(txtquestion.Text.Trim(), txtdes.Text.Trim(),
                        Convert.ToInt32(ddlauditcat.SelectedValue),
                        Convert.ToInt32(ddlrating.SelectedValue), Convert.ToInt32(ddlweightage.SelectedValue),
                        UserSession.UserID, DateTime.Now) > 0)
                    {
                        NotifyMessages("Saved successfully", Common.ErrorType.Information);
                    }
                    else
                    {
                        NotifyMessages("Question Name already exist", Common.ErrorType.Information);
                        return;
                    }
                }
                else if (this.ID != null && this.ID.Value > 0)
                {
                    if (objquestion.updatemauditquestion(Convert.ToInt32(this.ID.Value), txtquestion.Text.Trim(),
                        txtdes.Text.Trim(), Convert.ToInt32(ddlauditcat.SelectedValue),
                        Convert.ToInt32(ddlrating.SelectedValue), Convert.ToInt32(ddlweightage.SelectedValue),
                        UserSession.UserID, DateTime.Now) > 0)
                    {
                        NotifyMessages("Updated successfully", Common.ErrorType.Information);
                    }
                    else
                    {
                        NotifyMessages("Question Name already exist", Common.ErrorType.Information);
                        return;
                    }
                }

                ButtonStatus(Common.ButtonStatus.Save);
            }
            catch
            {

            }
        }

        protected void btncncl_Click(object sender, EventArgs e)
        {
            ButtonStatus(Common.ButtonStatus.Cancel);
        }

        public void BindToCtrls()
        {
            try
            {
                //loadaudit();
                //loadcategory();
                //loadrating();
                //loadweightage();

                DataSet ds = new DataSet();
                ds = objquestion.getmauditquestion(Convert.ToInt32(ddlauditcat.SelectedValue), this.ID.Value, 1);

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    ddlaudit.SelectedValue = ds.Tables[0].Rows[0]["auditid"].ToString();
                    ddlauditcat.SelectedValue = ds.Tables[0].Rows[0]["categoryid"].ToString();
                    ddlrating.SelectedValue = ds.Tables[0].Rows[0]["ratingid"].ToString();
                    ddlweightage.SelectedValue = ds.Tables[0].Rows[0]["weightageid"].ToString();
                    this.txtquestion.Text = ds.Tables[0].Rows[0]["auditqname"].ToString();
                    this.txtdes.Text = ds.Tables[0].Rows[0]["qdescription"].ToString();
                }
            }
            catch
            {

            }
        }

        protected void rptrquestion_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

        }

        private void ButtonStatus(Common.ButtonStatus status)
        {
            this.txtdes.Text = txtquestion.Text = string.Empty;
            //ddlaudit.ClearSelection();
            //ddlauditcat.ClearSelection();
            //ddlrating.ClearSelection();
            //ddlweightage.ClearSelection();
            //ddlgroup.ClearSelection();

            if (status == Common.ButtonStatus.New)
            {
                this.pnlnew.Visible = true;
                this.bNew.Enabled = false;
                this.bNew.Visible = false;
                this.btnsave.Enabled = true;
                this.txtdes.Focus();
                this.lblError.Text = "";
                divmsg.Visible = false;
                this.linewbtn.Visible = false;
                loadaudit();
                loadrating();
                loadweightage();
            }
            else if (status == Common.ButtonStatus.Edit)
            {
                this.txtquestion.Focus();
                this.pnlnew.Visible = true;
                this.bNew.Enabled = false;
                this.bNew.Visible = false;
                this.btnsave.Enabled = true;
                this.lblError.Text = "";
                divmsg.Visible = false;
                this.linewbtn.Visible = false;
            }
            else if (status == Common.ButtonStatus.Cancel)
            {
                this.pnlnew.Visible = false;
                this.bNew.Visible = true;
                this.bNew.Enabled = true;
                this.btnsave.Enabled = false;
                this.ID = 0;
                divrep.Visible = true;
                loadrprtdetails();
                this.lblError.Text = "";
                divmsg.Visible = false;
                this.linewbtn.Visible = true;
            }
            else if (status == Common.ButtonStatus.Save)
            {
                this.pnlnew.Visible = false;
                this.bNew.Visible = true;
                this.bNew.Enabled = true;
                this.btnsave.Enabled = false;
                this.ID = 0;
                divrep.Visible = true;
                this.linewbtn.Visible = true;
                loadrprtdetails();
            }
        }

        public void NotifyMessages(string message, Common.ErrorType et)
        {
            divmsg.Visible = true;
            lblError.Text = message;
            if (et == Common.ErrorType.Error)
            {

            }
            else if (et == Common.ErrorType.Information)
            {

            }
        }

        protected void ddlaudit_SelectedIndexChanged(object sender, EventArgs e)
        {
            loadcategory();
            loadrating();
            loadweightage();
            ButtonStatus(Common.ButtonStatus.Cancel);
        }

        protected void ddlauditcat_SelectedIndexChanged(object sender, EventArgs e)
        {
            ButtonStatus(Common.ButtonStatus.Cancel);
            loadrating();
            loadweightage();
        }

        protected void ddlrating_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void ddlweightage_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void ddlsector_SelectedIndexChanged(object sender, EventArgs e)
        {
            loadaudit();
            loadcategory();
            ButtonStatus(Common.ButtonStatus.Cancel);
        }

        protected void lnkdownload_Click(object sender, EventArgs e)
        {
            try
            {
                int sectorid = 0;
                Common.DDVal(ddlsector, out sectorid);
                DataSet dsquesDet = new DataSet();
                dsquesDet = objquestion.GetQuestionDetails(sectorid);
                if (dsquesDet != null && dsquesDet.Tables.Count > 0 && dsquesDet.Tables[0].Rows.Count > 0)
                {
                    string filename = Guid.NewGuid() + ".xlsx";
                    string excelfilepath = Server.MapPath("DownloadExcels/") + filename;
                    GenerateExcel(dsquesDet, excelfilepath);
                }
            }
            catch
            {
            }
        }
        protected void lnksupercategory_Click(object sender, EventArgs e)
        {
            try
            {
                int sectorid = 0;
                Common.DDVal(ddlsector, out sectorid);
                DataSet dsquesDet = new DataSet();
                dsquesDet = objquestion.GetQuestionDetails(sectorid);
                if (dsquesDet != null && dsquesDet.Tables.Count > 0 && dsquesDet.Tables[2].Rows.Count > 0)
                {
                    string filename = Guid.NewGuid() + ".xlsx";
                    string excelfilepath = Server.MapPath("DownloadExcels/") + filename;
                    DownloadExcel(dsquesDet, excelfilepath);
                }
            }
            catch
            {
            }
        }


        private static object CheckInteger(string values)
        {
            try
            {
                int NumericValue;
                decimal decimalvalue;
                bool isNumeric = int.TryParse(values.Trim(), out NumericValue);
                if (isNumeric)
                {
                    return (object)NumericValue;
                }
                else
                {

                    bool isDecimal = decimal.TryParse(values.Replace(",", string.Empty), out decimalvalue);
                    if (isDecimal)
                    {
                        return (object)decimalvalue;
                    }
                }
                return (object)values;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private void DownloadExcel(DataSet dsDatas, string ExcelFilePath)
        {
            try
            {
                DataTable dtQuestions = dsDatas.Tables[2];

                FileInfo newFile = new FileInfo(ExcelFilePath);
                if (newFile.Exists)
                {
                    newFile.Delete();  // ensures we create a new workbook
                    newFile = new FileInfo(ExcelFilePath);
                }
                using (ExcelPackage package = new ExcelPackage(newFile))
                {
                    ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("Super Category");
                    for (int icolcount = 0; icolcount < dtQuestions.Columns.Count; icolcount++)
                    {
                        worksheet.Cells[1, icolcount + 1].Value = dtQuestions.Columns[icolcount].ColumnName;
                        worksheet.Cells[1, icolcount + 1].Style.Font.Bold = true;
                    }
                    int _irowcount = 2;
                    for (int irowcount = 0; irowcount < dtQuestions.Rows.Count; irowcount++)
                    {
                        for (int icolcount = 0; icolcount < dtQuestions.Columns.Count; icolcount++)
                        {
                            worksheet.Cells[_irowcount, (icolcount + 1)].Value = CheckInteger(dtQuestions.Rows[irowcount][icolcount].ToString());
                        }
                        _irowcount++;
                    }

                    for (int icolcount = 0; icolcount < dtQuestions.Columns.Count; icolcount++)
                    {
                        worksheet.Cells.AutoFitColumns(icolcount);//Autofit columns for all cells
                    }

                    string filename = ddlsector.SelectedItem.Text + ".xlsx";

                    package.Save();
                    Response.ContentType = ContentType;
                    Response.AppendHeader("Content-Disposition", "attachment; filename=SuperCategory-" + filename);
                    Response.WriteFile(ExcelFilePath);
                    Response.Flush();
                    Response.End();
                }
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.ToString(), Common.ErrorType.Error);
            }

        }

        private void GenerateExcel(DataSet dsDatas, string ExcelFilePath)
        {
            try
            {
                DataTable dtQuestions = dsDatas.Tables[0];
                DataTable dtsupercat = dsDatas.Tables[1];

                string[] supercategories = new string[dtsupercat.Rows.Count];
                if (dtsupercat.Rows.Count > 0)
                {
                    int irow = 0;
                    foreach (DataRow drfreq in dtsupercat.Rows)
                    {
                        string shortname = drfreq["supercategory"].ToString();
                        supercategories[irow] = shortname;
                        irow++;
                    }
                }
                FileInfo newFile = new FileInfo(ExcelFilePath);
                if (newFile.Exists)
                {
                    newFile.Delete();  // ensures we create a new workbook
                    newFile = new FileInfo(ExcelFilePath);
                }
                using (ExcelPackage package = new ExcelPackage(newFile))
                {
                    ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("Super Category Upload");
                    for (int icolcount = 0; icolcount < dtQuestions.Columns.Count; icolcount++)
                    {
                        worksheet.Cells[1, icolcount + 1].Value = dtQuestions.Columns[icolcount].ColumnName;
                        worksheet.Cells[1, icolcount + 1].Style.Font.Bold = true;
                    }
                    int _irowcount = 2;
                    for (int irowcount = 0; irowcount < dtQuestions.Rows.Count; irowcount++)
                    {
                        for (int icolcount = 0; icolcount < dtQuestions.Columns.Count; icolcount++)
                        {
                            worksheet.Cells[_irowcount, (icolcount + 1)].Value = CheckInteger(dtQuestions.Rows[irowcount][icolcount].ToString());

                            if (dtQuestions.Columns[icolcount].ColumnName.StartsWith("SuperCategory"))
                            {
                                //ExcelWorksheet ws
                                var validation = worksheet.DataValidations.AddListValidation(worksheet.Cells[_irowcount, (icolcount + 1)].Address);
                                validation.ShowErrorMessage = true;
                                validation.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                                validation.ErrorTitle = "Error";
                                validation.Error = "Error Text";
                                // sheet with a name : DropDownLists 

                                foreach (string supercategory in supercategories)
                                {
                                    validation.Formula.Values.Add(supercategory);
                                }

                            }

                        }
                        _irowcount++;
                    }



                    //worksheet.Cells.AutoFitColumns(3);
                    //worksheet.Cells.AutoFitColumns(4);
                    //worksheet.Cells.AutoFitColumns(5);

                    ExcelColumn objxlcol = worksheet.Column(1);
                    objxlcol.ColumnMax = 3;
                    objxlcol.Hidden = true;

                    //worksheet.Column(3).Width = 40;
                    worksheet.Column(4).Width = 50;
                    worksheet.Column(5).Width = 40;
                    worksheet.Column(6).Width = 120;




                    package.Save();
                    Response.ContentType = ContentType;
                    Response.AppendHeader("Content-Disposition", "attachment; filename=" + Path.GetFileName(ExcelFilePath));
                    Response.WriteFile(ExcelFilePath);
                    Response.Flush();
                    Response.End();
                }
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.ToString(), Common.ErrorType.Error);
            }

        }

        protected void lnkupload_Click(object sender, EventArgs e)
        {
            try
            {
                if (fuuploade.HasFile)
                {
                    string fileExtension = Path.GetExtension(fuuploade.PostedFile.FileName);

                    string directoryPath = Server.MapPath(string.Format("~/upload/"));
                    if (!Directory.Exists(directoryPath))
                    {
                        Directory.CreateDirectory(directoryPath);
                    }
                    string filename = Guid.NewGuid().ToString() + "." + fileExtension;
                    if (!File.Exists(Server.MapPath("~/upload/" + filename)))
                    {
                        fuuploade.SaveAs(Server.MapPath("~/upload/" + filename));
                    }
                    else
                    {
                        File.Delete(Server.MapPath("~/upload/" + filename));
                        fuuploade.SaveAs(Server.MapPath("~/upload/" + filename));
                    }
                    DataTable dtUpload = new DataTable();
                    if (fileExtension == ".xls")
                    {
                        dtUpload = Excel(Server.MapPath("~/upload/" + filename), true);
                    }
                    else if (fileExtension == ".xlsx")
                    {
                        dtUpload = Xlsx(Server.MapPath("~/upload/" + filename), true);
                    }

                    if (dtUpload.Rows.Count > 0)
                    {
                        string xmlString = string.Empty;
                        using (TextWriter writer = new StringWriter())
                        {
                            if (dtUpload.Columns.Contains("Audit"))
                            {
                                dtUpload.Columns.Remove("Audit");
                            }
                            if (dtUpload.Columns.Contains("Category"))
                            {
                                dtUpload.Columns.Remove("Category");
                            }
                            if (dtUpload.Columns.Contains("Question"))
                            {
                                dtUpload.Columns.Remove("Question");
                            }

                            dtUpload.TableName = "mapdet";
                            dtUpload.WriteXml(writer);
                            xmlString = writer.ToString();

                            string[] result = objquestion.MapSuperCategory(xmlString, UserSession.UserID);
                            NotifyMessages(result[0], Common.ErrorType.Error);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, Common.ErrorType.Error);
            }
        }


        #region "xl upload"

        public DataTable Excel(String Filepath, Boolean IsHeader)
        {
            //_____________________________________________________________________________________________________________
            //          Description        :  For Reading an Xls
            //          Author             :  N.Venkadesan
            //          Created Date       :  03-Dec-2014
            //_____________________________________________________________________________________________________________

            string strConn = null;
            DataTable dtWorksheet = new DataTable();
            DataTable dt = new DataTable();

            try
            {
                if (IsHeader == true)
                {
                    strConn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Filepath + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=1\"";
                }
                else
                {
                    strConn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Filepath + ";Extended Properties=\"Excel 8.0;HDR=NO;IMEX=1\"";
                }
                OleDbConnection Conn = new OleDbConnection();
                Conn.ConnectionString = strConn;
                Conn.Open();

                dtWorksheet = Conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });

                DataView dv1 = dtWorksheet.Copy().DefaultView;
                dv1.RowFilter = "TABLE_NAME like '%$%'";

                if (dv1.ToTable().Rows.Count == 0)
                {
                    Exception excep = new Exception("Special Characters([,],\\,//,',},{,),(,&)or space in your Excel Sheet Name. Verify!");
                    throw excep;
                }
                //else if (dv1.ToTable().Rows.Count > 1)
                //    {
                //        Exception excep = new Exception("Only one Excel Sheets are Allowed");
                //        throw excep;
                //    }
                else
                {
                    String ExcelTblName = dv1.ToTable().Rows[0][2].ToString().ToLower();
                    //compo.CheckSheetNameErr(ExcelTblName);

                    string s = "select * from [" + ExcelTblName + "]";
                    OleDbDataAdapter da = new OleDbDataAdapter(s, Conn);
                    da.Fill(dt);
                    Conn.Close();
                    return dt.Copy();
                }
            }
            catch (Exception ex)
            {
                Exception excep = new Exception("Excel : " + ex.Message);
                throw excep;
            }
        }

        public DataTable Xlsx(String Filepath, Boolean IsHeader)
        {
            //_____________________________________________________________________________________________________________
            //          Description        :  For Reading an Xlsx
            //          Author             :  N.Venkadesan
            //          Created Date       :  03 Dec 2014
            //_____________________________________________________________________________________________________________

            string strConn = null;
            DataTable dtWorksheet = new DataTable();
            DataTable dt = new DataTable();

            try
            {
                if (IsHeader == true)
                {
                    strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + Filepath + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=1\"";
                }
                else
                {
                    strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + Filepath + ";Extended Properties=\"Excel 12.0;HDR=No;IMEX=1\"";
                }
                OleDbConnection Conn = new OleDbConnection();
                Conn.ConnectionString = strConn;
                Conn.Open();

                dtWorksheet = Conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });

                //dtWorksheet = Conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                DataView dv1 = dtWorksheet.Copy().DefaultView;
                dv1.RowFilter = "TABLE_NAME like '%$%'";
                if (dv1.ToTable().Rows.Count == 0)
                {
                    Exception excep = new Exception("Special Characters([,],\\,//,',},{,),(,&)or space in your Excel Sheet Name. Verify!");
                    throw excep;
                }
                //else if (dv1.ToTable().Rows.Count > 1)
                //    {
                //        Exception excep = new Exception("Only one Excel Sheets are Allowed");
                //        throw excep;
                //    }
                else
                {
                    String ExcelTblName = dv1.ToTable().Rows[0][2].ToString().ToLower();
                    //compo.CheckSheetNameErr(ExcelTblName);
                    string s = "select * from [" + ExcelTblName + "]";
                    OleDbDataAdapter da = new OleDbDataAdapter(s, Conn);
                    da.Fill(dt);
                    Conn.Close();
                    return dt.Copy();
                }

                //dtExcelsheetname = excelcon.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });

            }
            catch (Exception ex)
            {
                Exception excep = new Exception("Excel : " + ex.Message);
                throw excep;
            }
        }

        #endregion
    }
}