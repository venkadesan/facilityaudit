﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FacilityAudit.Code;
using KB = Kowni.BusinessLogic;
using KCB = Kowni.Common.BusinessLogic;
using System.Data;
using System.Configuration;
using System.Text;
using FacilityAudit.BLL;
using BL;
using System.Web.UI.HtmlControls;
using System.IO;
using Ionic.Zip;
using System.Threading;
using System.Globalization;
using System.Collections;
using InfoSoftGlobal;

namespace Facility_Audit
{
    public partial class Dashboard : System.Web.UI.Page
    {
        #region Properties
        private bool IsAdd
        {
            get
            {
                if (!ID.HasValue)
                    return false;
                else if (ID.Value != 0)
                    return false;
                else
                    return true;
            }
        }


        private int? ID
        {
            get
            {
                if (ViewState["id"] == null)
                    ViewState["id"] = 0;
                return (int?)ViewState["id"];
            }

            set
            {
                ViewState["id"] = value;
            }
        }

        private bool DisableStatus
        {
            get
            {
                if (ViewState["disablestatus"] == null)
                    ViewState["disablestatus"] = 0;
                return (bool)ViewState["disablestatus"];
            }

            set
            {
                ViewState["disablestatus"] = value;
            }
        }

        #endregion
        BLGroupcompany objgrp = new BLGroupcompany();
        BLTransaction objtransaction = new BLTransaction();
        BLGroupcompany objgrpcmp = new BLGroupcompany();
        BLCategory objauditcategory = new BLCategory();
        BLAudit objaudit = new BLAudit();
        KB.BLCompanyMainForms objMainForms = new KB.BLCompanyMainForms();
        KB.BLCompanySubForms objSubForms = new KB.BLCompanySubForms();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");

                //DateTime dt = Convert.ToDateTime(DateTime.Now.ToString("MM/dd/yyyy"));
                //// txtauditdate.Text = string.Format("{0:D}", dt);
                //tfromdate.Text = DateTime.Now.AddHours(Convert.ToInt32(ConfigurationManager.AppSettings["addhour"].ToString()))
                //                        .AddMinutes(Convert.ToInt32(ConfigurationManager.AppSettings["addminute"].ToString())).ToString("MM/dd/yyyy");
                if (Session["FromDate"] != null)
                {
                    tfromdate.Text = Session["FromDate"].ToString();
                }
                else
                {
                    tfromdate.Text = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).ToString("MM/dd/yyyy");
                }
                if (Session["ToDate"] != null)
                {
                    txttodate.Text = Session["ToDate"].ToString();
                }
                else
                {
                    txttodate.Text = DateTime.Now.ToString("MM/dd/yyyy");
                }

                loadsbu();

                loadcompany();
                loadlocation();
               
                if (Request.QueryString["auditid"] != null )
                {
                    int auditid = Convert.ToInt32(Request.QueryString["auditid"].ToString());
                    loadallauditscore();
                    FetchInspection();
                    loadallcategory(auditid);

                }
                //Session["CategoryCount"] = "1";
                
                //if (Request.QueryString["CityID"] != null && Request.QueryString["cityname"] != null)
                //{
                //    int cityid = Convert.ToInt32(Request.QueryString["CityID"].ToString());
                //    string cityname = Request.QueryString["cityname"].ToString();
                //    if (Session["auditid"] != null)
                //    {
                //        int _inspectionid = Convert.ToInt32(Session["auditid"]);
                //        string _inspectionname = Session["auditname"].ToString();
                //        FetchCountryGaugeDashboard(_inspectionid, _inspectionname);
                //        FetchGaugeDashboard(_inspectionid, _inspectionname);                       
                //    }
                //    ClearControls();
                //}
                //if (Request.QueryString["CountryID"] != null)
                //{
                //    if (Session["auditid"] != null)
                //    {
                //        int _inspectionid = Convert.ToInt32(Session["auditid"]);
                //        string _inspectionname = Session["auditname"].ToString();
                //        FetchCountryGaugeDashboard(_inspectionid, _inspectionname);
                //        FetchGaugeDashboard(_inspectionid, _inspectionname);
                //    }
                //    ClearControls();
                //}               
            }
        }

        private void ClearControls()
        {
            try
            {
                //if (rptrinspectiondetails.Items.Count == 0)
                //{
                //    //gvCategory.DataSource = null;
                //    //gvCategory.DataBind();
                //    //rptAssetDet.DataSource = null;
                //    //rptAssetDet.DataBind();
                //    lblCategory.Text = string.Empty;
                //    lblRawDatas.Text = string.Empty;
                //    lblCountry.Text = string.Empty;
                //    lblGaugeCountry.Text = string.Empty;
                //    lblCountryname.Text = string.Empty;
                //    lblCitywise.Text = string.Empty;
                //    dlguaguedashboard.DataSource = null;
                //    dlguaguedashboard.DataBind();
                //    lblInspection.Text = string.Empty;
                //    locationliteral.Text = string.Empty;
                //    OverallStatusLiteral.Text = string.Empty;
                //}
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void loadsbu()
        {
            try
            {
                DataSet ds = new DataSet();
                ds = objgrpcmp.getsbu();
                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddlsbu.DataSource = ds;
                    ddlsbu.DataTextField = "locationsettingsname";
                    ddlsbu.DataValueField = "locationsettingsid";
                    ddlsbu.DataBind();
                    try
                    {
                        this.ddlsbu.ClearSelection();
                        this.ddlsbu.Items.FindByValue(UserSession.sbu.ToString()).Selected = true;
                    }
                    catch
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            UserSession.sbu = Convert.ToInt32(this.ddlsbu.Items[0].Value);
                            // UserSession.CompanyName = this.ddlsbu.Items[0].Text;
                        }
                    }
                }

                else
                {
                    UserSession.sbu = 0;
                }
            }
            catch { }
        }       

        private void loadcompany()
        {
            try
            {
                DataSet ds = new DataSet();
                ds = objgrpcmp.getcompanybysbu(Convert.ToInt32(UserSession.sbu));

                if (ds.Tables[0].Rows.Count > 0)
                {
                    ds.Tables[0].DefaultView.ToTable(true, "CompanyName");
                    ddlcompany.DataSource = ds;
                    ddlcompany.DataTextField = "CompanyName";
                    ddlcompany.DataValueField = "CompanyID";
                    ddlcompany.DataBind();
                    try
                    {
                        this.ddlcompany.ClearSelection();
                        this.ddlcompany.Items.FindByValue(UserSession.Companyid.ToString()).Selected = true;
                    }
                    catch
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            UserSession.Companyid = Convert.ToInt32(this.ddlcompany.Items[0].Value);
                            UserSession.CompanyName = this.ddlcompany.Items[0].Text;
                        }
                    }
                }
                else
                {
                    divmsg.Visible = false;
                }
            }
            catch { }
        }

        private void loadlocation()
        {
            try
            {
                DataSet ds = new DataSet();
                // ds = objgrpcmp.getlocationbycompany(Convert.ToInt32(ddlsbu.SelectedValue));
                ds = objgrpcmp.getlocationbysbu(Convert.ToInt32(UserSession.sbu), Convert.ToInt32(ddlcompany.SelectedValue));

                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddllocation.DataSource = ds;
                    ddllocation.DataTextField = "LocationName";
                    ddllocation.DataValueField = "LocationID";
                    ddllocation.DataBind();
                    UserSession.sectorid = Convert.ToInt32(ds.Tables[0].Rows[0]["sectorid"].ToString());
                    try
                    {
                        this.ddllocation.ClearSelection();
                        this.ddllocation.Items.FindByValue(UserSession.locationid.ToString()).Selected = true;
                    }
                    catch
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            UserSession.locationid = Convert.ToInt32(this.ddllocation.Items[0].Value);
                            UserSession.LocationName = this.ddllocation.Items[0].Text;
                        }
                    }
                }
                else
                {
                    UserSession.sectorid = 0;
                    divmsg.Visible = false;
                }                
            }
            catch { UserSession.sectorid = 0; }
        }

        protected void ddlsbu_SelectedIndexChanged(object sender, EventArgs e)
        {
            UserSession.sbu = Convert.ToInt32(ddlsbu.SelectedValue);
            loadcompany();
            loadlocation();           
        }

        protected void ddlcompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            UserSession.Companyid = Convert.ToInt32(ddlcompany.SelectedValue);
            loadlocation();
        }

        protected void ddllocation_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                UserSession.locationid = Convert.ToInt32(ddllocation.SelectedValue);
                DataSet ds = new DataSet();
                ds = objgrpcmp.getlocationsbubylocationid(Convert.ToInt32(UserSession.locationid));

                UserSession.sectorid = Convert.ToInt32(ds.Tables[0].Rows[0]["sectorid"].ToString());
            }
            catch { }
            FetchInspection();
        }

        protected void btngo_Click(object sender, EventArgs e)
        {
            Session["FromDate"] = tfromdate.Text;
            Session["Todate"] = txttodate.Text;
            loadallauditscore();
            FetchInspection();            
        }

        private void loadallauditscore()
        {
            try
            {
                DataSet ds = new DataSet();
                double percent = 0;
                ds = objaudit.getscorefordashbaord(0, Convert.ToInt16(ddlsbu.SelectedValue), Convert.ToInt16(ddlcompany.SelectedValue), Convert.ToInt16(ddllocation.SelectedValue),
                    Convert.ToDateTime(tfromdate.Text.Trim()), Convert.ToDateTime(txttodate.Text.Trim()));
                if (ds.Tables[0].Rows.Count > 0)
                {
                    percent = Convert.ToDouble(ds.Tables[0].Rows[0]["total"].ToString());
                    string guaguestring = ShowCountryGauge(Convert.ToString(percent));
                    lblallaudit.Text = FusionCharts.RenderChart("../FusionCharts/AngularGauge.swf", "",
             guaguestring, "countrywise", "340", "340", false, true);
                    lblname.Text = "Overall Score" + " - " + percent.ToString("#") + "%";
                }
            }
            catch { }
        }

        private void FetchInspection()
        {
            try
            {
                DataSet ds = new DataSet();
                DateTime FromDate = DateTime.ParseExact(tfromdate.Text, "MM/dd/yyyy", CultureInfo.InvariantCulture);

                ds = objaudit.getdashscorebyaudit(0, Convert.ToInt16(ddlsbu.SelectedValue), Convert.ToInt16(ddlcompany.SelectedValue), Convert.ToInt16(ddllocation.SelectedValue),
                      Convert.ToDateTime(tfromdate.Text.Trim()), Convert.ToDateTime(txttodate.Text.Trim()));
                if (ds.Tables[0].Rows.Count > 0)
                {
                    dlguaguedashboard.DataSource = ds;
                    dlguaguedashboard.DataBind();
                }
                else
                {
                    dlguaguedashboard.DataSource = null;
                    dlguaguedashboard.DataBind();
                }
            }
            catch (Exception ex)
            {
                //throw ex;
            }
        }

        private void loadallcategory(int auditid)
        {
            try
            {
                DataSet ds = new DataSet();
                DateTime FromDate = DateTime.ParseExact(tfromdate.Text, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                
                ds = objaudit.getdashscorebycategory(0, Convert.ToInt16(ddlsbu.SelectedValue), Convert.ToInt16(ddlcompany.SelectedValue), Convert.ToInt16(ddllocation.SelectedValue),
                      Convert.ToDateTime(tfromdate.Text.Trim()), Convert.ToDateTime(txttodate.Text.Trim()), auditid);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    datcatwise.DataSource = ds;
                    datcatwise.DataBind();
                }
                else
                {
                    datcatwise.DataSource = null;
                    datcatwise.DataBind();
                }
            }
            catch (Exception ex)
            {
                //throw ex;
            }
        }

        protected void rptrinspectiondetails_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {

                }
            }
            catch (Exception ex)
            {

            }
        }

        private string ShowCountryGauge(string gaugevalue)
        {
            try
            {
                StringBuilder xmlData = new StringBuilder();
               // string val = "dashboard.aspx%3FCountryID%3d" + 1 + "city";
                string val = string.Empty;

                xmlData.Append("<chart manageresize='1'  clickURL='" + val + "' origw='340' origh='340' bgcolor='FFFFFF' upperlimit='100' lowerlimit='0' showlimits='1' basefontcolor='666666' majortmnumber='11' majortmcolor='666666' majortmheight='8' minortmnumber='5' minortmcolor='666666' minortmheight='3' pivotradius='20' showgaugeborder='0' gaugeouterradius='100' gaugeinnerradius='90' gaugeoriginx='170' gaugeoriginy='170' gaugestartangle='250' gaugeendangle='-70' placevaluesinside='1' gaugefillmix='' pivotfillmix='{F0EFEA}, {BEBCB0}' pivotbordercolor='BEBCB0' pivotfillratio='80,20' showshadow='0' showborder='0'>");
                xmlData.Append("<colorrange>");
                xmlData.Append("<color minvalue='0' maxvalue='80' code='00FF00' alpha='0' />");
                xmlData.Append("<color minvalue='80' maxvalue='100' code='FF0000' alpha='50' />");
                xmlData.Append("</colorrange>");
                xmlData.Append("<dials>");
                xmlData.Append("<dial value='" + gaugevalue + "' bordercolor='FFFFFF' bgcolor='bebcb0, f0efea, bebcb0' borderalpha='0' basewidth='10' topwidth='3' />");
                xmlData.Append("</dials>");
                xmlData.Append("<annotations>");
                xmlData.Append("<annotationgroup x='170' y='170'>");
                xmlData.Append("<annotation type='circle' x='0' y='0' radius='150' bordercolor='bebcb0' fillasgradient='1' fillcolor='f0efea, bebcb0' fillratio='85,15' />");
                xmlData.Append("<annotation type='circle' x='0' y='0' radius='120' fillcolor='bebcb0, f0efea' fillratio='85,15' />");
                xmlData.Append("<annotation type='circle' x='0' color='FFFFFF' y='0' radius='100' bordercolor='f0efea' />");
                xmlData.Append("</annotationgroup>");
                xmlData.Append("</annotations>");
                xmlData.Append("</chart>");

                return xmlData.ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void FetchGaugeDashboard(int auditid, string auditname)
        {
            try
            {
                DataSet ds = new DataSet();
                DateTime FromDate = DateTime.ParseExact(tfromdate.Text, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                
                ds = objauditcategory.getmauditcategory( auditid,0,0);
                
                if (ds.Tables[0].Rows.Count > 0)
                {
                    dlguaguedashboard.DataSource = ds;
                    dlguaguedashboard.DataBind();
                    lblallaudit.Text = "Audit Wise - " + auditname;
                }
                else
                {
                    dlguaguedashboard.DataSource = null;
                    dlguaguedashboard.DataBind();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void dlguaguedashboard_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    string percent = string.Empty;

                    HiddenField hidauditid = (HiddenField)e.Item.FindControl("hidauditid");
                    HiddenField hidpercantage = (HiddenField)e.Item.FindControl("hidtotal");
                    Label lblgauge = (Label)e.Item.FindControl("lblgauge");
                    Label lblcityName = (Label)e.Item.FindControl("lblcityName");

                    percent = hidpercantage.Value;
                    if (percent != string.Empty)
                    {

                        string chart = ShowGauge(percent, hidauditid.Value.ToString(), lblcityName.Text);
                        lblcityName.Text += " - " + percent + "%";
                        lblgauge.Text = FusionCharts.RenderChart("/FusionCharts/AngularGauge.swf", "",
                  chart, "AuditWise" + hidauditid.Value.ToString(), "300", "200", false, true);
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        protected void datcatwise_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    string percent = string.Empty;

                    HiddenField hidauditid = (HiddenField)e.Item.FindControl("hidauditid");
                    HiddenField hidcatid = (HiddenField)e.Item.FindControl("hidcatid");
                    HiddenField hidpercantage = (HiddenField)e.Item.FindControl("hidtotal");
                    Label lblcatgauge = (Label)e.Item.FindControl("lblcatgauge");
                    Label lblcatname = (Label)e.Item.FindControl("lblcatname");

                    percent = hidpercantage.Value;
                    if (percent != string.Empty)
                    {
                        string chart = ShowGauge2(percent, hidauditid.Value.ToString(), hidcatid.Value.ToString(), lblcatname.Text);
                        lblcatname.Text += " - " + percent + "%";
                        lblcatgauge.Text = FusionCharts.RenderChart("/FusionCharts/AngularGauge.swf", "",
                  chart, "CategoryWise" + hidcatid.Value.ToString(), "300", "200", false, true);
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private string ShowGauge(string gaugevalue, string auditid, string auditname)
        {
            try
            {
                StringBuilder xmlData = new StringBuilder();
                string val = "dashboard.aspx%3Fauditid%3d" + auditid;
                
                xmlData.Append("<chart manageresize='1' origw='400' origh='250' clickURL='" + val + "'  managevalueoverlapping='1' autoaligntickvalues='1' bgcolor='FFFFFF,0040FF' numberSuffix='%' fillangle='45' upperlimit='100' lowerlimit='0' majortmnumber='10' majortmheight='8' showgaugeborder='0' gaugeouterradius='140' pivotradius='20' pivotfillcolor='000000,383836' gaugeoriginx='205' gaugeoriginy='206' gaugeinnerradius='2' formatnumberscale='1'  decmials='2' tickmarkdecimals='1' pivotradius='17' showpivotborder='1' pivotbordercolor='000000' pivotborderthickness='5' pivotfillmix='FFFFFF,000000' tickvaluedistance='10' showborder='0'>");
                xmlData.Append("<colorrange>");
                xmlData.Append("<color minValue='0' maxValue='" + gaugevalue + "' code='8BBA00'/>");
                xmlData.Append("<color minValue='" + gaugevalue + "' maxValue='100' code='FF654F'/>");
                xmlData.Append("</colorrange>");
                xmlData.Append("<dials>");
                xmlData.Append("<dial value='" + gaugevalue + "' borderalpha='0' bgcolor='000000' basewidth='28' topwidth='1' radius='100' />");
                xmlData.Append("</dials>");

                xmlData.Append("</chart>");

                return xmlData.ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private string ShowGauge2(string gaugevalue, string auditid,string catid, string auditname)
        {
            try
            {
                StringBuilder xmlData = new StringBuilder();
                //string val = "dashboard.aspx%3Fauditid%3d" + auditid;
                string val = string.Empty;

                xmlData.Append("<chart manageresize='1' origw='400' origh='250' clickURL='" + val + "'  managevalueoverlapping='1' autoaligntickvalues='1' bgcolor='FFFFFF,0040FF' numberSuffix='%' fillangle='45' upperlimit='100' lowerlimit='0' majortmnumber='10' majortmheight='8' showgaugeborder='0' gaugeouterradius='140' pivotradius='20' pivotfillcolor='000000,383836' gaugeoriginx='205' gaugeoriginy='206' gaugeinnerradius='2' formatnumberscale='1'  decmials='2' tickmarkdecimals='1' pivotradius='17' showpivotborder='1' pivotbordercolor='000000' pivotborderthickness='5' pivotfillmix='FFFFFF,000000' tickvaluedistance='10' showborder='0'>");
                xmlData.Append("<colorrange>");
                xmlData.Append("<color minValue='0' maxValue='" + gaugevalue + "' code='8BBA00'/>");
                xmlData.Append("<color minValue='" + gaugevalue + "' maxValue='100' code='FF654F'/>");
                xmlData.Append("</colorrange>");
                xmlData.Append("<dials>");
                xmlData.Append("<dial value='" + gaugevalue + "' borderalpha='0' bgcolor='000000' basewidth='28' topwidth='1' radius='100' />");
                xmlData.Append("</dials>");

                xmlData.Append("</chart>");

                return xmlData.ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}