﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BL;
using System.Data;

namespace Facility_Audit
{
    public partial class ImageLists : System.Web.UI.Page
    {
        BLTransaction objbltran = new BLTransaction();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["TransactionID"] != null)
                {
                    int _transactionid = Convert.ToInt32(Request.QueryString["TransactionID"].ToString());
                    GetImageLists(_transactionid);
                }
                else
                {
                    Response.Redirect("login.aspx", false);
                }
            }
        }

        private void GetImageLists(int TransactionId)
        {
            try
            {
                DataSet dsImglist = new DataSet();
                dsImglist = objbltran.GetImageListByTransactionId(TransactionId);
                if (dsImglist.Tables.Count > 0 && dsImglist.Tables[0].Rows.Count > 0)
                {
                    dlimglist.DataSource = dsImglist.Tables[0];
                    dlimglist.DataBind();

                    lblquestionn.Text = dsImglist.Tables[1].Rows[0][0].ToString();
                }
                else
                {
                    dlimglist.DataSource = null;
                    dlimglist.DataBind();

                    lblquestionn.Text = string.Empty;
                }
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, "error");
            }
        }

        protected string GetUrl(string imagepath)
        {
            string[] splits = Request.Url.AbsoluteUri.Split('/');
            if (splits.Length >= 2)
            {
                string url = splits[0] + "//";
                for (int i = 2; i < splits.Length - 1; i++)
                {
                    url += splits[i];
                    url += "/";
                }
                return url + imagepath;
            }
            return imagepath;
        }


        public void NotifyMessages(string message, string errortype)
        {
            divmsg.Visible = true;
            lblError.Text = message;
            if (errortype == "info")
            {
                divmsg.Attributes.Add("class", "alert alert-success fade in");
            }
            else if (errortype == "error")
            {
                divmsg.Attributes.Add("class", "alert alert-danger fade in");
            }
        }

        protected void dlimglist_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                HiddenField hdnImageName = (HiddenField)e.Item.FindControl("hdnImageName");
                Image feedbackimg = (Image)e.Item.FindControl("feedbackimg");
                if (hdnImageName != null)
                {
                   //feedbackimg.ImageUrl = Server.MapPath("auditimage/" + hdnImageName.Value);
                }
            }

        }
    }
}