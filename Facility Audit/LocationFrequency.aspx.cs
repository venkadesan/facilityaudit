﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FacilityAudit.BLL;
using BL;
using System.Data;
using FacilityAudit.Code;
using System.IO;

namespace Facility_Audit
{
    public partial class LocationFrequency : System.Web.UI.Page
    {
        BLCategory objauditcategory = new BLCategory();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindCategory();
                BindLocations();
                //ddlcategory_SelectedIndexChanged(null, null);
            }
        }

        private void BindCategory()
        {
            try
            {
                DataSet dsCategory = new DataSet();
                dsCategory = objauditcategory.GetCategoryDet(0);
                if (dsCategory.Tables.Count > 0 && dsCategory.Tables[0].Rows.Count > 0)
                {
                    ddlcategory.DataSource = dsCategory.Tables[0];
                    ddlcategory.DataTextField = "categoryname";
                    ddlcategory.DataValueField = "categoryid";
                    ddlcategory.DataBind();
                }
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, "error");
            }
        }

        private void BindLocations()
        {
            try
            {
                int categoryid = 0;
                if (!Common.DDVal(ddlcategory, out categoryid))
                {
                    NotifyMessages("Select Category", "error");
                    return;
                }
                DataSet dsMapDet = new DataSet();
                dsMapDet = objauditcategory.GetFrequencyLocation(categoryid);
                if (dsMapDet.Tables.Count > 0 && dsMapDet.Tables[0].Rows.Count > 0)
                {
                    rptrlocations.DataSource = dsMapDet.Tables[0];
                    rptrlocations.DataBind();
                }
                else
                {
                    rptrlocations.DataSource = null;
                    rptrlocations.DataBind();
                }
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, "error");

            }
        }

        protected void btnsave_Click(object sender, EventArgs e)
        {
            try
            {
                if (rptrlocations.Items.Count > 0)
                {
                    int categoryid = 0;
                    if (!Common.DDVal(ddlcategory, out categoryid))
                    {
                        NotifyMessages("Select Category", "error");
                        return;
                    }
                    DataTable dtlocdet = new DataTable();
                    dtlocdet.Columns.Add("LocationId", typeof(int));
                    dtlocdet.Columns.Add("MonthId", typeof(int));
                    foreach (RepeaterItem rilocdet in rptrlocations.Items)
                    {
                        HiddenField hdnlocationid = (HiddenField)rilocdet.FindControl("hdnlocationid");
                        CheckBox chksave = (CheckBox)rilocdet.FindControl("chksave");
                        DropDownList ddlmoths = (DropDownList)rilocdet.FindControl("ddlmoths");
                        if (hdnlocationid != null && chksave != null && ddlmoths != null)
                        {
                            if (chksave.Checked)
                            {
                                int monthid = 0;
                                Common.DDVal(ddlmoths, out monthid);
                                dtlocdet.Rows.Add(Convert.ToInt32(hdnlocationid.Value), monthid);
                            }
                        }
                    }
                    if (dtlocdet.Rows.Count > 0)
                    {
                        string xmlString = string.Empty;
                        using (TextWriter writer = new StringWriter())
                        {
                            dtlocdet.TableName = "LocationDet";
                            dtlocdet.WriteXml(writer);
                            xmlString = writer.ToString();
                        }
                        string[] result = objauditcategory.InsertLocationFrequency(categoryid, xmlString);
                        if (result[1] != "-1")
                        {
                            BindLocations();
                            NotifyMessages(result[0], "info");
                        }
                        else
                        {
                            NotifyMessages(result[1], "error");
                        }
                    }
                    else
                    {
                        NotifyMessages("No Data Found to Save", "error");
                        return;
                    }

                }
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, "error");
            }
        }

        public void NotifyMessages(string message, string errortype)
        {
            divmsg.Visible = true;
            lblError.Text = message;
            if (errortype == "info")
            {
                divmsg.Attributes.Add("class", "alert alert-success fade in");
            }
            else if (errortype == "error")
            {
                divmsg.Attributes.Add("class", "alert alert-danger fade in");
            }
        }

        private bool GetSelectedValues(ListBox lstDetails, string colname, out string result)
        {
            try
            {
                result = string.Empty;
                int totalSelected = 0;
                DataTable dtSelectedValue = new DataTable(colname);
                dtSelectedValue.Columns.Add(colname + "ID", typeof(string));
                foreach (System.Web.UI.WebControls.ListItem li in lstDetails.Items)
                {
                    if (li.Selected == true)
                    {
                        //selectedvalues = selectedvalues + li.Value + ",";
                        dtSelectedValue.Rows.Add(li.Value);
                        totalSelected++;
                    }
                }
                string xmlString = string.Empty;
                using (TextWriter writer = new StringWriter())
                {
                    dtSelectedValue.WriteXml(writer);
                    xmlString = writer.ToString();
                }
                result = xmlString;
                return totalSelected > 0;
            }
            catch (Exception ex)
            {
                result = string.Empty;
                //NotifyMessages(ex.Message.ToString(), Common.ErrorType.Error);
                return false;
            }
        }

        protected void ddlcategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                lblError.Text = string.Empty;
                divmsg.Visible = false;

                int categoryid = 0;
                if (!Common.DDVal(ddlcategory, out categoryid))
                {
                    NotifyMessages("Select Category", "error");
                    return;
                }
                BindLocations();
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, "error");
            }
        }

        protected void rptrlocations_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    HiddenField hdnmonthid = (HiddenField)e.Item.FindControl("hdnmonthid");
                    DropDownList ddlmoths = (DropDownList)e.Item.FindControl("ddlmoths");
                    if (hdnmonthid != null && ddlmoths != null)
                    {
                        if (hdnmonthid.Value != "0")
                        {
                            ddlmoths.SelectedValue = hdnmonthid.Value;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, "error");
            }
        }
    }
}