﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BL;
using System.Data;

using System.IO;
using NPOI.HSSF.UserModel;
using NPOI.HPSF;
using NPOI.POIFS.FileSystem;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using NPOI.HSSF.Util;
using System.Text;
using Newtonsoft.Json;
using FacilityAudit.Code;

namespace Facility_Audit
{
    public partial class TopLevelReport1 : System.Web.UI.Page
    {
        BLTransaction objbltran = new BLTransaction();
        HSSFWorkbook hssfworkbook;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindDetails();
            }
        }

        private void BindDetails()
        {
            try
            {
                ClearDatas();
                divmsg.Visible = false;
                lblError.Text = string.Empty;
                DataSet dsreport = new DataSet();
                //dsreport = objbltran.GetSbuWiseScore(1, 2016, 1);
                dsreport = objbltran.GetSbuWiseScore(1, 2016, 1, UserSession.GroupID);
                if (dsreport.Tables.Count > 0 && dsreport.Tables[0].Rows.Count > 0 && dsreport.Tables[1].Rows.Count > 0)
                {
                    rptrsbuwise.DataSource = dsreport.Tables[0];
                    rptrsbuwise.DataBind();

                    rptroverall.DataSource = dsreport.Tables[1];
                    rptroverall.DataBind();
                }
                else
                {
                    rptrsbuwise.DataSource = null;
                    rptrsbuwise.DataBind();

                    rptroverall.DataSource = null;
                    rptroverall.DataBind();

                    NotifyMessages("No Data Found", "info");
                }
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, "error");
            }
        }

        public class Observations
        {
            public string JS_SCM { get; set; }
            public string JS_Others { get; set; }
            public string JS_HR { get; set; }
            public string JS_Complaince { get; set; }
            public string JS_Training { get; set; }
            public string JS_Operation { get; set; }
        }

        private void ClearDatas()
        {
            rptrsbuwise.DataSource = null;
            rptrsbuwise.DataBind();

            rptrlocationwise.DataSource = null;
            rptrlocationwise.DataBind();

            rptrdatewiserpt.DataSource = null;
            rptrdatewiserpt.DataBind();
        }

        public void NotifyMessages(string message, string errortype)
        {
            divmsg.Visible = true;
            lblError.Text = message;
            if (errortype == "info")
            {
                divmsg.Attributes.Add("class", "alert alert-success fade in");
            }
            else if (errortype == "error")
            {
                divmsg.Attributes.Add("class", "alert alert-danger fade in");
            }
        }

        protected void btnsbuwise_Click(object sender, EventArgs e)
        {
            try
            {
                divmsg.Visible = false;
                lblError.Text = string.Empty;
                DataSet dsreport = new DataSet();
                int sbuid = Convert.ToInt32(hdnsbuid.Value);
                //dsreport = objbltran.GetLocationwiseScore(sbuid, 1, 2016);
                dsreport = objbltran.GetLocationwiseScore(sbuid, 1, 2016, UserSession.GroupID);
                if (dsreport.Tables.Count > 0 && dsreport.Tables[0].Rows.Count > 0)
                {
                    rptrlocationwise.DataSource = dsreport.Tables[0];
                    rptrlocationwise.DataBind();

                    rptrdatewiserpt.DataSource = null;
                    rptrdatewiserpt.DataBind();
                }
                else
                {
                    rptrlocationwise.DataSource = null;
                    rptrlocationwise.DataBind();
                    NotifyMessages("No Data Found", "info");
                }
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, "error");
            }
        }

        protected void btnlocwise_Click(object sender, EventArgs e)
        {
            try
            {
                divmsg.Visible = false;
                lblError.Text = string.Empty;
                DataSet dsreport = new DataSet();
                int locationid = Convert.ToInt32(hdnlocationid.Value);
                dsreport = objbltran.GetDateWiseReportByLocation(locationid, 1, 2016);
                if (dsreport.Tables.Count > 0 && dsreport.Tables[0].Rows.Count > 0)
                {
                    rptrdatewiserpt.DataSource = dsreport.Tables[0];
                    rptrdatewiserpt.DataBind();
                }
                else
                {
                    rptrdatewiserpt.DataSource = null;
                    rptrdatewiserpt.DataBind();
                    NotifyMessages("No Data Found", "info");
                }
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, "error");
            }
        }

        public static int LoadImage(string path, HSSFWorkbook wb)
        {
            FileStream file = new FileStream(path, FileMode.Open, FileAccess.Read);
            byte[] buffer = new byte[file.Length];
            file.Read(buffer, 0, (int)file.Length);

            return wb.AddPicture(buffer, PictureType.JPEG);

        }

        MemoryStream WriteToStream()
        {
            //Write the stream data of workbook to the root directory
            MemoryStream file = new MemoryStream();
            hssfworkbook.Write(file);
            return file;
        }

        void InitializeWorkbook()
        {
            hssfworkbook = new HSSFWorkbook();

            ////create a entry of DocumentSummaryInformation
            DocumentSummaryInformation dsi = PropertySetFactory.CreateDocumentSummaryInformation();
            dsi.Company = "NPOI Team";
            hssfworkbook.DocumentSummaryInformation = dsi;

            ////create a entry of SummaryInformation
            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "NPOI SDK Example";
            hssfworkbook.SummaryInformation = si;
        }

        private string GetSiteID(string sitename)
        {
            string[] sitedet = sitename.Split('-');
            return sitedet[sitedet.Length - 1];
        }

        protected void rptrdatewiserpt_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "Download")
            {
                try
                {
                    HiddenField hdnlocationid = (HiddenField)e.Item.FindControl("locationid");
                    HiddenField hdnsbuid = (HiddenField)e.Item.FindControl("sbuid");
                    HiddenField hdnsectionid = (HiddenField)e.Item.FindControl("sectionid");
                    HiddenField hdnactualdate = (HiddenField)e.Item.FindControl("actualdate");
                    Label Score = (Label)e.Item.FindControl("Score");
                    if (hdnlocationid != null && hdnsbuid != null && hdnsectionid != null && hdnactualdate != null && Score != null)
                    {
                        string Getscored = Score.Text;
                        //string filename = "TopLevelDataReport.xls";
                        //Response.ContentType = "application/vnd.ms-excel";
                        //Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", filename));
                        //Response.Clear();

                        InitializeWorkbook();

                        using (FileStream file = new FileStream(Server.MapPath("AppThemes/images/downloaddata.xls"), FileMode.Open, FileAccess.Read))
                        {
                            hssfworkbook = new HSSFWorkbook(file);
                            file.Close();
                        }

                        //Sheet sheet = hssfworkbook.CreateSheet("Sheet1");
                        Sheet sheet = hssfworkbook.GetSheetAt(0);


                        int locationid = Convert.ToInt32(hdnlocationid.Value);
                        int sbuid = Convert.ToInt32(hdnsbuid.Value);
                        int sectorid = Convert.ToInt32(hdnsectionid.Value);
                        DateTime dtactualdate = Convert.ToDateTime(hdnactualdate.Value);
                        DataSet dsRawdatas = new DataSet();
                        dsRawdatas = objbltran.GetRawDatasDateWiseReport(dtactualdate, locationid, sbuid, sectorid, string.Empty);

                        GenerateExcel(dsRawdatas, Getscored);

                        //Font font1 = hssfworkbook.CreateFont();
                        //font1.FontHeightInPoints = 14;
                        ////font1.FontHeight = 14;
                        //font1.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.BOLD;
                        //CellStyle rwstyle = hssfworkbook.CreateCellStyle();
                        //rwstyle.Alignment = HorizontalAlignment.CENTER;
                        //rwstyle.ShrinkToFit = true;
                        //rwstyle.SetFont(font1);


                        //Font font2 = hssfworkbook.CreateFont();
                        //font2.FontHeightInPoints = 11;
                        //font2.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.BOLD;
                        //CellStyle rwstyle2 = hssfworkbook.CreateCellStyle();
                        //rwstyle2.ShrinkToFit = true;
                        //rwstyle2.SetFont(font2);

                        //Font font3 = hssfworkbook.CreateFont();
                        //font3.FontHeightInPoints = 11;
                        //CellStyle style1 = hssfworkbook.CreateCellStyle();
                        //style1.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.TAN.index;
                        //style1.ShrinkToFit = true;
                        //style1.FillPattern = FillPatternType.SOLID_FOREGROUND;

                        //style1.BorderBottom = CellBorderType.THIN;
                        //style1.BottomBorderColor = HSSFColor.BLACK.index;
                        //style1.BorderLeft = CellBorderType.THIN;
                        //style1.LeftBorderColor = HSSFColor.BLACK.index;
                        //style1.BorderRight = CellBorderType.THIN;
                        //style1.RightBorderColor = HSSFColor.BLACK.index;
                        //style1.BorderTop = CellBorderType.THIN;
                        //style1.TopBorderColor = HSSFColor.BLACK.index;

                        //style1.SetFont(font3);

                        //float fEnteirAvg = 0;
                        //int icount = 0;
                        //int iTotalScore = 0;
                        //int iTotalWeightage = 0;

                        //if (dsRawdatas.Tables.Count > 0 && dsRawdatas.Tables[0].Rows.Count > 0)
                        //{
                        //    DataTable dtAudit = dsRawdatas.Tables[0];
                        //    DataTable dtCategory = dsRawdatas.Tables[1];
                        //    DataTable dtRawData = dsRawdatas.Tables[2];
                        //    DataRow drHeader = dsRawdatas.Tables[3].Rows[0];

                        //    //Row rowimage = sheet.CreateRow(0);
                        //    //rowimage.CreateCell(0).SetCellValue(string.Empty);


                        //    //Row rowheader1 = sheet.CreateRow(1);
                        //    //// rowheader1.CreateCell(0).SetCellValue(string.Empty);
                        //    //Cell clheader1 = rowheader1.CreateCell(1);
                        //    //clheader1.SetCellValue("DUSTERS TOTAL SOLUTIONS SERVICES PVT LTD");
                        //    //clheader1.CellStyle = rwstyle;
                        //    //rowheader1.HeightInPoints = 40;



                        //    //HSSFPatriarch patriarch = (HSSFPatriarch)sheet.CreateDrawingPatriarch();
                        //    ////create the anchor
                        //    //HSSFClientAnchor anchor;
                        //    //anchor = new HSSFClientAnchor(0, 0, 0, 0, (short)0, 0, (short)0, 0);

                        //    //////anchor = new HSSFClientAnchor(20, 0, 35, 15, 0, 0, 1, 3);
                        //    ////anchor = new HSSFClientAnchor();
                        //    ////anchor.Col1 = 1;
                        //    ////anchor.Col2 = 1;
                        //    //anchor.AnchorType = (short)AnchorType.DONT_MOVE_AND_RESIZE;
                        //    ////anchor.AnchorType = 3;
                        //    ////load the picture and get the picture index in the workbook
                        //    //HSSFPicture picture = (HSSFPicture)patriarch.CreatePicture(anchor, LoadImage(Server.MapPath("AppThemes/images/dts.png"), hssfworkbook));
                        //    ////Reset the image to the original size.
                        //    //picture.Resize();
                        //    //////picture.LineStyle = HSSFPicture.LINESTYLE_DASHDOTGEL;



                        //    Row rowclient = sheet.CreateRow(7);
                        //    rowclient.CreateCell(0).SetCellValue(string.Empty);
                        //    rowclient.CreateCell(1).SetCellValue("Name of the Client");
                        //    rowclient.CreateCell(2).SetCellValue(drHeader["clientname"].ToString());
                        //    rowclient.CreateCell(3).SetCellValue(string.Empty);
                        //    rowclient.GetCell(1).CellStyle = style1;
                        //    rowclient.GetCell(2).CellStyle = style1;
                        //    rowclient.GetCell(3).CellStyle = style1;

                        //    Row rowsite = sheet.CreateRow(8);
                        //    rowsite.CreateCell(0).SetCellValue(string.Empty);
                        //    rowsite.CreateCell(1).SetCellValue("Name of the site");
                        //    rowsite.CreateCell(2).SetCellValue(drHeader["locsitename"].ToString());
                        //    rowsite.CreateCell(3).SetCellValue(string.Empty);
                        //    rowsite.GetCell(1).CellStyle = style1;
                        //    rowsite.GetCell(2).CellStyle = style1;
                        //    rowsite.GetCell(3).CellStyle = style1;

                        //    Row rowssa = sheet.CreateRow(9);
                        //    rowssa.CreateCell(0).SetCellValue(string.Empty);
                        //    rowssa.CreateCell(1).SetCellValue("Site ID");
                        //    rowssa.CreateCell(2).SetCellValue(GetSiteID(drHeader["locsitename"].ToString()));
                        //    rowssa.GetCell(1).CellStyle = style1;
                        //    rowssa.GetCell(2).CellStyle = style1;
                        //    rowssa.CreateCell(3).SetCellValue(string.Empty);
                        //    rowssa.GetCell(3).CellStyle = style1;

                        //    Row rowperson = sheet.CreateRow(10);
                        //    rowperson.CreateCell(0).SetCellValue(string.Empty);
                        //    rowperson.CreateCell(1).SetCellValue("Name of the Client person");
                        //    rowperson.CreateCell(2).SetCellValue(drHeader["clientperson"].ToString());
                        //    rowperson.GetCell(1).CellStyle = style1;
                        //    rowperson.GetCell(2).CellStyle = style1;
                        //    rowperson.CreateCell(3).SetCellValue(string.Empty);
                        //    rowperson.GetCell(3).CellStyle = style1;


                        //    Row rowsbu = sheet.CreateRow(11);
                        //    rowsbu.CreateCell(0).SetCellValue(string.Empty);
                        //    rowsbu.CreateCell(1).SetCellValue("Name of the SBU");
                        //    rowsbu.CreateCell(2).SetCellValue(drHeader["sbuname"].ToString());
                        //    rowsbu.GetCell(1).CellStyle = style1;
                        //    rowsbu.GetCell(2).CellStyle = style1;
                        //    rowsbu.CreateCell(3).SetCellValue(string.Empty);
                        //    rowsbu.GetCell(3).CellStyle = style1;


                        //    Row rowaom = sheet.CreateRow(12);
                        //    rowaom.CreateCell(0).SetCellValue(string.Empty);
                        //    rowaom.CreateCell(1).SetCellValue("Name of the OM / AOM");
                        //    rowaom.CreateCell(2).SetCellValue(drHeader["aom"].ToString());
                        //    rowaom.GetCell(1).CellStyle = style1;
                        //    rowaom.GetCell(2).CellStyle = style1;
                        //    rowaom.CreateCell(3).SetCellValue(string.Empty);
                        //    rowaom.GetCell(3).CellStyle = style1;


                        //    Row rowauditor = sheet.CreateRow(13);
                        //    rowauditor.CreateCell(0).SetCellValue(string.Empty);
                        //    rowauditor.CreateCell(1).SetCellValue("Name of the Auditor");
                        //    rowauditor.CreateCell(2).SetCellValue(drHeader["auditor"].ToString());
                        //    rowauditor.GetCell(1).CellStyle = style1;
                        //    rowauditor.GetCell(2).CellStyle = style1;
                        //    rowauditor.CreateCell(3).SetCellValue(string.Empty);
                        //    rowauditor.GetCell(3).CellStyle = style1;

                        //    Row rowAuditee = sheet.CreateRow(14);
                        //    rowAuditee.CreateCell(0).SetCellValue(string.Empty);
                        //    rowAuditee.CreateCell(1).SetCellValue("Name of the Auditee");
                        //    rowAuditee.CreateCell(2).SetCellValue(drHeader["auditee"].ToString());
                        //    rowAuditee.GetCell(1).CellStyle = style1;
                        //    rowAuditee.GetCell(2).CellStyle = style1;
                        //    rowAuditee.CreateCell(3).SetCellValue(string.Empty);
                        //    rowAuditee.GetCell(3).CellStyle = style1;


                        //    Row rowlastauitdate = sheet.CreateRow(15);
                        //    rowlastauitdate.CreateCell(0).SetCellValue(string.Empty);
                        //    rowlastauitdate.CreateCell(1).SetCellValue("Last audit date");
                        //    rowlastauitdate.CreateCell(2).SetCellValue(drHeader["displastauditdate"].ToString());
                        //    rowlastauitdate.CreateCell(3).SetCellValue("Current Audit Score:");
                        //    rowlastauitdate.GetCell(1).CellStyle = style1;
                        //    rowlastauitdate.GetCell(2).CellStyle = style1;
                        //    rowlastauitdate.GetCell(3).CellStyle = style1;


                        //    Row rowauitdate = sheet.CreateRow(16);
                        //    rowauitdate.CreateCell(0).SetCellValue(string.Empty);
                        //    rowauitdate.CreateCell(1).SetCellValue("Current audit date");
                        //    rowauitdate.CreateCell(2).SetCellValue(drHeader["dispauditdate"].ToString());
                        //    rowauitdate.CreateCell(3).SetCellValue("Last Audit Score :");
                        //    rowauitdate.GetCell(1).CellStyle = style1;
                        //    rowauitdate.GetCell(2).CellStyle = style1;
                        //    rowauitdate.GetCell(3).CellStyle = style1;


                        //    sheet.AutoSizeColumn(0);
                        //    sheet.AutoSizeColumn(1);
                        //    sheet.AutoSizeColumn(2);
                        //    sheet.AutoSizeColumn(3);
                        //    sheet.AutoSizeColumn(4);
                        //    sheet.AutoSizeColumn(5);
                        //    sheet.AutoSizeColumn(6);
                        //    sheet.AutoSizeColumn(7);
                        //    icount = 17;

                        //    for (int iauditcount = 0; iauditcount < dtAudit.Rows.Count; iauditcount++)
                        //    {
                        //        DataRow draudit = dtAudit.Rows[iauditcount];
                        //        int auditid = Convert.ToInt32(draudit["auditid"].ToString());
                        //        string auditname = draudit["auditname"].ToString();
                        //        Row rowAudit = sheet.CreateRow(icount);
                        //        rowAudit.CreateCell(0).SetCellValue(string.Empty);
                        //        rowAudit.GetCell(0).CellStyle = rwstyle;
                        //        Cell clAudit = rowAudit.CreateCell(1);
                        //        clAudit.SetCellValue(auditname);
                        //        clAudit.CellStyle = rwstyle;

                        //        //sheet.AddMergedRegion(new CellRangeAddress(icount, icount, 0, 4));

                        //        icount++;

                        //        Row rowheading = sheet.CreateRow(icount);
                        //        Cell clslno = rowheading.CreateCell(0);
                        //        clslno.SetCellValue("Sl.No");
                        //        Cell clheading1 = rowheading.CreateCell(1);
                        //        clheading1.SetCellValue("Observations");
                        //        //Cell clheading2 = rowheading.CreateCell(2);
                        //        //clheading2.SetCellValue("Weightage");
                        //        Cell clheading2 = rowheading.CreateCell(2);
                        //        clheading2.SetCellValue("Maximum Score");
                        //        Cell clheading3 = rowheading.CreateCell(3);
                        //        clheading3.SetCellValue("Score Obtained");
                        //        Cell clheading4 = rowheading.CreateCell(4);
                        //        clheading4.SetCellValue("Auditor remarks");
                        //        Cell clheading5 = rowheading.CreateCell(5);
                        //        clheading5.SetCellValue("Closure status & Remarks by SBU");

                        //        Cell climgheading6 = rowheading.CreateCell(6);
                        //        climgheading6.SetCellValue("Image");

                        //        clheading1.CellStyle = style1;
                        //        clheading2.CellStyle = style1;
                        //        clheading3.CellStyle = style1;
                        //        clheading4.CellStyle = style1;
                        //        clheading5.CellStyle = style1;
                        //        climgheading6.CellStyle = style1;
                        //        clslno.CellStyle = style1;

                        //        icount++;

                        //        int iscore = 0;
                        //        int iweightage = 0;
                        //        int iSingleAvg = 0;
                        //        int iTotalAvg = 0;
                        //        DataRow[] drCategory = dtCategory.Select("auditid=" + auditid);
                        //        for (int icategroryCount = 0; icategroryCount < drCategory.Length; icategroryCount++)
                        //        {
                        //            int categoryid = Convert.ToInt32(drCategory[icategroryCount]["categoryid"].ToString());
                        //            string categoryname = drCategory[icategroryCount]["categoryname"].ToString();

                        //            Row rowCategory = sheet.CreateRow(icount);
                        //            rowCategory.CreateCell(0).SetCellValue(icategroryCount + 1);
                        //            Cell clCategory = rowCategory.CreateCell(1);
                        //            clCategory.SetCellValue(categoryname);
                        //            clCategory.CellStyle = rwstyle2;
                        //            rowCategory.GetCell(0).CellStyle = rwstyle2;
                        //            icount++;

                        //            DataRow[] drRawDatas = dtRawData.Select("categoryid=" + categoryid);

                        //            int iCategoryScore = 0;
                        //            int iCategoryTotal = 0;

                        //            for (int iRawDatacount = 0; iRawDatacount < drRawDatas.Length; iRawDatacount++)
                        //            {
                        //                string question = drRawDatas[iRawDatacount]["auditqname"].ToString();
                        //                string remarks = drRawDatas[iRawDatacount]["remarks"].ToString();
                        //                int score = Convert.ToInt32(drRawDatas[iRawDatacount]["score"].ToString());
                        //                int weightage = Convert.ToInt32(drRawDatas[iRawDatacount]["weightage"].ToString());
                        //                string scorename = drRawDatas[iRawDatacount]["scorename"].ToString();
                        //                string imgFile = drRawDatas[iRawDatacount]["uploadfilename"].ToString();

                        //                int tScore = 0;

                        //                if (score == -1)
                        //                {
                        //                    score = 0;
                        //                    tScore = -1;
                        //                    weightage = 0;
                        //                }

                        //                iscore += score;
                        //                iweightage += weightage;

                        //                iCategoryScore += score;
                        //                iCategoryTotal += weightage;

                        //                iTotalScore += score;
                        //                iTotalWeightage += weightage;

                        //                iSingleAvg = score * weightage;
                        //                iTotalAvg += iSingleAvg;

                        //                Row rowRawData = sheet.CreateRow(icount);

                        //                Cell clrwSlNo = rowRawData.CreateCell(0);
                        //                clrwSlNo.SetCellValue(iRawDatacount + 1);

                        //                Cell clRawData1 = rowRawData.CreateCell(1);
                        //                clRawData1.SetCellValue(question);


                        //                //Cell clRawData2 = rowRawData.CreateCell(2);
                        //                //clRawData2.SetCellValue(weightage);

                        //                CellStyle stylecell = hssfworkbook.CreateCellStyle();
                        //                stylecell.ShrinkToFit = true;
                        //                stylecell.Alignment = HorizontalAlignment.FILL;

                        //                Cell clRawData2 = rowRawData.CreateCell(2);
                        //                if (tScore == 0)
                        //                {
                        //                    clRawData2.SetCellValue(" " + score);
                        //                    //clRawData2.CellStyle = stylecell;
                        //                }
                        //                else
                        //                {
                        //                    clRawData2.SetCellValue(scorename);

                        //                }


                        //                Cell clRawData3 = rowRawData.CreateCell(3);
                        //                clRawData3.SetCellValue(score);
                        //                if (tScore == -1)
                        //                {
                        //                    clRawData3.SetCellValue(scorename);
                        //                }

                        //                Cell clRawData4 = rowRawData.CreateCell(4);
                        //                clRawData4.SetCellValue(remarks);

                        //                Cell clRawData5 = rowRawData.CreateCell(5);
                        //                clRawData5.SetCellValue(string.Empty);

                        //                if (imgFile.Trim() != string.Empty)
                        //                {
                        //                    Cell clImgRow6 = rowRawData.CreateCell(6);
                        //                    clImgRow6.SetCellValue(imgFile);
                        //                    HSSFHyperlink link = new HSSFHyperlink(HyperlinkType.URL);
                        //                    link.Address = ("http://ifazility.com/facilityaudit/auditimage/" + imgFile);
                        //                    clImgRow6.Hyperlink = (link);
                        //                }


                        //                icount++;

                        //            }

                        //            Row rowCateTotal = sheet.CreateRow(icount);

                        //            Cell clCateTotal1 = rowCateTotal.CreateCell(0);
                        //            clCateTotal1.SetCellValue(string.Empty);

                        //            Cell clCateTotal2 = rowCateTotal.CreateCell(1);
                        //            clCateTotal2.SetCellValue(categoryname);

                        //            //Cell clCateTotal3 = rowCateTotal.CreateCell(2);
                        //            //clCateTotal3.SetCellValue(iweightage);

                        //            Cell clCateTotal3 = rowCateTotal.CreateCell(2);
                        //            clCateTotal3.SetCellValue(iCategoryTotal);

                        //            Cell clCateTotal4 = rowCateTotal.CreateCell(3);
                        //            clCateTotal4.SetCellValue(iCategoryScore);

                        //            Cell clCateTotal5 = rowCateTotal.CreateCell(4);
                        //            clCateTotal5.SetCellValue(string.Empty);

                        //            Cell clCateTotal6 = rowCateTotal.CreateCell(5);
                        //            clCateTotal6.SetCellValue(string.Empty);

                        //            clCateTotal1.CellStyle = style1;
                        //            clCateTotal2.CellStyle = style1;
                        //            clCateTotal3.CellStyle = style1;
                        //            clCateTotal4.CellStyle = style1;
                        //            clCateTotal5.CellStyle = style1;
                        //            clCateTotal6.CellStyle = style1;

                        //            icount++;


                        //        }

                        //        float iScoredAvg = 0;
                        //        iScoredAvg = ((float)(iTotalAvg * 100 / iweightage));

                        //        fEnteirAvg += iScoredAvg;

                        //        Row rowtotal = sheet.CreateRow(icount);

                        //        Cell cltotal1 = rowtotal.CreateCell(0);
                        //        cltotal1.SetCellValue(string.Empty);

                        //        Cell cltotal2 = rowtotal.CreateCell(1);
                        //        cltotal2.SetCellValue("Total Score for Documents & checklists");

                        //        //Cell cltotal3 = rowtotal.CreateCell(2);
                        //        //cltotal3.SetCellValue(iweightage);

                        //        Cell cltotal3 = rowtotal.CreateCell(2);
                        //        cltotal3.SetCellValue(iweightage);

                        //        Cell cltotal4 = rowtotal.CreateCell(3);
                        //        cltotal4.SetCellValue(iscore);

                        //        Cell cltotal5 = rowtotal.CreateCell(4);
                        //        cltotal5.SetCellValue(string.Empty);

                        //        Cell cltotal6 = rowtotal.CreateCell(5);
                        //        cltotal6.SetCellValue(string.Empty);
                        //        icount++;

                        //        Row rowavg = sheet.CreateRow(icount);

                        //        Cell clavg1 = rowavg.CreateCell(0);
                        //        cltotal1.SetCellValue(string.Empty);

                        //        Cell clavg2 = rowavg.CreateCell(1);
                        //        clavg2.SetCellValue("Percentage (%)");

                        //        Cell clavg3 = rowavg.CreateCell(2);
                        //        clavg3.SetCellValue(string.Empty);

                        //        Cell clavg4 = rowavg.CreateCell(3);
                        //        clavg4.SetCellValue(iScoredAvg + "%");

                        //        Cell clavg5 = rowavg.CreateCell(4);
                        //        clavg5.SetCellValue(string.Empty);

                        //        Cell clavg6 = rowavg.CreateCell(5);
                        //        clavg6.SetCellValue(string.Empty);


                        //        cltotal1.CellStyle = style1;
                        //        cltotal2.CellStyle = style1;
                        //        cltotal3.CellStyle = style1;
                        //        cltotal4.CellStyle = style1;
                        //        cltotal5.CellStyle = style1;
                        //        cltotal6.CellStyle = style1;


                        //        clavg1.CellStyle = style1;
                        //        clavg2.CellStyle = style1;
                        //        clavg3.CellStyle = style1;
                        //        clavg4.CellStyle = style1;
                        //        clavg5.CellStyle = style1;
                        //        clavg6.CellStyle = style1;


                        //        icount++;
                        //    }
                        //}

                        //float tAvg = fEnteirAvg / (dsRawdatas.Tables[0].Rows.Count);


                        //Row rowgrndTotal = sheet.CreateRow(icount);

                        //Cell clgrndTotal1 = rowgrndTotal.CreateCell(0);
                        //clgrndTotal1.SetCellValue(string.Empty);

                        //Cell clgrndTotal2 = rowgrndTotal.CreateCell(1);
                        //clgrndTotal2.SetCellValue("GRAND TOTAL SCORE");

                        ////Cell clgrndTotal3 = rowgrndTotal.CreateCell(2);
                        ////clgrndTotal3.SetCellValue(iTotalWeightage);

                        //Cell clgrndTotal3 = rowgrndTotal.CreateCell(2);
                        //clgrndTotal3.SetCellValue(iTotalWeightage);

                        //Cell clgrndTotal4 = rowgrndTotal.CreateCell(3);
                        //clgrndTotal4.SetCellValue(iTotalScore);

                        //Cell clgrndTotal5 = rowgrndTotal.CreateCell(4);
                        //clgrndTotal5.SetCellValue(string.Empty);

                        //Cell clgrndTotal6 = rowgrndTotal.CreateCell(5);
                        //clgrndTotal6.SetCellValue(string.Empty);

                        //icount++;

                        //Row rowgrndavg = sheet.CreateRow(icount);

                        //Cell clgrndavg1 = rowgrndavg.CreateCell(0);
                        //clgrndavg1.SetCellValue(string.Empty);


                        //Cell clgrndavg2 = rowgrndavg.CreateCell(1);
                        //clgrndavg2.SetCellValue("Percentage (%)");

                        ////Cell clgrndavg3 = rowgrndavg.CreateCell(2);
                        ////clgrndavg3.SetCellValue(string.Empty);

                        //Cell clgrndavg3 = rowgrndavg.CreateCell(2);
                        //clgrndavg3.SetCellValue(string.Empty);

                        ////Cell clgrndavg5 = rowgrndavg.CreateCell(4);
                        ////clgrndavg5.SetCellValue(tAvg + "%");

                        //Cell clgrndavg4 = rowgrndavg.CreateCell(3);
                        //clgrndavg4.SetCellValue(Getscored + "%");

                        //Cell clgrndavg5 = rowgrndavg.CreateCell(4);
                        //clgrndavg5.SetCellValue(string.Empty);

                        //Cell clgrndavg6 = rowgrndavg.CreateCell(5);
                        //clgrndavg6.SetCellValue(string.Empty);

                        //icount++;

                        //if (dsRawdatas.Tables[3].Rows.Count > 0)
                        //{
                        //    Row rowgrndfollowupdate = sheet.CreateRow(icount);

                        //    Cell clgrndfollowupdate1 = rowgrndfollowupdate.CreateCell(0);
                        //    clgrndfollowupdate1.SetCellValue(string.Empty);

                        //    Cell clgrndfollowupdate2 = rowgrndfollowupdate.CreateCell(1);
                        //    clgrndfollowupdate2.SetCellValue("Follow-Up Date");

                        //    //Cell clgrndfollowupdate3 = rowgrndfollowupdate.CreateCell(2);
                        //    //clgrndfollowupdate3.SetCellValue(string.Empty);

                        //    Cell clgrndfollowupdate3 = rowgrndfollowupdate.CreateCell(2);
                        //    clgrndfollowupdate3.SetCellValue(dsRawdatas.Tables[3].Rows[0]["dispFollowUpDate"].ToString());

                        //    //Cell clgrndfollowupdate5 = rowgrndfollowupdate.CreateCell(4);
                        //    //clgrndfollowupdate5.SetCellValue(tfollowupdate + "%");

                        //    Cell clgrndfollowupdate4 = rowgrndfollowupdate.CreateCell(3);
                        //    clgrndfollowupdate4.SetCellValue(string.Empty);

                        //    Cell clgrndfollowupdate5 = rowgrndfollowupdate.CreateCell(4);
                        //    clgrndfollowupdate5.SetCellValue(string.Empty);

                        //    Cell clgrndfollowupdate6 = rowgrndfollowupdate.CreateCell(5);
                        //    clgrndfollowupdate6.SetCellValue(string.Empty);
                        //}

                        //decimal scoreGot = Convert.ToDecimal(Getscored);
                        //CellStyle scoreColor = hssfworkbook.CreateCellStyle();
                        //if (scoreGot >= 83)
                        //{
                        //    scoreColor.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.GREEN.index;
                        //    scoreColor.FillPattern = FillPatternType.SOLID_FOREGROUND;
                        //    clgrndavg4.CellStyle = scoreColor;
                        //}
                        //else
                        //{
                        //    scoreColor.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.ORANGE.index;
                        //    scoreColor.FillPattern = FillPatternType.SOLID_FOREGROUND;
                        //    clgrndavg4.CellStyle = scoreColor;
                        //}



                        //sheet.AutoSizeColumn(0);
                        //sheet.AutoSizeColumn(1);
                        //sheet.AutoSizeColumn(2);
                        //sheet.AutoSizeColumn(3);
                        //sheet.AutoSizeColumn(4);
                        //sheet.AutoSizeColumn(5);
                        //sheet.AutoSizeColumn(6);


                        //Response.BinaryWrite(WriteToStream().GetBuffer());
                        //Response.End();
                    }
                }
                catch (Exception ex)
                {
                    NotifyMessages(ex.Message, "error");
                }

            }
        }

        protected void rptrlocationwise_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if ((e.Item.ItemType == ListItemType.Item) ||
                  (e.Item.ItemType == ListItemType.AlternatingItem))
                {
                    Label score = (Label)e.Item.FindControl("score");
                    Image imgicon = (Image)e.Item.FindControl("imgicon");
                    if (score != null && imgicon != null)
                    {
                        decimal dscore = Convert.ToDecimal(score.Text);
                        if (dscore >= 83)
                        {
                            imgicon.ImageUrl = "~/AppThemes/images/smile-icon5.png";
                        }
                        else if (dscore < 82 && dscore >= 75)
                        {
                            imgicon.ImageUrl = "~/AppThemes/images/smile-icon4.png";
                        }
                        else
                        {
                            imgicon.ImageUrl = "~/AppThemes/images/smile-icon1.png";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, "error");
            }
        }

        private void GenerateExcel(DataSet dsRawdatas, string Getscored)
        {
            try
            {
                StringBuilder sDatas = new StringBuilder();

                float fEnteirAvg = 0;
                int iTotalScore = 0;
                int iTotalWeightage = 0;
                int iMaxScore = 0;

                if (dsRawdatas.Tables.Count > 0 && dsRawdatas.Tables[0].Rows.Count > 0)
                {
                    sDatas.Append("<table width='100%' cellspacing='0' cellpadding='2' border = '1'>");
                    DataTable dtAudit = dsRawdatas.Tables[0];
                    DataTable dtCategory = dsRawdatas.Tables[1];
                    DataTable dtRawData = dsRawdatas.Tables[2];
                    DataRow drHeader = dsRawdatas.Tables[3].Rows[0];
                    sDatas.Append("<tr><td rowspan='5' style='border: none;' ><img style='height: 54px;width: 100px;' border='0' alt='Dusters' src='http://ifazility.com/facilityaudit/AppThemes/images/JLL%20New.png'></td><td rowspan='4' style='font-size: 24px;border: none;' align='center' colspan='6'>DUSTERS TOTAL SOLUTIONS SERVICES PVT LTD</td>");
                    sDatas.Append("<tr></tr>");
                    sDatas.Append("<tr></tr>");
                    sDatas.Append("<tr></tr>");
                    sDatas.Append("<tr></tr>");
                    sDatas.Append("<tr><td colspan='2'>Name of the Client</td><td colspan='3'>" + drHeader["clientname"].ToString() + "</td><td colspan='2'></td></tr>");
                    sDatas.Append("<tr><td colspan='2'>Name of the Site</td><td colspan='3'>" + drHeader["locsitename"].ToString() + "</td><td colspan='2'></td></tr>");
                    sDatas.Append("<tr><td colspan='2'>Site ID</td><td colspan='3'>" + GetSiteID(drHeader["locsitename"].ToString()) + "</td><td colspan='2'></td></tr>");
                    sDatas.Append("<tr><td colspan='2'>Name of the Client person</td><td colspan='3'>" + drHeader["clientperson"].ToString() + "</td><td colspan='2'></td></tr>");
                    sDatas.Append("<tr><td colspan='2'>Name of the SBU</td><td colspan='3'>" + drHeader["sbuname"].ToString() + "</td><td colspan='2'></td></tr>");
                    sDatas.Append("<tr><td colspan='2'>Name of the OM / AOM</td><td colspan='3'>" + drHeader["aom"].ToString() + "</td><td colspan='2'></td></tr>");
                    sDatas.Append("<tr><td colspan='2'>Name of the Auditor</td><td colspan='3'>" + drHeader["auditor"].ToString() + "</td><td colspan='2'></td></tr>");
                    sDatas.Append("<tr><td colspan='2'>Name of the Auditee</td><td colspan='3'>" + drHeader["auditee"].ToString() + "</td><td colspan='2'></td></tr>");
                    sDatas.Append("<tr><td colspan='2'>Last audit date</td><td align='left' colspan='3'>" + drHeader["displastauditdate"].ToString() + "</td><td colspan='2'>Last Audit Score</td></tr>");
                    sDatas.Append("<tr><td colspan='2'>Current audit date</td><td align='left' colspan='3'>" + drHeader["dispauditdate"].ToString() + "</td><td>Current Audit Score</td><td>" + Getscored + "%</td></tr>");

                    for (int iauditcount = 0; iauditcount < dtAudit.Rows.Count; iauditcount++)
                    {
                        DataRow draudit = dtAudit.Rows[iauditcount];
                        int auditid = Convert.ToInt32(draudit["auditid"].ToString());
                        string auditname = draudit["auditname"].ToString();
                        sDatas.Append("<tr><td align='center' style='font-size: 22px;' colspan='7'>" + auditname + "</td></tr>");
                        sDatas.Append("<tr><th style = 'background-color: #D20B0C;color:#ffffff'>Sl.No</th><th style = 'background-color: #D20B0C;color:#ffffff'>Observations</th><th style = 'background-color: #D20B0C;color:#ffffff;width: 5%;'>Maximum Score</th><th style = 'background-color: #D20B0C;color:#ffffff;width: 5%;'>Score Obtained</th><th style = 'background-color: #D20B0C;color:#ffffff;width: 30%;'>Auditor Remarks</th><th style = 'background-color: #D20B0C;color:#ffffff'>Remarks by SBU </th><th style = 'background-color: #D20B0C;color:#ffffff'>Image</th></tr>");
                        int iscore = 0;
                        int iweightage = 0;
                        int iSingleAvg = 0;
                        int iTotalAvg = 0;
                        int iAuditMax = 0;
                        DataRow[] drCategory = dtCategory.Select("auditid=" + auditid);
                        for (int icategroryCount = 0; icategroryCount < drCategory.Length; icategroryCount++)
                        {
                            int categoryid = Convert.ToInt32(drCategory[icategroryCount]["categoryid"].ToString());
                            string categoryname = drCategory[icategroryCount]["categoryname"].ToString();
                            sDatas.Append("<tr><td>" + (icategroryCount + 1) + "</td><td style='font-size: 18px;' colspan='6'>" + categoryname + "</td></tr>");
                            DataRow[] drRawDatas = dtRawData.Select("categoryid=" + categoryid);

                            int iCategoryScore = 0;
                            int iCategoryTotal = 0;
                            int iCatMax = 0;

                            for (int iRawDatacount = 0; iRawDatacount < drRawDatas.Length; iRawDatacount++)
                            {
                                string question = drRawDatas[iRawDatacount]["auditqname"].ToString();
                                string remarks = drRawDatas[iRawDatacount]["remarks"].ToString();
                                int score = Convert.ToInt32(drRawDatas[iRawDatacount]["score"].ToString());
                                int weightage = Convert.ToInt32(drRawDatas[iRawDatacount]["weightage"].ToString());
                                string scorename = drRawDatas[iRawDatacount]["scorename"].ToString();
                                string imgFile = drRawDatas[iRawDatacount]["uploadfilename"].ToString();
                                string filelink = string.Empty;
                                if (imgFile.Trim() != string.Empty)
                                {
                                    string[] imgfiles = imgFile.Split('#');
                                    foreach (string files in imgfiles)
                                    {
                                        if (files.Trim() != string.Empty)
                                        {
                                            imgFile = "http://ifazility.com/facilityaudit/auditimage/" + files;
                                            filelink += "<a href='" + imgFile + "'>" + imgFile + "</a><br/>";
                                        }
                                    }
                                }

                                int tScore = 0;
                                int MaxScore = 0;

                                if (score == -1)
                                {
                                    score = 0;
                                    tScore = -1;
                                    weightage = 0;
                                    MaxScore = 0;
                                }
                                else
                                {
                                    MaxScore = 1;
                                }

                                iscore += score;
                                iweightage += weightage;

                                iCategoryScore += score;
                                iCategoryTotal += weightage;

                                iTotalScore += score;
                                iTotalWeightage += weightage;

                                iSingleAvg = score * weightage;
                                iTotalAvg += iSingleAvg;

                                iMaxScore += MaxScore;
                                iCatMax += MaxScore;
                                iAuditMax += MaxScore;

                                string _score = string.Empty;
                                string _maxscore = string.Empty;
                                if (tScore == 0)
                                {
                                    _score = " " + score;
                                    _maxscore = " " + MaxScore;
                                }
                                else
                                {
                                    _score = scorename;
                                    _maxscore = scorename;
                                }
                                sDatas.Append("<tr><td>" + (iRawDatacount + 1).ToString() + "</td><td>" + question + "</td><td style='width: 5%;' align='center'>" + _maxscore + "</td><td  style='width: 5%;'  align='center'>" + _score + "</td><td>" + remarks + "</td><td style='width: 30%;'></td><td>" + filelink + "</td></tr>");
                            }
                            sDatas.Append("<tr style='font-weight:bold;font-size: 18px;'><td colspan='2'>" + categoryname + "</td><td align='center'>" + iCatMax + "</td><td align='center'>" + iCategoryScore + "</td><td></td><td></td><td></td></tr>");
                        }
                        float iScoredAvg = 0;
                        iScoredAvg = ((float)(iTotalAvg * 100 / iweightage));
                        fEnteirAvg += iScoredAvg;

                        sDatas.Append("<tr style='font-weight:bold;font-size: 18px;'><td colspan='2'>Total Score for " + auditname + "</td><td align='center'>" + iAuditMax + "</td><td align='center'>" + iscore + "</td><td></td><td></td><td></td></tr>");
                        sDatas.Append("<tr style='font-weight:bold;font-size: 18px;'><td colspan='2'>Percentage (%)</td><td></td><td align='center'>" + iScoredAvg + "%" + "</td><td></td><td></td><td></td></tr>");
                    }
                    float tAvg = fEnteirAvg / (dsRawdatas.Tables[0].Rows.Count);

                    sDatas.Append("<tr style='font-weight:bold;font-size: 18px;'><td colspan='2'>GRAND TOTAL SCORE</td><td align='center'>" + iMaxScore + "</td><td align='center'>" + iTotalScore + "</td><td></td><td></td><td></td></tr>");

                    decimal scoreGot = Convert.ToDecimal(Getscored);
                    string setColour = string.Empty;
                    if (scoreGot >= 83)
                    {
                        setColour = "<td style='background-color: #00FF00;' align='center'>" + Getscored + "%" + "</td>";
                    }
                    else
                    {
                        setColour = "<td style='background-color: #FFBE00;' align='center'>" + Getscored + "%" + "</td>";
                    }
                    sDatas.Append("<tr style='font-weight:bold;font-size: 18px;'><td colspan='2'>Percentage (%)</td><td></td>" + setColour + "<td></td><td></td><td></td></tr>");
                    if (dsRawdatas.Tables[3].Rows.Count > 0)
                    {
                        sDatas.Append("<tr style='font-weight:bold;font-size: 18px;'><td colspan='2'>Follow-Up Date</td><td></td><td>" + dsRawdatas.Tables[3].Rows[0]["dispFollowUpDate"].ToString() + "</td><td></td><td></td><td></td></tr>");
                    }


                    string observations = drHeader["observation"].ToString();
                    string clientremarks = drHeader["feedback"].ToString();
                    observations = observations.Replace('[', ' ');
                    observations = observations.Replace(']', ' ');
                    Observations objobservation = JsonConvert.DeserializeObject<Observations>(observations);
                    //                string observationdet = "1. Operations : " + objobservation.JS_Operation + "break" +
                    //"2. Training  : " + objobservation.JS_Training + " break  " +
                    //"3. SCM  : " + objobservation.JS_SCM + " break " +
                    //"4. Compliance  : " + objobservation.JS_Complaince + "break" +
                    //"5. HR  : " + objobservation.JS_HR + "break " +
                    //"6. Others : " + objobservation.JS_Operation;
                    if (objobservation != null)
                    {
                        sDatas.Append("<tr><td align='center' style='font-size: 22px;' colspan='7'>Other Observations</td></tr>");
                        sDatas.Append("<tr style='font-weight:bold;font-size: 15px;'><td colspan='2'>Operations</td><td colspan='5'>" + objobservation.JS_Operation + "</td></tr>");
                        sDatas.Append("<tr style='font-weight:bold;font-size: 15px;'><td colspan='2'>Training</td><td colspan='5'>" + objobservation.JS_Training + "</td></tr>");
                        sDatas.Append("<tr style='font-weight:bold;font-size: 15px;'><td colspan='2'>SCM</td><td colspan='5'>" + objobservation.JS_SCM + "</td></tr>");
                        sDatas.Append("<tr style='font-weight:bold;font-size: 15px;'><td colspan='2'>Compliance</td><td colspan='5'>" + objobservation.JS_Complaince + "</td></tr>");
                        sDatas.Append("<tr style='font-weight:bold;font-size: 15px;'><td colspan='2'>HR</td><td colspan='5'>" + objobservation.JS_HR + "</td></tr>");
                        sDatas.Append("<tr style='font-weight:bold;font-size: 15px;'><td colspan='2'>Others</td><td colspan='5'>" + objobservation.JS_Operation + "</td></tr>");
                    }
                    sDatas.Append("<tr><td align='center' style='font-size: 22px;' colspan='7'>Client Feedback</td></tr>");
                    sDatas.Append("<tr><td  colspan='7'>" + clientremarks + "</td></tr>");



                    sDatas.Append("</table>");
                    //dvData.InnerHtml = sDatas.ToString();
                    //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "callexport", "<script type='text/javascript'>PrintExce();</script>", false);

                    Response.Clear();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment;filename=Toplevelreport1.xls");
                    Response.Charset = "";
                    Response.ContentType = "application/vnd.ms-excel";
                    // string style = @"<style> .textmode { } </style>";
                    //Response.Write(style);
                    Response.Output.Write(sDatas.ToString());
                    Response.Flush();
                    Response.End();
                }
            }
            catch (Exception ex)
            {
            }
        }
    }
}