﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FacilityAudit.BLL;
using FacilityAudit.Code;
using System.Text;
using BL;

namespace Facility_Audit
{
    public partial class Audit : System.Web.UI.Page
    {
        #region Properties
        private bool IsAdd
        {
            get
            {
                if (!ID.HasValue)
                    return false;
                else if (ID.Value != 0)
                    return false;
                else
                    return true;
            }
        }


        private int? ID
        {
            get
            {
                if (ViewState["id"] == null)
                    ViewState["id"] = 0;
                return (int?)ViewState["id"];
            }

            set
            {
                ViewState["id"] = value;
            }
        }

        private bool DisableStatus
        {
            get
            {
                if (ViewState["disablestatus"] == null)
                    ViewState["disablestatus"] = 0;
                return (bool)ViewState["disablestatus"];
            }

            set
            {
                ViewState["disablestatus"] = value;
            }
        }

        #endregion
        BLGroupcompany objgrpcmp = new BLGroupcompany();
        BLAudit objaudit = new  BLAudit();
        BLGroupcompany objgrp = new BLGroupcompany();
        BLCategory objcat = new BLCategory();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                loadsector();
                loadrprtdetails();
                ButtonStatus(Common.ButtonStatus.Cancel);
            }
        }

        private void ButtonStatus(Common.ButtonStatus status)
        {
            this.txtaudname.Text =  this.txtdes.Text = string.Empty;
            //ddlgroup.ClearSelection();

            if (status == Common.ButtonStatus.New)
            {
                this.pnlnew.Visible = true;
                this.bNew.Enabled = false;
                this.bNew.Visible = false;
                this.linewbtn.Visible = false;
                this.btnsave.Enabled = true;
                this.txtaudname.Focus();
                this.lblError.Text = "";
                divmsg.Visible = false;
            }
            else if (status == Common.ButtonStatus.Edit)
            {
                this.txtaudname.Focus();
                this.pnlnew.Visible = true;
                this.bNew.Enabled = false;
                this.bNew.Visible = false;
                this.linewbtn.Visible = false;
                this.btnsave.Enabled = true;
                this.lblError.Text = "";
                divmsg.Visible = false;
            }
            else if (status == Common.ButtonStatus.Cancel)
            {
                this.pnlnew.Visible = false;
                this.bNew.Visible = true;
                this.bNew.Enabled = true;
                this.linewbtn.Visible = true;
                this.btnsave.Enabled = false;
                this.ID = 0;
                divrep.Visible = true;
                loadrprtdetails();
                this.lblError.Text = "";
                divmsg.Visible = false;
            }
            else if (status == Common.ButtonStatus.Save)
            {
                this.pnlnew.Visible = false;
                this.bNew.Visible = true;
                this.bNew.Enabled = true;
                this.linewbtn.Visible = true;
                this.btnsave.Enabled = false;
                this.ID = 0;
                divrep.Visible = true;
                loadrprtdetails();
            }
        }

        public void NotifyMessages(string message, Common.ErrorType et)
        {
            divmsg.Visible = true;
            lblError.Text = message;
            if (et == Common.ErrorType.Error)
            {

            }
            else if (et == Common.ErrorType.Information)
            {

            }
        }

        protected void bNew_Click(object sender, EventArgs e)
        {
            ButtonStatus(Common.ButtonStatus.New);
        }

        protected void btnsave_Click(object sender, EventArgs e)
        {
            try
            {
                DateTime startdate = Convert.ToDateTime(this.txtstrtdate.Text.Trim());
                DateTime enddate = Convert.ToDateTime(this.txtenddate.Text.Trim());
                int sectorid = 0;
                Common.DDVal(ddlsector, out sectorid);
                if (this.txtaudname.Text == string.Empty)
                {
                    NotifyMessages("Enter Audit name", Common.ErrorType.Error);
                    return;
                }
                if (this.txtstrtdate.Text == string.Empty)
                {
                    NotifyMessages("Select Startdate", Common.ErrorType.Error);
                    return;
                }
                if (this.txtaudname.Text == string.Empty)
                {
                    NotifyMessages("Select Enddate", Common.ErrorType.Error);
                    return;
                }
                if (this.IsAdd)
                {
                    if (objaudit.saveaudit(sectorid, this.txtaudname.Text.Trim(), this.txtdes.Text.Trim(), startdate, enddate, UserSession.UserID, DateTime.Now) > 0)
                    {
                        NotifyMessages("Saved successfully", Common.ErrorType.Information);
                    }
                    else
                    {
                        NotifyMessages("Auditname already exist", Common.ErrorType.Information);
                        return;
                    }
                }
                else if (this.ID != null && this.ID.Value > 0)
                {
                    if (objaudit.updateaudit(this.ID.Value, sectorid, this.txtaudname.Text.Trim(), this.txtdes.Text.Trim(), startdate, enddate, UserSession.UserID, DateTime.Now) > 0)
                    {
                        NotifyMessages("Updated successfully", Common.ErrorType.Information);
                    }
                    else
                    {
                        NotifyMessages("Auditname already exist", Common.ErrorType.Information);
                        return;
                    }
                }

                ButtonStatus(Common.ButtonStatus.Save);
            }
            catch { }
        }

        private void loadsector()
        {
            try
            {
                DataSet ds = new DataSet();
                //ds = objgrp.getsector();
                ds = objgrp.GetSectorwithGroup(UserSession.GroupID);
                 if (ds.Tables[0].Rows.Count > 0)
                {
                    ddlsector.DataSource = ds;
                    ddlsector.DataTextField = "locationsectorname";
                    ddlsector.DataValueField = "locationsectorid";
                    ddlsector.DataBind();
                    UserSession.sectorid = Convert.ToInt32(this.ddlsector.Items[0].Value);
                }
            }
            catch { }
        }
          
        private void loadrprtdetails()
        {
            try
            {
                DataSet ds = new DataSet();
                int sectorid = 0;
                Common.DDVal(ddlsector, out sectorid);
                ds = objaudit.getmaudit(0, 0, sectorid);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    divrep.Visible = true;
                    rptaudit.DataSource = ds;
                    rptaudit.DataBind();
                }
                else
                {
                    divrep.Visible = false;
                }
            }
            catch { }
        }

        protected void rptaudit_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Edit")
                {
                    ButtonStatus(Common.ButtonStatus.Edit);
                    this.ID = Convert.ToInt32(e.CommandArgument);
                    BindToCtrls();
                }
                else if (e.CommandName == "Delete")
                {
                    this.ID = Convert.ToInt32(e.CommandArgument);
                    DataSet ds = new DataSet();
                    ds = objcat.getmauditcategory(this.ID.Value, 0, 0);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        NotifyMessages("Cannot be delete as reference exist", Common.ErrorType.Information);
                        return;
                    }
                    else
                    {
                        objaudit.deletemaudit(this.ID.Value);
                        ButtonStatus(Common.ButtonStatus.Cancel);
                        NotifyMessages("Deleted successfully", Common.ErrorType.Information);
                    }
                }
            }
            catch { }
        }

        public void BindToCtrls()
        {
            try
            {
                DataSet ds = new DataSet();
                ds = objaudit.getmaudit(this.ID.Value,1,UserSession.sectorid);

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    this.txtaudname.Text = ds.Tables[0].Rows[0]["auditname"].ToString();
                    this.txtstrtdate.Text = ds.Tables[0].Rows[0]["startdate"].ToString().Replace(","," ");
                    this.txtenddate.Text = ds.Tables[0].Rows[0]["enddate"].ToString().Replace(",","");
                    this.txtdes.Text = ds.Tables[0].Rows[0]["auditdescription"].ToString();
                }
            }
            catch { }
        }

        protected void rptaudit_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

        }

        protected void btncncl_Click(object sender, EventArgs e)
        {
            ButtonStatus(Common.ButtonStatus.Cancel);
        }

        protected void ddlsector_SelectedIndexChanged(object sender, EventArgs e)
        {
            UserSession.sectorid = Convert.ToInt32(this.ddlsector.SelectedItem.Value);

            this.ddlsector.ClearSelection();
            this.ddlsector.Items.FindByValue(UserSession.sectorid.ToString()).Selected = true;
            loadrprtdetails();
        }
    }
}