﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FacilityAudit.BLL;
using BL;
using System.Data;
using FacilityAudit.Code;

namespace Facility_Audit
{
    public partial class Category : System.Web.UI.Page
    {
        #region Properties
        private bool IsAdd
        {
            get
            {
                if (!ID.HasValue)
                    return false;
                else if (ID.Value != 0)
                    return false;
                else
                    return true;
            }
        }


        private int? ID
        {
            get
            {
                if (ViewState["id"] == null)
                    ViewState["id"] = 0;
                return (int?)ViewState["id"];
            }

            set
            {
                ViewState["id"] = value;
            }
        }

        private bool DisableStatus
        {
            get
            {
                if (ViewState["disablestatus"] == null)
                    ViewState["disablestatus"] = 0;
                return (bool)ViewState["disablestatus"];
            }

            set
            {
                ViewState["disablestatus"] = value;
            }
        }

        #endregion

        BLAudit objaudit = new BLAudit();
        BLCategory objauditcategory = new BLCategory();
        BLQuestion objq = new BLQuestion();
        BLGroupcompany objgrp = new BLGroupcompany();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                loadrprtdetails();
                ButtonStatus(Common.ButtonStatus.Cancel);
            }
        }



        private void ButtonStatus(Common.ButtonStatus status)
        {
            this.txtcatname.Text = txtnoofMonths.Text = string.Empty;
            //ddlgroup.ClearSelection();

            if (status == Common.ButtonStatus.New)
            {
                this.pnlnew.Visible = true;
                this.bNew.Enabled = false;
                this.bNew.Visible = false;
                this.btnsave.Enabled = true;
                this.txtcatname.Focus();
                this.lblError.Text = "";
                this.divmsg.Visible = false;
                this.linewbtn.Visible = false;
            }
            else if (status == Common.ButtonStatus.Edit)
            {
                this.txtcatname.Focus();
                this.pnlnew.Visible = true;
                this.bNew.Enabled = false;
                this.bNew.Visible = false;
                this.btnsave.Enabled = true;
                this.lblError.Text = "";
                this.divmsg.Visible = false;
                this.linewbtn.Visible = false;
            }
            else if (status == Common.ButtonStatus.Cancel)
            {
                this.pnlnew.Visible = false;
                this.bNew.Visible = true;
                this.bNew.Enabled = true;
                this.btnsave.Enabled = false;
                this.ID = 0;
                divrep.Visible = true;
                loadrprtdetails();
                this.lblError.Text = "";
                this.divmsg.Visible = false;
                this.linewbtn.Visible = true;
            }
            else if (status == Common.ButtonStatus.Save)
            {
                this.pnlnew.Visible = false;
                this.bNew.Visible = true;
                this.bNew.Enabled = true;
                this.btnsave.Enabled = false;
                this.ID = 0;
                divrep.Visible = true;
                this.linewbtn.Visible = true;
                loadrprtdetails();
            }
        }

        public void NotifyMessages(string message, Common.ErrorType et)
        {
            divmsg.Visible = true;
            lblError.Text = message;
            if (et == Common.ErrorType.Error)
            {

            }
            else if (et == Common.ErrorType.Information)
            {

            }
        }

        protected void bNew_Click(object sender, EventArgs e)
        {
            ButtonStatus(Common.ButtonStatus.New);
        }

        protected void btnsave_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.txtcatname.Text == string.Empty)
                {
                    NotifyMessages("Enter Category name", Common.ErrorType.Error);
                    return;
                }
                if (this.txtnoofMonths.Text == string.Empty)
                {
                    NotifyMessages("Enter No of Months", Common.ErrorType.Error);
                    return;
                }
                int noOfMonths = 0;
                if (txtnoofMonths.Text.Trim() != string.Empty)
                {
                    noOfMonths = Convert.ToInt32(txtnoofMonths.Text.Trim());
                }
                //string[] result = objauditcategory.InsertUpdateCategory(Convert.ToInt32(this.ID), txtcatname.Text.Trim(), UserSession.UserID, noOfMonths);
                string[] result = objauditcategory.InsertUpdateCategory_withGroup(Convert.ToInt32(this.ID), txtcatname.Text.Trim(), UserSession.UserID, noOfMonths,UserSession.GroupID);
                if (result[1] != "-1")
                {
                    loadrprtdetails();
                    ButtonStatus(Common.ButtonStatus.Cancel);
                    NotifyMessages(result[0], Common.ErrorType.Information);
                }
                else
                {
                    NotifyMessages(result[0], Common.ErrorType.Error);
                }

                ButtonStatus(Common.ButtonStatus.Save);
            }
            catch { }
        }


        private void loadrprtdetails()
        {
            try
            {
                DataSet ds = new DataSet();
                //ds = objauditcategory.GetCategoryDet(0);
                ds = objauditcategory.GetCategoryDet_withGroup(0, UserSession.GroupID);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    divrep.Visible = true;
                    rptrcat.DataSource = ds;
                    rptrcat.DataBind();
                }
                else
                {
                    divrep.Visible = false;
                }
            }
            catch { }
        }

        protected void rptrcat_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Edit")
                {
                    ButtonStatus(Common.ButtonStatus.Edit);
                    this.ID = Convert.ToInt32(e.CommandArgument);
                    BindToCtrls();
                }
                //else if (e.CommandName == "Delete")
                //{
                //    this.ID = Convert.ToInt32(e.CommandArgument);
                //    DataSet dp = new DataSet();
                //    dp = objq.getmauditquestion(Convert.ToInt32(this.ID.Value), 0, 0);
                //    if (dp.Tables[0].Rows.Count > 0)
                //    {
                //        NotifyMessages("Cannot be delete as reference exist", Common.ErrorType.Error);
                //        return;
                //    }
                //    else
                //    {
                //        objauditcategory.deletemauditcategory(this.ID.Value);
                //        ButtonStatus(Common.ButtonStatus.Cancel);
                //        NotifyMessages("Deleted successfully", Common.ErrorType.Information);
                //    }
                //}
            }
            catch { }
        }

        public void BindToCtrls()
        {
            try
            {
                DataSet ds = new DataSet();
                ds = objauditcategory.GetCategoryDet(this.ID.Value);

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    this.txtcatname.Text = ds.Tables[0].Rows[0]["CategoryName"].ToString();
                    this.txtnoofMonths.Text = ds.Tables[0].Rows[0]["noOfMonths"].ToString();
                }
            }
            catch { }
        }



        protected void btncncl_Click(object sender, EventArgs e)
        {
            ButtonStatus(Common.ButtonStatus.Cancel);
        }

    }
}