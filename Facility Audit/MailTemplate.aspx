﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Mainpage.Master" AutoEventWireup="true"
    CodeBehind="MailTemplate.aspx.cs" Inherits="Facility_Audit.MailTemplate" %>

<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="col-md-11">
        <div id="crumbs">
            <ul>
                <li class="active"><a href="#">Masters</a></li>
                <li><a href="#">Mail Template</a></li>
            </ul>
        </div>
    </div>
    <div class="main">
        <div class="row">
            <div id="Div1" class="row margin-vertical-15" runat="server">
                <div class="col-md-12">
                    <div class="alert alert-danger" id="divmsg" visible="false" runat="server">
                        <button type="button" class="close" data-dismiss="alert">
                            ×</button>
                        <asp:Label runat="server" ID="lblError"></asp:Label>
                    </div>
                </div>
                <br />
                <div class="row">
                    <div class="col-lg-12">
                        <div class="col-md-3">
                            <div class="tile cornered">
                                <!-- tile header -->
                                <div class="tile-header color dutch">
                                    <h1 class="small-bold">
                                        Mail Parameters
                                    </h1>
                                </div>
                                <!-- /tile header -->
                                <!-- tile body -->
                                <div class="tile-body">
                                    <ul class="list-group no-bottom-margin">
                                        <li class="list-group-item">
                                            <asp:Label ID="Label13" runat="server">#SbuName#</asp:Label>
                                        </li>
                                        <%-- <li class="list-group-item">
                                            <asp:Label ID="Label3" runat="server">#Operations#</asp:Label>
                                        </li>
                                        <li class="list-group-item">
                                            <asp:Label ID="Label4" runat="server">#Training#</asp:Label>
                                        </li>
                                        <li class="list-group-item">
                                            <asp:Label ID="Label5" runat="server">#SCM#</asp:Label>
                                        </li>
                                        <li class="list-group-item">
                                            <asp:Label ID="Label6" runat="server">#Compliance#</asp:Label>
                                        </li>
                                        <li class="list-group-item">
                                            <asp:Label ID="Label7" runat="server">#HR#</asp:Label>
                                        </li>
                                        <li class="list-group-item">
                                            <asp:Label ID="Label8" runat="server">#Others#</asp:Label>
                                        </li>
                                        <li class="list-group-item">
                                            <asp:Label ID="Label9" runat="server">#Score#</asp:Label>
                                        </li>
                                        <li class="list-group-item">
                                            <asp:Label ID="Label11" runat="server">#Closure#</asp:Label>
                                        </li>
                                        <li class="list-group-item">
                                            <asp:Label ID="Label12" runat="server">#Follow#</asp:Label>
                                        </li>
                                        <li class="list-group-item">
                                            <asp:Label ID="Label14" runat="server">#Location#</asp:Label>
                                        </li>--%>
                                        <li class="list-group-item">
                                            <asp:Label ID="Label15" runat="server">#SbuName#</asp:Label>
                                            <asp:Label ID="Label3" runat="server">#towername#</asp:Label>
                                            <asp:Label ID="Label4" runat="server">#Score#</asp:Label>
                                            <asp:Label ID="Label5" runat="server">#Operations#</asp:Label>
                                            <asp:Label ID="Label6" runat="server">#Training#</asp:Label>
                                            <asp:Label ID="Label10" runat="server">#SCM#</asp:Label>
                                            <asp:Label ID="Label9" runat="server">#Compliance#</asp:Label>
                                            <asp:Label ID="Label8" runat="server">#HR#</asp:Label>
                                            <asp:Label ID="Label7" runat="server">#Others#</asp:Label>
                                            <asp:Label ID="Label11" runat="server">#feedback#</asp:Label>
                                            <asp:Label ID="Label12" runat="server">#Closure#</asp:Label>
                                            <asp:Label ID="Label14" runat="server">#Follow# </asp:Label>
                                            <asp:Label ID="Label17" runat="server">#AuditDate# </asp:Label>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <!-- tile body -->
                            <h4 style="text-decoration: underline;">
                                Mail Settings</h4>
                            <div class="tile-body">
                                <div class="row">
                                    <!--col-6-->
                                    <div class="col-md-12">
                                        <div class="tile-body">
                                            <div id="Div2" runat="server" class="form-horizontal">
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label" for="">
                                                        <asp:Label Text="Subject" runat="server" ID="Label1" />:</label>
                                                    <div class="col-sm-10">
                                                        <asp:TextBox ID="txtsubject" runat="server" class="form-control parsley-validated"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div id="Div3" class="form-group" runat="server">
                                                    <label class="col-sm-2 control-label" for="">
                                                        <asp:Label Text="Body" runat="server" ID="Label2" />:</label>
                                                    <div class="col-sm-10">
                                                        <CKEditor:CKEditorControl ID="txtbody" BasePath="~/ckeditor/" RemovePlugins="elementspath"
                                                            runat="server" ToolbarFull="Bold|Italic|Underline|Strike|-|Subscript|Superscript NumberedList|BulletedList|-|Outdent|Indent|Blockquote|CreateDiv
JustifyLeft|JustifyCenter|JustifyRight|JustifyBlock
BidiLtr|BidiRtl
Link|Unlink|Anchor
Image|Flash|Table|HorizontalRule|Smiley|SpecialChar|PageBreak|Iframe
/
Styles|Format|Font|FontSize
TextColor|BGColor
Maximize|ShowBlocks|" Height="200"></CKEditor:CKEditorControl>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- end col 6 -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
