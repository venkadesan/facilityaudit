﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FacilityAudit.BLL;
using FacilityAudit.Code;
using System.Text;
using BL;

namespace Facility_Audit
{
    public partial class Weightage : System.Web.UI.Page
    {

        #region Properties
        private bool IsAdd
        {
            get
            {
                if (!ID.HasValue)
                    return false;
                else if (ID.Value != 0)
                    return false;
                else
                    return true;
            }
        }


        private int? ID
        {
            get
            {
                if (ViewState["id"] == null)
                    ViewState["id"] = 0;
                return (int?)ViewState["id"];
            }

            set
            {
                ViewState["id"] = value;
            }
        }

        private bool DisableStatus
        {
            get
            {
                if (ViewState["disablestatus"] == null)
                    ViewState["disablestatus"] = 0;
                return (bool)ViewState["disablestatus"];
            }

            set
            {
                ViewState["disablestatus"] = value;
            }
        }

        #endregion

        BLAudit objaudit = new BLAudit();
        BLGroupcompany objgrp = new BLGroupcompany();
        BLAuditRating objauditrating = new BLAuditRating();
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                loadsector();
                loadaudit();
                ButtonStatus(Common.ButtonStatus.Cancel);
            }
        }

        private void loadsector()
        {
            try
            {
                DataSet ds = new DataSet();
                //ds = objgrp.getsector();
                ds = objgrp.GetSectorwithGroup(UserSession.GroupID);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddlsector.DataSource = ds;
                    ddlsector.DataTextField = "locationsectorname";
                    ddlsector.DataValueField = "locationsectorid";
                    ddlsector.DataBind();
                    UserSession.sectorid = Convert.ToInt32(this.ddlsector.Items[0].Value);
                }
            }
            catch { }
        }
        private void ButtonStatus(Common.ButtonStatus status)
        {
            this.txtweightage.Text = string.Empty;
            //ddlgroup.ClearSelection();


            if (status == Common.ButtonStatus.New)
            {
                this.pnlnew.Visible = true;
                this.bNew.Enabled = false;
                this.bNew.Visible = false;
                this.btnsave.Enabled = true;
                this.txtweightage.Focus();
                this.lblError.Text = "";
                this.divmsg.Visible = false;
                this.linewbtn.Visible = false;
            }
            else if (status == Common.ButtonStatus.Edit)
            {
                this.txtweightage.Focus();
                this.pnlnew.Visible = true;
                this.bNew.Enabled = false;
                this.bNew.Visible = false;
                this.btnsave.Enabled = true;
                this.lblError.Text = "";
                this.linewbtn.Visible = false;
                this.divmsg.Visible = false;
            }
            else if (status == Common.ButtonStatus.Cancel)
            {
                this.pnlnew.Visible = false;
                this.bNew.Visible = true;
                this.bNew.Enabled = true;
                this.btnsave.Enabled = false;
                this.ID = 0;
                divrep.Visible = true;
                loadrprtdetails();
                this.lblError.Text = "";
                this.linewbtn.Visible = true;
                this.divmsg.Visible = false;
            }
            else if (status == Common.ButtonStatus.Save)
            {
                this.pnlnew.Visible = false;
                this.bNew.Visible = true;
                this.bNew.Enabled = true;
                this.btnsave.Enabled = false;
                this.ID = 0;
                divrep.Visible = true;
                this.linewbtn.Visible = true;
                loadrprtdetails();
            }
        }
        public void NotifyMessages(string message, Common.ErrorType et)
        {
            divmsg.Visible = true;
            lblError.Text = message;
            if (et == Common.ErrorType.Error)
            {

            }
            else if (et == Common.ErrorType.Information)
            {

            }
        }
        protected void bNew_Click(object sender, EventArgs e)
        {
            ButtonStatus(Common.ButtonStatus.New);
        }


        protected void btnsave_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.txtweightage.Text == string.Empty)
                {
                    NotifyMessages("Enter Weightage", Common.ErrorType.Error);
                    return;
                }
                if (this.IsAdd)
                {
                    if (objauditrating.addauditweightage(Convert.ToInt32(ddlaudit.SelectedValue), Convert.ToInt32(this.txtweightage.Text.Trim()), 1, DateTime.Now) > 0)
                    {
                        NotifyMessages("Saved successfully", Common.ErrorType.Information);
                    }
                    else
                    {
                        NotifyMessages("Weightage already exist", Common.ErrorType.Information);
                        return;
                    }
                }
                else if (this.ID != null && this.ID.Value > 0)
                {
                    if (objauditrating.updatemauditweightage(Convert.ToInt32(ddlaudit.SelectedValue), this.ID.Value, Convert.ToInt32(this.txtweightage.Text.Trim()), 1, DateTime.Now) > 0)
                    {
                        NotifyMessages("Updated successfully", Common.ErrorType.Information);
                    }
                    else
                    {
                        NotifyMessages("Weightage already exist", Common.ErrorType.Information);
                        return;
                    }
                }

                ButtonStatus(Common.ButtonStatus.Save);
            }
            catch { }
        }
        private void loadaudit()
        {
            try
            {
                DataSet ds = new DataSet();
                ds = objaudit.getmaudit(0, 0,UserSession.sectorid);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddlaudit.DataSource = ds;
                    ddlaudit.DataTextField = "auditname";
                    ddlaudit.DataValueField = "auditid";
                    ddlaudit.DataBind();
                }

            }
            catch { }
        }
        private void loadrprtdetails()
        {
            try
            {

                DataSet ds = new DataSet();
                ds = objauditrating.getmauditweightage(Convert.ToInt32(ddlaudit.SelectedValue), 0,0);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    divrep.Visible = true;
                    rptrweightage.DataSource = ds;
                    rptrweightage.DataBind();
                }
                else
                {
                    divrep.Visible = false;
                }
            }
            catch { }
        }

        protected void rptrweightage_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Edit")
                {
                    ButtonStatus(Common.ButtonStatus.Edit);
                    this.ID = Convert.ToInt32(e.CommandArgument);
                    BindToCtrls();
                }
                else if (e.CommandName == "Delete")
                {
                    this.ID = Convert.ToInt32(e.CommandArgument);
                    objauditrating.deletemauditweightage(this.ID.Value);
                    ButtonStatus(Common.ButtonStatus.Cancel);
                    NotifyMessages("Deleted successfully", Common.ErrorType.Information);
                    loadrprtdetails();

                }
            }
            catch { }


        }
        public void BindToCtrls()
        {

            try
            {
                DataSet ds = new DataSet();
                ds = objauditrating.getmauditweightage(Convert.ToInt32(ddlaudit.SelectedValue), this.ID.Value,1);

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    ddlaudit.SelectedValue = ds.Tables[0].Rows[0]["auditid"].ToString();
                    this.txtweightage.Text = ds.Tables[0].Rows[0]["weightage"].ToString();


                }
            }
            catch { }
        }

        protected void rptrweightage_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

        }

        protected void btncncl_Click(object sender, EventArgs e)
        {
            ButtonStatus(Common.ButtonStatus.Cancel);
        }

        protected void ddlaudit_SelectedIndexChanged(object sender, EventArgs e)
        {
            //loadrprtdetails();
            ButtonStatus(Common.ButtonStatus.Cancel);
        }

        protected void ddlsector_SelectedIndexChanged(object sender, EventArgs e)
        {
            UserSession.sectorid = Convert.ToInt32(this.ddlsector.SelectedItem.Value);

            this.ddlsector.ClearSelection();
            this.ddlsector.Items.FindByValue(UserSession.sectorid.ToString()).Selected = true;
            loadaudit();
            loadrprtdetails();
        }

    }
}