﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Mainpage.Master" AutoEventWireup="true" CodeBehind="ClientFeedbackReport.aspx.cs" Inherits="Facility_Audit.ClientFeedbackReport" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </cc1:ToolkitScriptManager>
    <div class="col-md-11">
        <div id="crumbs">
            <ul>
                <li class="active" style="width: 250px;"><a href="#">Client-Feedback Report</a></li>
            </ul>
        </div>
    </div>
    <div class="main" style="margin-left: 20px;">
        <div class=" col-md-5 col-xs-offset-4 alert alert-big alert-danger alert-dismissable fade in margin-top-10"
            runat="server" id="divmsg" visible="false">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                ×</button>
            <asp:Label runat="server" ID="lblError"></asp:Label>
        </div>
        <div class="row">
            <div class="form-box-content">
                <div class="row border-bottom margin-vertical-15">
                    <div class="col-md-8 col-xs-offset-4">
                        <div id="basicvalidations" class="form-horizontal">
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="">
                                </label>
                                <label class="col-sm-1 control-label" for="">
                                    From</label>
                                <div class="col-sm-3">
                                    <asp:TextBox ID="txtfrmdate" runat="server" class="form-control parsley-validated"></asp:TextBox>
                                    <cc1:CalendarExtender ID="calfromdate" runat="server" TargetControlID="txtfrmdate"
                                        Format="MM/dd/yyyy">
                                    </cc1:CalendarExtender>
                                </div>
                                <label class="col-sm-1 control-label margin-left15" for="">
                                    To</label>
                                <div class="col-sm-3">
                                    <asp:TextBox ID="txttodate" runat="server" class="form-control parsley-validated"></asp:TextBox>
                                    <cc1:CalendarExtender ID="caltodate" runat="server" TargetControlID="txttodate" Format="MM/dd/yyyy">
                                    </cc1:CalendarExtender>
                                </div>
                                <div class="col-sm-1">
                                    <asp:LinkButton ID="lnksearch" Style="text-decoration: none;" class="fa fa-2x fa-refresh"
                                        ToolTip="Refresh" runat="server" OnClick="lnksearch_Click"></asp:LinkButton>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <asp:LinkButton ID="lnkdownload" Style="float: right; text-decoration: none;" class="fa fa-2x fa-download"
                ToolTip="Export To Excel" runat="server" OnClick="lnkdownload_Click"></asp:LinkButton>
            <div class="row">
                <asp:Label ID="lbltotalscore" Style="font-weight: bold; font-size: 13px;" runat="server"></asp:Label>
                <asp:Panel ID="Panel1" runat="server" Height="100%" Width="100%">
                    <div class="col-md-12" id="divrep" runat="server">
                        <div class="tile-body nopadding margin-top-10">
                            <div class="table-responsive">
                                <asp:Repeater ID="rptrdatewiserpt" DataSourceID="" runat="server" >
                                    <HeaderTemplate>
                                        <table width="100%" border="0" cellspacing="0" class="table table-bordered" cellpadding="0">
                                            <tbody>
                                                <tr class="color green">
                                                    <th width="5%">
                                                        Sl.No
                                                    </th>
                                                    <th>
                                                        Date
                                                    </th>
                                                    <th>
                                                        Customer
                                                    </th>
                                                    <th>
                                                        Location
                                                    </th>                                                   
                                                    <th>
                                                        SBU
                                                    </th>                                                   
                                                    <th>
                                                        Client Name
                                                    </th>
                                                    <th>
                                                        Score
                                                    </th>                                                  
                                                   
                                                </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td>
                                                <asp:Label ID="Label2" runat="server" Text='<%#(((RepeaterItem)Container).ItemIndex+1).ToString() %>' />
                                            </td>
                                            <td>
                                                <asp:Label ID="date" runat="server" Text='<%# Eval("FeedbackDate") %>' />
                                            </td>
                                            <th>
                                                <asp:Label ID="customername" runat="server" Text='<%# Eval("Company") %>' />
                                            </th>
                                            <td>
                                                <asp:Label ID="LocationName" runat="server" Text='<%# Eval("Client") %>' />
                                            </td>
                                           
                                            <td>
                                                <asp:Label ID="sbuname" runat="server" Text='<%# Eval("SBU") %>' />
                                            </td>
                                           
                                            <td>
                                                <asp:Label ID="clientname" runat="server" Text='<%# Eval("clientname") %>' />
                                            </td>
                                            <td>
                                                <asp:Label ID="Score" runat="server" Text='<%# Eval("Score") %>' />
                                            </td>                                    
                                          
                                           
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <!-- tile body -->
                <!-- /tile body -->
            </div>
        </div>
    </div>
</asp:Content>
