﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ImageLists.aspx.cs" Inherits="Facility_Audit.ImageLists" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>iFazig - Facility Audit</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="UTF-8" />
    <link rel="icon" type="image/ico" href="AppThemes/images/favicon.ico" />
    <!-- Bootstrap -->
    <link href="AppThemes/css/bootstrap.min.css" rel="stylesheet" />
    <link href="AppThemes/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" href="AppThemes/css/animate.min.css" />
    <link rel="stylesheet" href="AppThemes/css/dataTables.bootstrap.css" />
    <link rel="stylesheet" href="AppThemes/css/chosen.min.css" />
    <link rel="stylesheet" href="AppThemes/css/chosen-bootstrap.css" />
    <link rel="stylesheet" href="AppThemes/css/ColVis.css" />
    <link rel="stylesheet" href="AppThemes/css/TableTools.css" />
    <link href="AppThemes/css/minoral.css" rel="stylesheet" />
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <script src="AppThemes/scripts/FusionCharts.js" type="text/javascript"></script>
    <script src="http://www.google-analytics.com/ga.js" type="text/javascript"></script>
</head>
<body class="brownish-scheme">
    <form id="Form1" if="form1" runat="server">
    <!-- Wrap all page content here -->
    <div id="wrap">
        <!-- Make page fluid -->
        <div class="row">
            <!-- Fixed navbar -->
            <div class="navbar navbar-default navbar-fixed-top" role="navigation">
                <!-- Branding -->
                <div class="navbar-header col-md-2">
                    <a class="navbar-brand" href="#">
                        <img src="AppThemes/images/ifazilit-audit.png" />
                    </a>
                    <div class="sidebar-collapse">
                        <a href="#"><i class="fa fa-bars"></i></a>
                    </div>
                </div>
                <!-- Branding end -->
                <!-- .nav-collapse -->
                <div class="navbar-collapse">
                    <!-- Content collapsing at 768px to sidebar -->
                    <div class="collapsing-content">
                        <!-- Search -->
                        <!-- Search -->
                        <div class="search" id="selectbox">
                        </div>
                        <div class="search" runat="server" visible="true" id="Div1">
                        </div>
                        <!-- Search end -->
                        <!-- User Controls -->
                        <div class="user-controls">
                            <ul>
                                <li></li>
                                <li></li>
                                <li></li>
                                <li class="settings"></li>
                            </ul>
                        </div>
                        <!-- User Controls end -->
                    </div>
                    <!-- /Content collapsing at 768px to sidebar -->
                    <!-- Sidebar -->
                    <ul class="nav navbar-nav side-nav" id="navigation">
                        <li class="collapsed-content">
                            <!-- Collapsed content pasting here at 768px -->
                        </li>
                        <li id="user-status">
                            <div>
                                <img style="padding-top: 2px;" src="AppThemes1/images/image_dusters_small.png" alt="" />
                            </div>
                        </li>
                        <asp:Literal ID="ltrlMenu" runat="server"></asp:Literal>
                    </ul>
                    <!-- Sidebar end -->
                </div>
                <!--/.nav-collapse -->
            </div>
            <!-- Fixed navbar end -->
            <!-- Page content -->
            <div id="content" class="col-md-12">
                <div class="main">
                    <div class=" col-md-5 col-xs-offset-4 alert alert-big alert-danger alert-dismissable fade in margin-top-10"
                        runat="server" id="divmsg" visible="false">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                            ×</button>
                        <asp:Label runat="server" ID="lblError"></asp:Label>
                    </div>
                    <div class="row">
                        <div class="row">
                            <div class="col-md-12" id="divrep" runat="server">
                                <div class="tile-body nopadding margin-top-10">
                                    <h4>
                                        <asp:Label ID="lblquestionn" Text="question" runat="server"></asp:Label></h4>
                                    <asp:DataList BorderStyle="None" ID="dlimglist" CssClass="table  table-responsive"
                                        RepeatColumns="4" RepeatDirection="Horizontal" runat="server" OnItemDataBound="dlimglist_ItemDataBound">
                                        <ItemTemplate>
                                            <asp:HiddenField runat="server" ID="hdnImageName" Value='<%# Eval("ImageName")%>' />
                                            <asp:Image ID="feedbackimg" ImageUrl='<%# Eval("ImageName", GetUrl("auditimage/"+"{0}")) %>' Width="200px" Height="150px" runat="server" />
                                        </ItemTemplate>
                                    </asp:DataList>
                                </div>
                            </div>
                            <!-- tile body -->
                            <!-- /tile body -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- /content container -->
        </div>
        <!-- Page content end -->
    </div>
    <footer class="footer">
    <div class="container">
        <p style="margin-left:450px;"  class="text-muted">Copyright 2016 by i2i Softwares Pvt Ltd.</p>
    </div>
</footer>
    <!-- Make page fluid-->
    <!-- Wrap all page content end -->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://code.jquery.com/jquery.js" type="text/javascript"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js" type="text/javascript"></script>
    <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css&amp;skin=sons-of-obsidian"
        type="text/javascript"></script>
    <script src="js/plugins/jquery.nicescroll.min.js" type="text/javascript"></script>
    <script src="js/plugins/blockui/jquery.blockUI.js" type="text/javascript"></script>
    <script src="js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
    <script src="js/plugins/chosen/chosen.jquery.min.js" type="text/javascript"></script>
    <script src="js/minoral.min.js" type="text/javascript"></script>
    <script type="text/javascript">

        $(function () {

            //chosen select input
            $(".chosen-select").chosen({ disable_search_threshold: 10 });

        })
      
    </script>
    </form>
</body>
</html>
