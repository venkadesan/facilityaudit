﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BL;
using System.Data;
using System.IO;
using NPOI.HSSF.UserModel;
using NPOI.HPSF;
using NPOI.POIFS.FileSystem;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using NPOI.HSSF.Util;
using System.Text;
using Newtonsoft.Json;

using OfficeOpenXml;
using OfficeOpenXml.Drawing;
using OfficeOpenXml.Style;
using System.Drawing;
using OfficeOpenXml.Drawing.Chart;
using System.Configuration;

using FacilityAudit.Code;

namespace Facility_Audit
{
    public partial class Test : System.Web.UI.Page
    {
        BLTransaction objbltran = new BLTransaction();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindYears();
                //BindDetails();
                //BindRegionWise();
                BindOverall();
            }
        }

        private void BindOverall()
        {
            try
            {
                ClearDatas();
                //DataTable dtRegion = new DataTable(); ;
                //dtRegion.Columns.Add("RegionId", typeof(int));
                //dtRegion.Columns.Add("Region", typeof(string));
                //dtRegion.Columns.Add("Score", typeof(decimal));

                //dtRegion.Rows.Add(1, "North", 68.4);
                //dtRegion.Rows.Add(2, "East", 40.7);
                //dtRegion.Rows.Add(3, "West", 79.4);
                //dtRegion.Rows.Add(4, "South", 98.4);
                loverall.Text = string.Empty;
                lregionwise.Text = string.Empty;
                //lsbuwise.Text = string.Empty;
                llocationwise.Text = string.Empty;
                lrawdata.Text = string.Empty;
                regiondiv.Visible = false;
                DataSet dsreport = new DataSet();
                int month = 0, year = 0;
                Common.DDVal(ddlmonth, out month);
                Common.DDVal(ddlyear, out year);
                dsreport = objbltran.GetRegionWiseScore(month, year, UserSession.GroupID);

                if (dsreport.Tables[1].Rows.Count > 0)
                {
                    rptroverall.DataSource = dsreport.Tables[1];
                    rptroverall.DataBind();

                    loverall.Text = "Overall";
                }
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, "error");
            }
        }

        private void BindRegionWise()
        {
            try
            {
                ClearDatas();
                //DataTable dtRegion = new DataTable(); ;
                //dtRegion.Columns.Add("RegionId", typeof(int));
                //dtRegion.Columns.Add("Region", typeof(string));
                //dtRegion.Columns.Add("Score", typeof(decimal));

                //dtRegion.Rows.Add(1, "North", 68.4);
                //dtRegion.Rows.Add(2, "East", 40.7);
                //dtRegion.Rows.Add(3, "West", 79.4);
                //dtRegion.Rows.Add(4, "South", 98.4);
                loverall.Text = string.Empty;
                lregionwise.Text = string.Empty;
                //lsbuwise.Text = string.Empty;
                llocationwise.Text = string.Empty;
                lrawdata.Text = string.Empty;
                DataSet dsreport = new DataSet();
                int month = 0, year = 0;
                Common.DDVal(ddlmonth, out month);
                Common.DDVal(ddlyear, out year);
                regiondiv.Visible = false;
                dsreport = objbltran.GetRegionWiseScore(month, year, UserSession.GroupID);
                if (dsreport.Tables.Count > 0 && dsreport.Tables[0].Rows.Count > 0)
                {
                    rptrregion.DataSource = dsreport.Tables[0];
                    rptrregion.DataBind();

                    regiondiv.Visible = true;

                    lregionwise.Text = "Region - SBU Wise";
                }
                if (dsreport.Tables[1].Rows.Count > 0)
                {
                    rptroverall.DataSource = dsreport.Tables[1];
                    rptroverall.DataBind();

                    loverall.Text = "Overall";
                }
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, "error");
            }
        }

        private void BindYears()
        {
            try
            {
                int year = DateTime.Now.Year;
                for (int icount = (year - 1); icount < (year + 2); icount++)
                {
                    ddlyear.Items.Add(new ListItem(icount.ToString(), icount.ToString()));
                }

                ddlyear.SelectedValue = DateTime.Now.Year.ToString();
                ddlmonth.SelectedValue = DateTime.Now.Month.ToString();
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, "error");
            }
        }

        private void BindDetails(int regionid, Repeater rptrsbuwise)
        {
            try
            {

                //lsbuwise.Text = string.Empty;
                llocationwise.Text = string.Empty;
                lrawdata.Text = string.Empty;
                divmsg.Visible = false;
                llocationwise.Text = string.Empty;
                lrawdata.Text = string.Empty;
                lblError.Text = string.Empty;

                rptrsbuwise.DataSource = null;
                rptrsbuwise.DataBind();

                rptrlocationwise.DataSource = null;
                rptrlocationwise.DataBind();

                rptrdatewiserpt.DataSource = null;
                rptrdatewiserpt.DataBind();

                DataSet dsreport = new DataSet();
                int month = 0, year = 0;
                Common.DDVal(ddlmonth, out month);
                Common.DDVal(ddlyear, out year);
                dsreport = objbltran.GetSbuWiseScore(month, year, regionid, UserSession.GroupID);
                if (dsreport.Tables.Count > 0 && dsreport.Tables[0].Rows.Count > 0)
                {

                    rptrsbuwise.DataSource = dsreport.Tables[0];
                    rptrsbuwise.DataBind();


                    //lsbuwise.Text = "SBU - Wise";
                }
                else
                {
                    lrawdata.Text = string.Empty;
                    loverall.Text = string.Empty;
                    //lsbuwise.Text = string.Empty;
                    rptrsbuwise.DataSource = null;
                    rptrsbuwise.DataBind();

                    NotifyMessages("No Data Found", "info");
                }
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, "error");
            }
        }

        public class Observations
        {
            public string JS_SCM { get; set; }
            public string JS_Others { get; set; }
            public string JS_HR { get; set; }
            public string JS_Complaince { get; set; }
            public string JS_Training { get; set; }
            public string JS_Operation { get; set; }
        }

        private void ClearDatas()
        {
            rptrregion.DataSource = null;
            rptrregion.DataBind();


            //rptrsbuwise.DataSource = null;
            //rptrsbuwise.DataBind();

            rptrlocationwise.DataSource = null;
            rptrlocationwise.DataBind();

            rptrdatewiserpt.DataSource = null;
            rptrdatewiserpt.DataBind();
        }

        public void NotifyMessages(string message, string errortype)
        {
            divmsg.Visible = true;
            lblError.Text = message;
            if (errortype == "info")
            {
                divmsg.Attributes.Add("class", "alert alert-success fade in");
            }
            else if (errortype == "error")
            {
                divmsg.Attributes.Add("class", "alert alert-danger fade in");
            }
        }

        protected void btnoverall_Click(object sender, EventArgs e)
        {
            try
            {
                BindRegionWise();
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, "error");
            }
        }

        protected void btnsbuwise_Click(object sender, EventArgs e)
        {
            try
            {

                llocationwise.Text = string.Empty;
                lrawdata.Text = string.Empty;

                divmsg.Visible = false;
                lblError.Text = string.Empty;
                lrawdata.Text = string.Empty;

                DataSet dsreport = new DataSet();
                int month = 0, year = 0;
                Common.DDVal(ddlmonth, out month);
                Common.DDVal(ddlyear, out year);
                int sbuid = Convert.ToInt32(hdnsbuid.Value);
                dsreport = objbltran.GetLocationwiseScore(sbuid, month, year, UserSession.GroupID);
                if (dsreport.Tables.Count > 0 && dsreport.Tables[0].Rows.Count > 0)
                {
                    rptrlocationwise.DataSource = dsreport.Tables[0];
                    rptrlocationwise.DataBind();

                    rptrdatewiserpt.DataSource = null;
                    rptrdatewiserpt.DataBind();

                    llocationwise.Text = "Location - Wise";
                }
                else
                {
                    llocationwise.Text = string.Empty;
                    rptrlocationwise.DataSource = null;
                    rptrlocationwise.DataBind();

                    rptrdatewiserpt.DataSource = null;
                    rptrdatewiserpt.DataBind();
                    NotifyMessages("No Data Found", "info");
                }
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, "error");
            }
        }

        protected void btnregionwise_Click(object sender, EventArgs e)
        {
            try
            {
                int regionid = Convert.ToInt32(hdnregionid.Value);
                //BindDetails(regionid);
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, "error");
            }
        }
        protected void btnlocwise_Click(object sender, EventArgs e)
        {
            try
            {

                lrawdata.Text = string.Empty;

                divmsg.Visible = false;
                lblError.Text = string.Empty;
                DataSet dsreport = new DataSet();
                int locationid = Convert.ToInt32(hdnlocationid.Value);
                int month = 0, year = 0;
                Common.DDVal(ddlmonth, out month);
                Common.DDVal(ddlyear, out year);
                dsreport = objbltran.GetDateWiseReportByLocation(locationid, month, year);
                if (dsreport.Tables.Count > 0 && dsreport.Tables[0].Rows.Count > 0)
                {
                    rptrdatewiserpt.DataSource = dsreport.Tables[0];
                    rptrdatewiserpt.DataBind();

                    lrawdata.Text = "Date Wise";
                }
                else
                {
                    rptrdatewiserpt.DataSource = null;
                    rptrdatewiserpt.DataBind();

                    lrawdata.Text = string.Empty;
                    NotifyMessages("No Data Found", "info");
                }
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, "error");
            }
        }



        private string GetSiteID(string sitename)
        {
            string[] sitedet = sitename.Split('-');
            return sitedet[sitedet.Length - 1];
        }

        protected void rptrdatewiserpt_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "Download")
            {
                try
                {
                    HiddenField hdnlocationid = (HiddenField)e.Item.FindControl("locationid");
                    HiddenField hdnsbuid = (HiddenField)e.Item.FindControl("sbuid");
                    HiddenField hdnsectionid = (HiddenField)e.Item.FindControl("sectionid");
                    HiddenField hdnactualdate = (HiddenField)e.Item.FindControl("actualdate");
                    Label Score = (Label)e.Item.FindControl("Score");
                    if (hdnlocationid != null && hdnsbuid != null && hdnsectionid != null && hdnactualdate != null && Score != null)
                    {
                        string Getscored = Score.Text;
                        //string filename = "TopLevelDataReport.xls";
                        //Response.ContentType = "application/vnd.ms-excel";
                        //Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", filename));
                        //Response.Clear();


                        int locationid = Convert.ToInt32(hdnlocationid.Value);
                        int sbuid = Convert.ToInt32(hdnsbuid.Value);
                        int sectorid = Convert.ToInt32(hdnsectionid.Value);
                        DateTime dtactualdate = Convert.ToDateTime(hdnactualdate.Value);
                        DataSet dsRawdatas = new DataSet();
                        dsRawdatas = objbltran.GetRawDatasDateWiseReport(dtactualdate, locationid, sbuid, sectorid,string.Empty);


                        MakeExcel(dsRawdatas, Getscored);


                    }
                }
                catch (Exception ex)
                {
                    NotifyMessages(ex.Message, "error");
                }

            }
        }


        protected void rptrlocationwise_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if ((e.Item.ItemType == ListItemType.Item) ||
                  (e.Item.ItemType == ListItemType.AlternatingItem))
                {
                    Label score = (Label)e.Item.FindControl("score");
                    HiddenField hdnregionid = (HiddenField)e.Item.FindControl("hdnregionid");
                    Repeater rptrsbuwise = (Repeater)e.Item.FindControl("rptrsbuwise");
                    System.Web.UI.WebControls.Image imgicon = (System.Web.UI.WebControls.Image)e.Item.FindControl("imgicon");
                    if (score != null && imgicon != null)
                    {
                        decimal dscore = Convert.ToDecimal(score.Text);
                        if (dscore >= 83)
                        {
                            imgicon.ImageUrl = "~/AppThemes/images/smile-icon5.png";
                        }
                        else if (dscore < 82 && dscore >= 75)
                        {
                            imgicon.ImageUrl = "~/AppThemes/images/smile-icon4.png";
                        }
                        else
                        {
                            imgicon.ImageUrl = "~/AppThemes/images/smile-icon1.png";
                        }
                    }

                    Label location = (Label)e.Item.FindControl("location");
                    if (location != null)
                    {
                        string locname = location.Text;
                        if (locname.Length > 25)
                        {
                            string[] Splitname = locname.Split('-');
                            string displocname = Splitname[0].Substring(0, Math.Min(Splitname[0].Length, 25));
                            if (Splitname.Length > 1)
                            {
                                displocname += " - " + Splitname[Splitname.Length - 1];
                            }
                            location.Text = displocname;
                        }
                    }

                    if (hdnregionid != null && rptrsbuwise != null)
                    {
                        int regionid = Convert.ToInt32(hdnregionid.Value);
                        BindDetails(regionid, rptrsbuwise);
                    }
                }
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, "error");
            }
        }

        private void GenerateExcel(DataSet dsRawdatas, string Getscored)
        {
            try
            {
                StringBuilder sDatas = new StringBuilder();

                float fEnteirAvg = 0;
                int iTotalScore = 0;
                int iTotalWeightage = 0;
                int iMaxScore = 0;

                if (dsRawdatas.Tables.Count > 0 && dsRawdatas.Tables[0].Rows.Count > 0)
                {
                    sDatas.Append("<table width='100%' cellspacing='0' cellpadding='2' border = '1'>");
                    DataTable dtAudit = dsRawdatas.Tables[0];
                    DataTable dtCategory = dsRawdatas.Tables[1];
                    DataTable dtRawData = dsRawdatas.Tables[2];
                    DataRow drHeader = dsRawdatas.Tables[3].Rows[0];
                    sDatas.Append("<tr><td rowspan='5' style='border: none;' ><img style='height: 54px;width: 100px;' border='0' alt='Dusters' src='http://ifazility.com/facilityaudit/AppThemes/images/JLL%20New.png'></td><td rowspan='4' style='font-size: 24px;border: none;font-weight:bold;' align='center' colspan='6'>DUSTERS TOTAL SOLUTIONS SERVICES PVT LTD</td>");
                    sDatas.Append("<tr></tr>");
                    sDatas.Append("<tr></tr>");
                    sDatas.Append("<tr></tr>");
                    sDatas.Append("<tr></tr>");
                    sDatas.Append("<tr><td colspan='2'>Name of the Client</td><td colspan='3'>" + drHeader["clientname"].ToString() + "</td><td colspan='2'></td></tr>");
                    sDatas.Append("<tr><td colspan='2'>Name of the Site</td><td colspan='3'>" + drHeader["locsitename"].ToString() + "</td><td colspan='2'></td></tr>");
                    sDatas.Append("<tr><td colspan='2'>Site ID</td><td colspan='3'>" + GetSiteID(drHeader["locsitename"].ToString()) + "</td><td colspan='2'></td></tr>");
                    sDatas.Append("<tr><td colspan='2'>Name of the Client person</td><td colspan='3'>" + drHeader["clientperson"].ToString() + "</td><td colspan='2'></td></tr>");
                    sDatas.Append("<tr><td colspan='2'>Name of the SBU</td><td colspan='3'>" + drHeader["sbuname"].ToString() + "</td><td colspan='2'></td></tr>");
                    sDatas.Append("<tr><td colspan='2'>Name of the OM / AOM</td><td colspan='3'>" + drHeader["aom"].ToString() + "</td><td colspan='2'></td></tr>");
                    sDatas.Append("<tr><td colspan='2'>Name of the Auditor</td><td colspan='3'>" + drHeader["auditor"].ToString() + "</td><td colspan='2'></td></tr>");
                    sDatas.Append("<tr><td colspan='2'>Name of the Auditee</td><td colspan='3'>" + drHeader["auditee"].ToString() + "</td><td colspan='2'></td></tr>");
                    sDatas.Append("<tr><td colspan='2'>Last audit date</td><td align='left' colspan='3'>" + drHeader["displastauditdate"].ToString() + "</td><td colspan='2'>Last Audit Score</td></tr>");
                    sDatas.Append("<tr><td colspan='2'>Current audit date</td><td align='left' colspan='3'>" + drHeader["dispauditdate"].ToString() + "</td><td>Current Audit Score</td><td>" + Getscored + "%</td></tr>");

                    for (int iauditcount = 0; iauditcount < dtAudit.Rows.Count; iauditcount++)
                    {
                        DataRow draudit = dtAudit.Rows[iauditcount];
                        int auditid = Convert.ToInt32(draudit["auditid"].ToString());
                        string auditname = draudit["auditname"].ToString();
                        sDatas.Append("<tr><td align='center' style='font-size: 22px;' colspan='7'>" + auditname + "</td></tr>");
                        sDatas.Append("<tr><th style = 'background-color: #FCB133;color:#ffffff'>Sl.No</th><th style = 'background-color: #FCB133;color:#ffffff'>Observations</th><th style = 'background-color: #FCB133;color:#ffffff;width: 5%;'>Maximum <br/> Score</th><th style = 'background-color: #FCB133;color:#ffffff;width: 5%;'>Score <br/> Obtained</th><th style = 'background-color: #FCB133;color:#ffffff;width: 30%;'>Auditor Remarks</th><th style = 'background-color: #FCB133;color:#ffffff'>Remarks by SBU </th><th style = 'background-color: #FCB133;color:#ffffff'>Image</th></tr>");
                        int iscore = 0;
                        int iweightage = 0;
                        int iSingleAvg = 0;
                        int iTotalAvg = 0;
                        int iAuditMax = 0;
                        DataRow[] drCategory = dtCategory.Select("auditid=" + auditid);
                        for (int icategroryCount = 0; icategroryCount < drCategory.Length; icategroryCount++)
                        {
                            int categoryid = Convert.ToInt32(drCategory[icategroryCount]["categoryid"].ToString());
                            string categoryname = drCategory[icategroryCount]["categoryname"].ToString();
                            sDatas.Append("<tr><td align='center'>" + (icategroryCount + 1) + "</td><td style='font-size: 18px;' colspan='6'>" + categoryname + "</td></tr>");
                            DataRow[] drRawDatas = dtRawData.Select("categoryid=" + categoryid);

                            int iCategoryScore = 0;
                            int iCategoryTotal = 0;
                            int iCatMax = 0;

                            for (int iRawDatacount = 0; iRawDatacount < drRawDatas.Length; iRawDatacount++)
                            {
                                string question = drRawDatas[iRawDatacount]["auditqname"].ToString();
                                string remarks = drRawDatas[iRawDatacount]["remarks"].ToString();
                                int score = Convert.ToInt32(drRawDatas[iRawDatacount]["score"].ToString());
                                int weightage = Convert.ToInt32(drRawDatas[iRawDatacount]["weightage"].ToString());
                                string scorename = drRawDatas[iRawDatacount]["scorename"].ToString();
                                string imgFile = drRawDatas[iRawDatacount]["uploadfilename"].ToString();
                                string filelink = string.Empty;
                                if (imgFile.Trim() != string.Empty)
                                {
                                    string[] imgfiles = imgFile.Split('#');
                                    foreach (string files in imgfiles)
                                    {
                                        if (files.Trim() != string.Empty)
                                        {
                                            imgFile = "http://ifazility.com/facilityaudit/auditimage/" + files;
                                            filelink += "<a href='" + imgFile + "'>" + imgFile + "</a><br/>";
                                        }
                                    }
                                }

                                int tScore = 0;
                                int MaxScore = 0;

                                if (score == -1)
                                {
                                    score = 0;
                                    tScore = -1;
                                    weightage = 0;
                                    MaxScore = 0;
                                }
                                else
                                {
                                    MaxScore = 1;
                                }

                                iscore += score;
                                iweightage += weightage;

                                iCategoryScore += score;
                                iCategoryTotal += weightage;

                                iTotalScore += score;
                                iTotalWeightage += weightage;

                                iSingleAvg = score * weightage;
                                iTotalAvg += iSingleAvg;

                                iMaxScore += MaxScore;
                                iCatMax += MaxScore;
                                iAuditMax += MaxScore;

                                string _score = string.Empty;
                                string _maxscore = string.Empty;
                                if (tScore == 0)
                                {
                                    _score = " " + score;
                                    _maxscore = " " + MaxScore;
                                }
                                else
                                {
                                    _score = scorename;
                                    _maxscore = scorename;
                                }
                                sDatas.Append("<tr><td align='center'>" + (iRawDatacount + 1).ToString() + "</td><td>" + question + "</td><td style='width: 5%;' align='center'>" + _maxscore + "</td><td  style='width: 5%;'  align='center'>" + _score + "</td><td>" + remarks + "</td><td style='width: 30%;'></td><td>" + filelink + "</td></tr>");
                            }

                            float _CateWisTot = 0;
                            _CateWisTot = ((float)(iCategoryScore * 100 / iCategoryTotal));

                            string sCatNA = string.Empty;
                            string sCatMaxNA = string.Empty;
                            sCatNA = iCatMax.ToString();
                            sCatMaxNA = iCategoryScore.ToString();
                            if (iCatMax == 0)
                            {
                                sCatNA = "N/A";
                            }
                            if (iCategoryScore == 0)
                            {
                                sCatMaxNA = "N/A";
                            }
                            sDatas.Append("<tr style='font-weight:bold;font-size: 18px;'><td colspan='2'>" + categoryname + "</td><td align='center'>" + sCatNA + "</td><td align='center'>" + sCatMaxNA + "</td><td></td><td></td><td></td></tr>");
                        }
                        float iScoredAvg = 0;
                        iScoredAvg = ((float)(iTotalAvg * 100 / iweightage));
                        fEnteirAvg += iScoredAvg;

                        sDatas.Append("<tr style='font-weight:bold;font-size: 18px;'><td colspan='2'>Total Score for " + auditname + "</td><td align='center'>" + iAuditMax + "</td><td align='center'>" + iscore + "</td><td></td><td></td><td></td></tr>");
                        sDatas.Append("<tr style='font-weight:bold;font-size: 18px;'><td colspan='2'>Percentage (%)</td><td></td><td align='center'>" + iScoredAvg + "%" + "</td><td></td><td></td><td></td></tr>");
                    }
                    float tAvg = fEnteirAvg / (dsRawdatas.Tables[0].Rows.Count);

                    sDatas.Append("<tr style='font-weight:bold;font-size: 18px;'><td colspan='2'>GRAND TOTAL SCORE</td><td align='center'>" + iMaxScore + "</td><td align='center'>" + iTotalScore + "</td><td></td><td></td><td></td></tr>");

                    decimal scoreGot = Convert.ToDecimal(Getscored);
                    string setColour = string.Empty;
                    if (scoreGot >= 83)
                    {
                        setColour = "<td style='background-color: #00FF00;' align='center'>" + Getscored + "%" + "</td>";
                    }
                    else
                    {
                        setColour = "<td style='background-color: #FFBE00;' align='center'>" + Getscored + "%" + "</td>";
                    }
                    sDatas.Append("<tr style='font-weight:bold;font-size: 18px;'><td colspan='2'>Percentage (%)</td><td></td>" + setColour + "<td></td><td></td><td></td></tr>");
                    if (dsRawdatas.Tables[3].Rows.Count > 0)
                    {
                        sDatas.Append("<tr style='font-weight:bold;font-size: 18px;'><td colspan='2'>Follow-Up Date</td><td></td><td>" + dsRawdatas.Tables[3].Rows[0]["dispFollowUpDate"].ToString() + "</td><td></td><td></td><td></td></tr>");
                    }


                    string observations = drHeader["observation"].ToString();
                    string clientremarks = drHeader["feedback"].ToString();
                    observations = observations.Replace('[', ' ');
                    observations = observations.Replace(']', ' ');
                    try
                    {
                        Observations objobservation = JsonConvert.DeserializeObject<Observations>(observations);
                        //                string observationdet = "1. Operations : " + objobservation.JS_Operation + "break" +
                        //"2. Training  : " + objobservation.JS_Training + " break  " +
                        //"3. SCM  : " + objobservation.JS_SCM + " break " +
                        //"4. Compliance  : " + objobservation.JS_Complaince + "break" +
                        //"5. HR  : " + objobservation.JS_HR + "break " +
                        //"6. Others : " + objobservation.JS_Operation;
                        if (objobservation != null)
                        {
                            sDatas.Append("<tr><td align='center' style='font-size: 22px;' colspan='7'>Other Observations</td></tr>");
                            sDatas.Append("<tr style='font-weight:bold;font-size: 15px;'><td colspan='2'>Operations</td><td colspan='5'>" + objobservation.JS_Operation + "</td></tr>");
                            sDatas.Append("<tr style='font-weight:bold;font-size: 15px;'><td colspan='2'>Training</td><td colspan='5'>" + objobservation.JS_Training + "</td></tr>");
                            sDatas.Append("<tr style='font-weight:bold;font-size: 15px;'><td colspan='2'>SCM</td><td colspan='5'>" + objobservation.JS_SCM + "</td></tr>");
                            sDatas.Append("<tr style='font-weight:bold;font-size: 15px;'><td colspan='2'>Compliance</td><td colspan='5'>" + objobservation.JS_Complaince + "</td></tr>");
                            sDatas.Append("<tr style='font-weight:bold;font-size: 15px;'><td colspan='2'>HR</td><td colspan='5'>" + objobservation.JS_HR + "</td></tr>");
                            sDatas.Append("<tr style='font-weight:bold;font-size: 15px;'><td colspan='2'>Others</td><td colspan='5'>" + objobservation.JS_Operation + "</td></tr>");
                        }
                    }
                    catch (Exception) { }
                    sDatas.Append("<tr><td align='center' style='font-size: 22px;' colspan='7'>Client Feedback</td></tr>");
                    sDatas.Append("<tr><td  colspan='7'>" + clientremarks + "</td></tr>");



                    sDatas.Append("</table>");
                    //dvData.InnerHtml = sDatas.ToString();
                    //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "callexport", "<script type='text/javascript'>PrintExce();</script>", false);

                    Response.Clear();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment;filename=TopLevelReport1.xlsx");
                    Response.Charset = "";
                    Response.ContentType = "application/vnd.ms-excel";
                    // string style = @"<style> .textmode { } </style>";
                    //Response.Write(style);
                    Response.Output.Write(sDatas.ToString());
                    Response.Flush();
                    Response.End();
                }
            }
            catch (Exception ex)
            {
            }
        }

        private void MakeExcel(DataSet dsRawdatas, string Getscored)
        {
            try
            {
                string excelfilepath = Server.MapPath("DownloadExcels/") + Guid.NewGuid().ToString() + ".xlsx";
                FileInfo newFile = new FileInfo(excelfilepath);
                if (newFile.Exists)
                {
                    newFile.Delete();  // ensures we create a new workbook
                    newFile = new FileInfo(excelfilepath);
                }

                using (ExcelPackage package = new ExcelPackage(newFile))
                {
                    // add a new worksheet to the empty workbook
                    ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("DownloadeExcel");

                    //Add the headers

                    StringBuilder sDatas = new StringBuilder();

                    float fEnteirAvg = 0;
                    int iTotalScore = 0;
                    int iTotalWeightage = 0;
                    int iMaxScore = 0;

                    DataTable dtChartdet = new DataTable();
                    dtChartdet.Columns.Add("Categoryname", typeof(string));
                    dtChartdet.Columns.Add("Value", typeof(float));
                    dtChartdet.Columns.Add("auditId", typeof(int));

                    if (dsRawdatas.Tables.Count > 0 && dsRawdatas.Tables[0].Rows.Count > 0)
                    {

                        DataTable dtAudit = dsRawdatas.Tables[0];
                        DataTable dtCategory = dsRawdatas.Tables[1];
                        DataTable dtRawData = dsRawdatas.Tables[2];
                        DataRow drHeader = dsRawdatas.Tables[3].Rows[0];

                        // worksheet.Cells[1, 1].Value = "Image Here";
                        worksheet.Cells[1, 1].Value = "DUSTERS TOTAL SOLUTIONS SERVICES PVT LTD";

                        var headerFont = worksheet.Cells[1, 1].Style.Font;
                        headerFont.Bold = true;
                        headerFont.Size = 18;
                        worksheet.Cells[1, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        worksheet.Cells[1, 1].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                        worksheet.Cells["A1:G4"].Merge = true;

                        FileInfo fileinfo = new FileInfo(Server.MapPath("AppThemes/images/dts.png"));
                        var picture = worksheet.Drawings.AddPicture("logo", fileinfo);
                        picture.SetSize(100, 50);
                        picture.SetPosition(1, 0, 1, 0);

                        worksheet.Cells[5, 1].Value = "Name of the Client";
                        worksheet.Cells[5, 3].Value = drHeader["clientname"].ToString();
                        worksheet.Cells[5, 6].Value = "";



                        worksheet.Cells["A5:B5"].Merge = true;
                        worksheet.Cells["C5:E5"].Merge = true;
                        worksheet.Cells["F5:G5"].Merge = true;

                        worksheet.Cells[6, 1].Value = "Name of the Site";
                        worksheet.Cells[6, 3].Value = drHeader["locsitename"].ToString();
                        worksheet.Cells[6, 6].Value = "";

                        worksheet.Name = drHeader["locsitename"].ToString();

                        worksheet.Cells["A6:B6"].Merge = true;
                        worksheet.Cells["C6:E6"].Merge = true;
                        worksheet.Cells["F6:G6"].Merge = true;

                        worksheet.Cells[7, 1].Value = "Site ID";
                        worksheet.Cells[7, 3].Value = GetSiteID(drHeader["locsitename"].ToString());
                        worksheet.Cells[7, 6].Value = "";

                        worksheet.Cells["A7:B7"].Merge = true;
                        worksheet.Cells["C7:E7"].Merge = true;
                        worksheet.Cells["F7:G7"].Merge = true;

                        worksheet.Cells[8, 1].Value = "Name of the Client person";
                        worksheet.Cells[8, 3].Value = drHeader["clientperson"].ToString();
                        worksheet.Cells[8, 6].Value = "";

                        worksheet.Cells["A8:B8"].Merge = true;
                        worksheet.Cells["C8:E8"].Merge = true;
                        worksheet.Cells["F8:G8"].Merge = true;

                        worksheet.Cells[9, 1].Value = "Name of the SBU";
                        worksheet.Cells[9, 3].Value = drHeader["sbuname"].ToString();
                        worksheet.Cells[9, 6].Value = "";

                        worksheet.Cells["A9:B9"].Merge = true;
                        worksheet.Cells["C9:E9"].Merge = true;
                        worksheet.Cells["F9:G9"].Merge = true;

                        worksheet.Cells[10, 1].Value = "Name of the OM / AOM";
                        worksheet.Cells[10, 3].Value = drHeader["aom"].ToString();
                        worksheet.Cells[10, 6].Value = "";

                        worksheet.Cells["A10:B10"].Merge = true;
                        worksheet.Cells["C10:E10"].Merge = true;
                        worksheet.Cells["F10:G10"].Merge = true;

                        worksheet.Cells[11, 1].Value = "Name of the Auditor";
                        worksheet.Cells[11, 3].Value = drHeader["auditor"].ToString();
                        worksheet.Cells[11, 6].Value = "";

                        worksheet.Cells["A11:B11"].Merge = true;
                        worksheet.Cells["C11:E11"].Merge = true;
                        worksheet.Cells["F11:G11"].Merge = true;

                        worksheet.Cells[12, 1].Value = "Name of the Auditee";
                        worksheet.Cells[12, 3].Value = drHeader["auditee"].ToString();
                        worksheet.Cells[12, 6].Value = "";

                        worksheet.Cells["A12:B12"].Merge = true;
                        worksheet.Cells["C12:E12"].Merge = true;
                        worksheet.Cells["F12:G12"].Merge = true;

                        worksheet.Cells[13, 1].Value = "Last audit date";
                        worksheet.Cells[13, 3].Value = drHeader["displastauditdate"].ToString();
                        worksheet.Cells[13, 6].Value = "Last Audit Score";

                        worksheet.Cells["A13:B13"].Merge = true;
                        worksheet.Cells["C13:E13"].Merge = true;
                        worksheet.Cells["F13:G13"].Merge = true;

                        worksheet.Cells[14, 1].Value = "Current audit date";
                        worksheet.Cells[14, 3].Value = drHeader["dispauditdate"].ToString();
                        worksheet.Cells[14, 6].Value = "Current Audit Score";
                        worksheet.Cells[14, 7].Value = Getscored;

                        worksheet.Cells["A14:B14"].Merge = true;
                        worksheet.Cells["C14:E14"].Merge = true;

                        int imaxrowcount = 15;

                        for (int iauditcount = 0; iauditcount < dtAudit.Rows.Count; iauditcount++)
                        {
                            DataRow draudit = dtAudit.Rows[iauditcount];
                            int auditid = Convert.ToInt32(draudit["auditid"].ToString());
                            string auditname = draudit["auditname"].ToString();

                            worksheet.Cells[imaxrowcount, 1].Value = auditname;
                            worksheet.Cells[imaxrowcount, 1].Style.Font.Bold = true;

                            worksheet.Cells["A" + imaxrowcount + ":G" + imaxrowcount + ""].Merge = true;
                            worksheet.Cells[imaxrowcount, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            imaxrowcount++;

                            worksheet.Cells[imaxrowcount, 1].Value = "Sl.No";
                            worksheet.Cells[imaxrowcount, 2].Value = "Observations";
                            worksheet.Cells[imaxrowcount, 3].Value = "Maximum Score";
                            worksheet.Cells[imaxrowcount, 4].Value = "Score Obtained";
                            worksheet.Cells[imaxrowcount, 5].Value = "Auditor Remarks";
                            worksheet.Cells[imaxrowcount, 6].Value = "Remarks by Sbu";
                            worksheet.Cells[imaxrowcount, 7].Value = "Image";

                            var range = worksheet.Cells[imaxrowcount, 1, imaxrowcount, 7];
                            range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                            range.Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#FCB133"));

                            imaxrowcount++;

                            int iscore = 0;
                            int iweightage = 0;
                            int iSingleAvg = 0;
                            int iTotalAvg = 0;
                            int iAuditMax = 0;
                            DataRow[] drCategory = dtCategory.Select("auditid=" + auditid);
                            for (int icategroryCount = 0; icategroryCount < drCategory.Length; icategroryCount++)
                            {
                                int categoryid = Convert.ToInt32(drCategory[icategroryCount]["categoryid"].ToString());
                                string categoryname = drCategory[icategroryCount]["categoryname"].ToString();

                                worksheet.Cells[imaxrowcount, 1].Value = icategroryCount + 1;
                                worksheet.Cells[imaxrowcount, 2].Value = categoryname;
                                worksheet.Cells[imaxrowcount, 1].Style.Font.Bold = true;
                                worksheet.Cells[imaxrowcount, 2].Style.Font.Bold = true;
                                worksheet.Cells[imaxrowcount, 1].Style.Font.Size = 14;

                                worksheet.Cells["B" + imaxrowcount + ":G" + imaxrowcount + ""].Merge = true;
                                imaxrowcount++;

                                DataRow[] drRawDatas = dtRawData.Select("categoryid=" + categoryid);

                                int iCategoryScore = 0;
                                int iCategoryTotal = 0;
                                int iCatMax = 0;

                                for (int iRawDatacount = 0; iRawDatacount < drRawDatas.Length; iRawDatacount++)
                                {
                                    string question = drRawDatas[iRawDatacount]["auditqname"].ToString();
                                    string remarks = drRawDatas[iRawDatacount]["remarks"].ToString();
                                    int score = Convert.ToInt32(drRawDatas[iRawDatacount]["score"].ToString());
                                    int weightage = Convert.ToInt32(drRawDatas[iRawDatacount]["weightage"].ToString());
                                    string scorename = drRawDatas[iRawDatacount]["scorename"].ToString();
                                    string imgFile = drRawDatas[iRawDatacount]["uploadfilename"].ToString();
                                    string transactionid = drRawDatas[iRawDatacount]["TransactionId"].ToString();
                                    string filelink = string.Empty;
                                    if (imgFile.Trim() != string.Empty)
                                    {
                                        string filepath = "http://ifazility.com/facilityaudit/ImageLists.aspx";
                                        if (ConfigurationManager.AppSettings["Imaglists"] != null)
                                        {
                                            filepath = ConfigurationManager.AppSettings["Imaglists"].ToString();
                                        }
                                        filelink = filepath + "?TransactionId=" + transactionid;
                                        //string[] imgfiles = imgFile.Split('#');
                                        //foreach (string files in imgfiles)
                                        //{
                                        //    if (files.Trim() != string.Empty)
                                        //    {
                                        //        imgFile = "http://ifazility.com/facilityaudit/auditimage/" + files;
                                        //        //filelink += "<a href='" + imgFile + "'>" + imgFile + "</a><br/>";

                                        //        filelink = "HYPERLINK(\"" + imgFile + "\",\"" + files + "\")";
                                        //    }
                                        //}
                                    }

                                    int tScore = 0;
                                    int MaxScore = 0;

                                    if (score == -1)
                                    {
                                        score = 0;
                                        tScore = -1;
                                        weightage = 0;
                                        MaxScore = 0;
                                    }
                                    else
                                    {
                                        MaxScore = 1;
                                    }

                                    iscore += score;
                                    iweightage += weightage;

                                    iCategoryScore += score;
                                    iCategoryTotal += weightage;

                                    iTotalScore += score;
                                    iTotalWeightage += weightage;

                                    iSingleAvg = score * weightage;
                                    iTotalAvg += iSingleAvg;

                                    iMaxScore += MaxScore;
                                    iCatMax += MaxScore;
                                    iAuditMax += MaxScore;

                                    string _score = string.Empty;
                                    string _maxscore = string.Empty;
                                    if (tScore == 0)
                                    {
                                        _score = " " + score;
                                        _maxscore = " " + MaxScore;
                                    }
                                    else
                                    {
                                        _score = scorename;
                                        _maxscore = scorename;
                                    }


                                    worksheet.Cells[imaxrowcount, 1].Value = iRawDatacount + 1;
                                    worksheet.Cells[imaxrowcount, 2].Value = question;
                                    //worksheet.Cells[imaxrowcount, 3].Value = _maxscore;

                                    int MaxRefId;
                                    int ScorerefID;

                                    bool isNumeric = int.TryParse(_maxscore.Trim(), out MaxRefId);

                                    if (isNumeric)
                                    {
                                        worksheet.Cells[imaxrowcount, 3].Value = MaxRefId;
                                        worksheet.Cells[imaxrowcount, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                    }
                                    else
                                    {
                                        worksheet.Cells[imaxrowcount, 3].Value = MaxRefId;
                                        worksheet.Cells[imaxrowcount, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                    }

                                    isNumeric = int.TryParse(_score.Trim(), out ScorerefID);

                                    if (isNumeric)
                                    {
                                        worksheet.Cells[imaxrowcount, 4].Value = ScorerefID;
                                        worksheet.Cells[imaxrowcount, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                    }
                                    else
                                    {
                                        worksheet.Cells[imaxrowcount, 4].Value = _score;
                                        worksheet.Cells[imaxrowcount, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                    }

                                    //worksheet.Cells[imaxrowcount, 4].Value = _score;
                                    worksheet.Cells[imaxrowcount, 5].Value = remarks;
                                    worksheet.Cells[imaxrowcount, 6].Value = string.Empty;

                                    if (imgFile.Trim() != string.Empty)
                                    {
                                        var cell = worksheet.Cells[imaxrowcount, 7];
                                        cell.Hyperlink = new Uri(filelink);
                                        cell.Value = "ImageList";
                                    }

                                    // worksheet.Cells[imaxrowcount, 7].Formula = filelink;
                                    //worksheet.Cells[imaxrowcount, 7].Style.WrapText = true;
                                    imaxrowcount++;
                                }

                                float _CateWisTot = 0;
                                if (iCategoryTotal != 0)
                                {
                                    _CateWisTot = ((float)(iCategoryScore * 100 / iCategoryTotal));
                                }
                                else
                                {
                                    _CateWisTot = 0;
                                }

                                dtChartdet.Rows.Add(categoryname, _CateWisTot, auditid);


                                string sCatNA = string.Empty;
                                string sCatMaxNA = string.Empty;
                                sCatNA = iCatMax.ToString();
                                sCatMaxNA = iCategoryScore.ToString();
                                if (iCatMax == 0)
                                {
                                    sCatNA = "N/A";
                                }
                                if (iCategoryScore == 0)
                                {
                                    sCatMaxNA = "N/A";
                                }

                                worksheet.Cells[imaxrowcount, 1].Value = categoryname;

                                int _catena;
                                int _catmaxna;
                                bool Numeric = int.TryParse(sCatNA.Trim(), out _catena);

                                if (Numeric)
                                {
                                    worksheet.Cells[imaxrowcount, 3].Value = _catena;
                                    worksheet.Cells[imaxrowcount, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                }
                                else
                                {
                                    worksheet.Cells[imaxrowcount, 3].Value = sCatNA;
                                    worksheet.Cells[imaxrowcount, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                }

                                Numeric = int.TryParse(sCatMaxNA.Trim(), out _catmaxna);

                                if (Numeric)
                                {
                                    worksheet.Cells[imaxrowcount, 4].Value = _catmaxna;
                                    worksheet.Cells[imaxrowcount, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                }
                                else
                                {
                                    worksheet.Cells[imaxrowcount, 4].Value = _catmaxna;
                                    worksheet.Cells[imaxrowcount, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                }

                                //worksheet.Cells[imaxrowcount, 3].Value = sCatNA;
                                //worksheet.Cells[imaxrowcount, 4].Value = sCatMaxNA;

                                worksheet.Cells[imaxrowcount, 1].Style.Font.Bold = true;
                                worksheet.Cells[imaxrowcount, 3].Style.Font.Bold = true;
                                worksheet.Cells[imaxrowcount, 4].Style.Font.Bold = true;
                                worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                                imaxrowcount++;
                            }
                            float iScoredAvg = 0;
                            iScoredAvg = ((float)(iTotalAvg * 100 / iweightage));
                            fEnteirAvg += iScoredAvg;

                            worksheet.Cells[imaxrowcount, 1].Value = auditname;
                            worksheet.Cells[imaxrowcount, 3].Value = iAuditMax;
                            worksheet.Cells[imaxrowcount, 4].Value = iscore;

                            worksheet.Cells[imaxrowcount, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            worksheet.Cells[imaxrowcount, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;

                            worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                            imaxrowcount++;

                            worksheet.Cells[imaxrowcount, 1].Value = "Percentage (%)";
                            worksheet.Cells[imaxrowcount, 3].Value = string.Empty;
                            worksheet.Cells[imaxrowcount, 4].Value = iScoredAvg + "%";

                            worksheet.Cells[imaxrowcount, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                            imaxrowcount++;
                        }
                        float tAvg = fEnteirAvg / (dsRawdatas.Tables[0].Rows.Count);

                        worksheet.Cells[imaxrowcount, 1].Value = "GRAND TOTAL SCORE";
                        worksheet.Cells[imaxrowcount, 3].Value = iMaxScore;
                        worksheet.Cells[imaxrowcount, 4].Value = iTotalScore;
                        worksheet.Cells[imaxrowcount, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        worksheet.Cells[imaxrowcount, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                        imaxrowcount++;

                        decimal scoreGot = Convert.ToDecimal(Getscored);


                        worksheet.Cells[imaxrowcount, 1].Value = "Percentage (%)";
                        worksheet.Cells[imaxrowcount, 3].Value = string.Empty;
                        worksheet.Cells[imaxrowcount, 4].Value = Getscored;
                        worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                        string setColour = string.Empty;
                        if (scoreGot >= 83)
                        {
                            worksheet.Cells[imaxrowcount, 4].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            worksheet.Cells[imaxrowcount, 4].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#00FF00"));
                        }
                        else
                        {
                            worksheet.Cells[imaxrowcount, 4].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            worksheet.Cells[imaxrowcount, 4].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#FFBE00"));
                        }
                        imaxrowcount++;



                        if (dsRawdatas.Tables[3].Rows.Count > 0)
                        {
                            worksheet.Cells[imaxrowcount, 1].Value = "Follow-Up Date";
                            worksheet.Cells[imaxrowcount, 3].Value = string.Empty;
                            worksheet.Cells[imaxrowcount, 4].Value = dsRawdatas.Tables[3].Rows[0]["dispFollowUpDate"].ToString();
                            worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                            imaxrowcount++;
                        }


                        string observations = drHeader["observation"].ToString();
                        string clientremarks = drHeader["feedback"].ToString();
                        observations = observations.Replace('[', ' ');
                        observations = observations.Replace(']', ' ');
                        try
                        {
                            Observations objobservation = JsonConvert.DeserializeObject<Observations>(observations);
                            //                string observationdet = "1. Operations : " + objobservation.JS_Operation + "break" +
                            //"2. Training  : " + objobservation.JS_Training + " break  " +
                            //"3. SCM  : " + objobservation.JS_SCM + " break " +
                            //"4. Compliance  : " + objobservation.JS_Complaince + "break" +
                            //"5. HR  : " + objobservation.JS_HR + "break " +
                            //"6. Others : " + objobservation.JS_Operation;
                            if (objobservation != null)
                            {

                                worksheet.Cells[imaxrowcount, 1].Value = "Other Observations";
                                worksheet.Cells["A" + imaxrowcount + ":G" + imaxrowcount + ""].Merge = true;
                                worksheet.Cells[imaxrowcount, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                imaxrowcount++;

                                worksheet.Cells[imaxrowcount, 1].Value = "Operations";
                                worksheet.Cells[imaxrowcount, 3].Value = objobservation.JS_Operation;
                                worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                                worksheet.Cells["C" + imaxrowcount + ":G" + imaxrowcount + ""].Merge = true;
                                imaxrowcount++;

                                worksheet.Cells[imaxrowcount, 1].Value = "Training";
                                worksheet.Cells[imaxrowcount, 3].Value = objobservation.JS_Training;
                                worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                                worksheet.Cells["C" + imaxrowcount + ":G" + imaxrowcount + ""].Merge = true;
                                imaxrowcount++;

                                worksheet.Cells[imaxrowcount, 1].Value = "SCM";
                                worksheet.Cells[imaxrowcount, 3].Value = objobservation.JS_SCM;
                                worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                                worksheet.Cells["C" + imaxrowcount + ":G" + imaxrowcount + ""].Merge = true;
                                imaxrowcount++;

                                worksheet.Cells[imaxrowcount, 1].Value = "Compliance";
                                worksheet.Cells[imaxrowcount, 3].Value = objobservation.JS_Complaince;
                                worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                                worksheet.Cells["C" + imaxrowcount + ":G" + imaxrowcount + ""].Merge = true;
                                imaxrowcount++;

                                worksheet.Cells[imaxrowcount, 1].Value = "HR";
                                worksheet.Cells[imaxrowcount, 3].Value = objobservation.JS_HR;
                                worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                                worksheet.Cells["C" + imaxrowcount + ":G" + imaxrowcount + ""].Merge = true;
                                imaxrowcount++;

                                worksheet.Cells[imaxrowcount, 1].Value = "Others";
                                worksheet.Cells[imaxrowcount, 3].Value = objobservation.JS_Operation;
                                worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                                worksheet.Cells["C" + imaxrowcount + ":G" + imaxrowcount + ""].Merge = true;
                                imaxrowcount++;
                            }
                        }
                        catch (Exception) { }

                        worksheet.Cells[imaxrowcount, 1].Value = "Client Feedback";
                        worksheet.Cells[imaxrowcount, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        worksheet.Cells["A" + imaxrowcount + ":G" + imaxrowcount + ""].Merge = true;
                        imaxrowcount++;


                        worksheet.Cells[imaxrowcount, 1].Value = clientremarks;
                        worksheet.Cells["A" + imaxrowcount + ":G" + imaxrowcount + ""].Merge = true;
                        imaxrowcount++;


                        worksheet.Cells.AutoFitColumns(0);  //Autofit columns for all cells
                        worksheet.Cells.AutoFitColumns(1);
                        worksheet.Cells.AutoFitColumns(2);
                        worksheet.Cells.AutoFitColumns(3);
                        worksheet.Cells.AutoFitColumns(4);
                        worksheet.Cells.AutoFitColumns(5);
                        worksheet.Cells.AutoFitColumns(6);
                        worksheet.Cells.AutoFitColumns(7);

                        int c1;

                        imaxrowcount = 7;
                        ExcelWorksheet worksheetChart = package.Workbook.Worksheets.Add("CategoryChart");

                        string Alphabetic = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                        int startchar = 0;
                        int colspace = 0;
                        foreach (DataRow drAuditRow in dtAudit.Rows)
                        {
                            int auditid = Convert.ToInt32(drAuditRow["auditid"].ToString());
                            string auditname = drAuditRow["auditname"].ToString();
                            DataRow[] drAuditCategories = dtChartdet.Select("auditid=" + auditid);
                            if (drAuditCategories.Length > 0)
                            {
                                DataTable dtCategoryChart = drAuditCategories.CopyToDataTable();
                                int startvalue = imaxrowcount;

                                char Firstcol = Alphabetic[startchar];
                                char Secondcol = Alphabetic[startchar + 1];

                                foreach (DataRow drchartdet in dtCategoryChart.Rows)
                                {
                                    string categoryname = drchartdet["categoryname"].ToString();
                                    decimal score = Convert.ToDecimal(drchartdet["Value"].ToString());

                                    worksheetChart.Cells["" + Firstcol + imaxrowcount].Value = categoryname;
                                    worksheetChart.Cells["" + Secondcol + imaxrowcount].Value = score;
                                    imaxrowcount++;
                                }

                                var chart = worksheetChart.Drawings.AddChart("chart" + imaxrowcount, eChartType.ColumnClustered3D);
                                var series = chart.Series.Add("" + Secondcol + startvalue + ":" + Secondcol + imaxrowcount, "" + Firstcol + startvalue + ":" + Firstcol + imaxrowcount);
                                //series.HeaderAddress = new ExcelAddress("'Sheet1'!B" + (startvalue - 1));
                                chart.SetSize(510, 300);
                                chart.Title.Text = auditname;
                                chart.SetPosition(7, 0, colspace, 0);
                                colspace += 9;

                                startchar += 2;
                            }
                        }


                        package.Save();

                        Response.ContentType = ContentType;
                        Response.AppendHeader("Content-Disposition", "attachment; filename=" + Path.GetFileName(excelfilepath));
                        Response.WriteFile(excelfilepath);
                        Response.End();

                        //this.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                        //this.Response.AddHeader(
                        //          "content-disposition",
                        //          string.Format("attachment;  filename={0}", "ExcellData.xlsx"));
                        //this.Response.BinaryWrite(package.GetAsByteArray());
                    }

                }

                //FileInfo prevFile = new FileInfo(excelfilepath);
                //if (prevFile.Exists)
                //{
                //    prevFile.Delete();
                //}
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, "error");
            }
        }



        protected void btnsearch_Click(object sender, EventArgs e)
        {
            try
            {
                BindOverall();
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, "error");
            }
        }
    }
}