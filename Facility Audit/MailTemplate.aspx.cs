﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BL;
using System.Data;
using FacilityAudit.Code;

namespace Facility_Audit
{
    public partial class MailTemplate : System.Web.UI.Page
    {
        BLContacts objblcontacts = new BLContacts();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindDetails();
            }
        }

        private void BindDetails()
        {
            try
            {
                DataSet dsmaildet = new DataSet();
                dsmaildet = objblcontacts.GetMailTemplate(UserSession.GroupID);
                if (dsmaildet != null && dsmaildet.Tables.Count > 0 && dsmaildet.Tables[0].Rows.Count > 0)
                {
                    DataRow drmaildet = dsmaildet.Tables[0].Rows[0];
                    txtsubject.Text = drmaildet["mailsubject"].ToString();
                    txtbody.Text = drmaildet["mailbody"].ToString();                   
                }
                else
                {
                    txtsubject.Text = string.Empty;
                    txtbody.Text = string.Empty;                  
                }
            }
            catch
            {
            }
        }

        public void NotifyMessages(string message, string errortype)
        {
            divmsg.Visible = true;
            lblError.Text = message;
            if (errortype == "info")
            {
                divmsg.Attributes.Add("class", "alert alert-success fade in");
            }
            else if (errortype == "error")
            {
                divmsg.Attributes.Add("class", "alert alert-danger fade in");
            }
        }

        protected void btnsave_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtsubject.Text.Trim() == string.Empty)
                {
                    NotifyMessages("Enter Mail-Subject", "error");
                    return;
                }
                if (txtbody.Text.Trim() == string.Empty)
                {
                    NotifyMessages("Enter Mail-Body", "error");
                    return;
                }

                string[] result = objblcontacts.InsertMailTemplate(UserSession.GroupID, txtsubject.Text.Trim(), txtbody.Text.Trim(), string.Empty,
                    string.Empty, string.Empty, UserSession.UserID);
                if (result[1] != "-1")
                {
                    NotifyMessages(result[0], "info");
                    BindDetails();
                }
                else
                {
                    NotifyMessages(result[0], "error");
                }
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message.ToString(), "error");
            }
        }
    }
}