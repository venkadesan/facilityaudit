﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FacilityAudit.BLL;
using BL;
using System.Data;
using FacilityAudit.Code;

namespace Facility_Audit
{
    public partial class AuditScore : System.Web.UI.Page
    {

        #region Properties
        private bool IsAdd
        {
            get
            {
                if (!ID.HasValue)
                    return false;
                else if (ID.Value != 0)
                    return false;
                else
                    return true;
            }
        }


        private int? ID
        {
            get
            {
                if (ViewState["id"] == null)
                    ViewState["id"] = 0;
                return (int?)ViewState["id"];
            }

            set
            {
                ViewState["id"] = value;
            }
        }

        private bool DisableStatus
        {
            get
            {
                if (ViewState["disablestatus"] == null)
                    ViewState["disablestatus"] = 0;
                return (bool)ViewState["disablestatus"];
            }

            set
            {
                ViewState["disablestatus"] = value;
            }
        }

        #endregion

        BLAudit objaudit = new BLAudit();
        BLGroupcompany objgrp = new BLGroupcompany();
        BLAuditscore objscore = new BLAuditscore();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                loadsector();
                loadaudit();
                ButtonStatus(Common.ButtonStatus.Cancel);
            }

        }

        private void loadsector()
        {
            try
            {
                DataSet ds = new DataSet();
                //ds = objgrp.getsector();
                ds = objgrp.GetSectorwithGroup(UserSession.GroupID);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddlsector.DataSource = ds;
                    ddlsector.DataTextField = "locationsectorname";
                    ddlsector.DataValueField = "locationsectorid";
                    ddlsector.DataBind();
                    UserSession.sectorid = Convert.ToInt32(this.ddlsector.Items[0].Value);
                }
            }
            catch { }
        }
        private void ButtonStatus(Common.ButtonStatus status)
        {
            this.txtscorename.Text = this.txtscore.Text = string.Empty;
            //ddlgroup.ClearSelection();


            if (status == Common.ButtonStatus.New)
            {
                this.pnlnew.Visible = true;
                this.bNew.Enabled = false;
                this.bNew.Visible = false;
                this.btnsave.Enabled = true;
                this.txtscorename.Focus();
                this.lblError.Text = "";
                this.divmsg.Visible = false;
                this.linewbtn.Visible = false;
            }
            else if (status == Common.ButtonStatus.Edit)
            {
                this.txtscorename.Focus();
                this.pnlnew.Visible = true;
                this.bNew.Enabled = false;
                this.bNew.Visible = false;
                this.btnsave.Enabled = true;
                this.lblError.Text = "";
                this.divmsg.Visible = false;
                this.linewbtn.Visible = false;
            }
            else if (status == Common.ButtonStatus.Cancel)
            {
                this.pnlnew.Visible = false;
                this.bNew.Visible = true;
                this.bNew.Enabled = true;
                this.btnsave.Enabled = false;
                this.ID = 0;
                divrep.Visible = true;
                loadrprtdetails();
                this.lblError.Text = "";
                this.divmsg.Visible = false;
                this.linewbtn.Visible = true;
            }
            else if (status == Common.ButtonStatus.Save)
            {
                this.pnlnew.Visible = false;
                this.bNew.Visible = true;
                this.bNew.Enabled = true;
                this.btnsave.Enabled = false;
                this.ID = 0;
                divrep.Visible = true;
                this.linewbtn.Visible = true;
                loadrprtdetails();
            }
        }
        public void NotifyMessages(string message, Common.ErrorType et)
        {
            divmsg.Visible = true;
            lblError.Text = message;
            if (et == Common.ErrorType.Error)
            {

            }
            else if (et == Common.ErrorType.Information)
            {

            }
        }
        protected void bNew_Click(object sender, EventArgs e)
        {
            ButtonStatus(Common.ButtonStatus.New);
        }


        protected void btnsave_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.txtscorename.Text == string.Empty)
                {
                    NotifyMessages("Enter Score name", Common.ErrorType.Error);
                    return;
                }
                if (this.txtscore.Text == string.Empty)
                {
                    NotifyMessages("Enter Score", Common.ErrorType.Error);
                    return;
                }
                int defaultchk =0;
                int notapplicable=0;
                if (chkdefault.Checked == true)
                {
                    defaultchk = 1;
                }
                else
                {
                    defaultchk = 0;
                }

                if (chknotapplicable.Checked == true)
                {
                    notapplicable = 1;
                }
                else
                {
                    notapplicable = 0;
                }

                if (this.IsAdd)
                {
                    if (objscore.addscore(Convert.ToInt32(ddlaudit.SelectedValue), this.txtscorename.Text.Trim(), Convert.ToInt32(this.txtscore.Text.Trim()), 1, DateTime.Now, Convert.ToBoolean(defaultchk), Convert.ToBoolean(notapplicable)) > 0)
                    {
                        NotifyMessages("Saved successfully", Common.ErrorType.Information);
                    }
                    else
                    {
                        NotifyMessages("Score name already exist", Common.ErrorType.Information);
                        return;
                    }
                }
                else if (this.ID != null && this.ID.Value > 0)
                {
                    if (objscore.updatemauditscore(Convert.ToInt32(ddlaudit.SelectedValue), this.ID.Value, this.txtscorename.Text.Trim(), Convert.ToInt32(this.txtscore.Text.Trim()), 1, DateTime.Now, Convert.ToBoolean(defaultchk), Convert.ToBoolean(notapplicable)) > 0)
                    {
                        NotifyMessages("Updated successfully", Common.ErrorType.Information);
                    }
                    else
                    {
                        NotifyMessages("Scorename already exist", Common.ErrorType.Information);
                        return;
                    }
                }

                ButtonStatus(Common.ButtonStatus.Save);
            }
            catch { }
        }
        private void loadaudit()
        {
            try
            {
                DataSet ds = new DataSet();
                int sectorid = 0;
                Common.DDVal(ddlsector, out sectorid);
                ds = objaudit.getmaudit(0, 0, sectorid);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddlaudit.DataSource = ds;
                    ddlaudit.DataTextField = "auditname";
                    ddlaudit.DataValueField = "auditid";
                    ddlaudit.DataBind();
                }

            }
            catch { }
        }
        private void loadrprtdetails()
        {
            try
            {

                DataSet ds = new DataSet();
                ds = objscore.getmauditscore(Convert.ToInt32(ddlaudit.SelectedValue), 0, 0);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    divrep.Visible = true;
                    rptrscore.DataSource = ds;
                    rptrscore.DataBind();
                }
                else
                {
                    divrep.Visible = false;
                }
            }
            catch { }
        }

        protected void rptrscore_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Edit")
                {
                    ButtonStatus(Common.ButtonStatus.Edit);
                    this.ID = Convert.ToInt32(e.CommandArgument);
                    BindToCtrls();
                }
                else if (e.CommandName == "Delete")
                {
                    this.ID = Convert.ToInt32(e.CommandArgument);
                    objscore.deletemauditscore(this.ID.Value);
                    ButtonStatus(Common.ButtonStatus.Cancel);
                    NotifyMessages("Deleted successfully", Common.ErrorType.Information);
                    loadrprtdetails();

                }
            }
            catch { }


        }
        public void BindToCtrls()
        {

            try
            {
                DataSet ds = new DataSet();
                ds = objscore.getmauditscore(Convert.ToInt32(ddlaudit.SelectedValue), this.ID.Value,1);

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    ddlaudit.SelectedValue = ds.Tables[0].Rows[0]["auditid"].ToString();
                    this.txtscorename.Text = ds.Tables[0].Rows[0]["scorename"].ToString();
                    this.txtscore.Text = ds.Tables[0].Rows[0]["score"].ToString();
                    chkdefault.Checked = Convert.ToBoolean(ds.Tables[0].Rows[0]["isdefault"].ToString());
                    chknotapplicable.Checked = Convert.ToBoolean(ds.Tables[0].Rows[0]["isnotapplicable"].ToString());

                }
            }
            catch { }
        }

        protected void rptrscore_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

        }

        protected void btncncl_Click(object sender, EventArgs e)
        {
            ButtonStatus(Common.ButtonStatus.Cancel);
        }

        protected void ddlaudit_SelectedIndexChanged(object sender, EventArgs e)
        {
            ButtonStatus(Common.ButtonStatus.Cancel);
            
        }

        protected void ddlsector_SelectedIndexChanged(object sender, EventArgs e)
        {
            UserSession.sectorid = Convert.ToInt32(this.ddlsector.SelectedItem.Value);

            this.ddlsector.ClearSelection();
            this.ddlsector.Items.FindByValue(UserSession.sectorid.ToString()).Selected = true;
            loadaudit();
            ButtonStatus(Common.ButtonStatus.Cancel);
        }

    }
}