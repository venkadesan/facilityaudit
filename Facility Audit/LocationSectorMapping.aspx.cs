﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FacilityAudit.BLL;
using KB = Kowni.BusinessLogic;
using KCB = Kowni.Common.BusinessLogic;
using FacilityAudit.Code;
using System.Text;
using BL;
using System.Configuration;
using System.IO;


namespace Facility_Audit
{
    public partial class LocationSectorMapping : System.Web.UI.Page
    {
        #region "Datas"

        private DataTable ReportDet
        {
            get
            {
                if (ViewState["reportdetails"] == null)
                    ViewState["reportdetails"] = null;
                return (DataTable)ViewState["reportdetails"];
            }

            set
            {
                ViewState["reportdetails"] = value;
            }
        }

        #endregion

        BLGroupcompany objgrp = new BLGroupcompany();
        BLGroupcompany objgrpcmp = new BLGroupcompany();
        KB.BLUser.BLUserLocation objuserLocation1 = new KB.BLUser.BLUserLocation();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    loadsector();
                }
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, "error");
            }
        }

        //private void loadsector()
        //{
        //    try
        //    {
        //        DataSet ds = new DataSet();
        //        ds = objgrp.getsector();
        //        if (UserSession.sectorid == 0)
        //        {
        //            if (ds.Tables[0].Rows.Count > 0)
        //            {
        //                ddlsector.DataSource = ds;
        //                ddlsector.DataTextField = "locationsectorname";
        //                ddlsector.DataValueField = "locationsectorid";
        //                ddlsector.DataBind();
        //                UserSession.sectorid = Convert.ToInt32(this.ddlsector.Items[0].Value);
        //            }

        //        }
        //        else
        //        {
        //            if (ds.Tables[0].Rows.Count > 0)
        //            {

        //                ddlsector.DataSource = ds;
        //                ddlsector.DataTextField = "locationsectorname";
        //                ddlsector.DataValueField = "locationsectorid";
        //                ddlsector.DataBind();
        //            }
        //            this.ddlsector.ClearSelection();
        //            this.ddlsector.Items.FindByValue(UserSession.sectorid.ToString()).Selected = true;

        //        }
        //        ddlsector_SelectedIndexChanged(null, null);

        //    }
        //    catch { throw; }
        //}

        private void loadsector()
        {
            try
            {
                DataSet ds = new DataSet();
                //ds = objgrp.getsector();
                ds = objgrp.GetSectorwithGroup(UserSession.GroupID);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddlsector.DataSource = ds;
                    ddlsector.DataTextField = "locationsectorname";
                    ddlsector.DataValueField = "locationsectorid";
                    ddlsector.DataBind();
                    ListItem lisector = ddlsector.SelectedItem;
                    if (lisector != null)
                    {
                        UserSession.sectorid = Convert.ToInt32(this.ddlsector.Items[0].Value);
                    }
                }
                ddlsector_SelectedIndexChanged(null, null);

            }
            catch { throw; }
        }

        protected void ddlsector_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                divmsg.Visible = false;
                lblError.Text = string.Empty;
                int sectorid = 0;
                Common.DDVal(ddlsector, out sectorid);
                DataSet dsLocDet = new DataSet();
                dsLocDet = objgrpcmp.GetMappedlocations(sectorid, UserSession.UserID);
                if (dsLocDet != null && dsLocDet.Tables.Count > 0 && dsLocDet.Tables[0].Rows.Count > 0)
                {
                    lstlocation.DataSource = dsLocDet.Tables[0];
                    lstlocation.DataTextField = "LocationName";
                    lstlocation.DataValueField = "locationid";
                    lstlocation.DataBind();
                }
                if (dsLocDet != null && dsLocDet.Tables.Count > 0 && dsLocDet.Tables[1].Rows.Count > 0)
                {
                    rptrmappeddet.DataSource = dsLocDet.Tables[1];
                    rptrmappeddet.DataBind();
                }
                else
                {
                    rptrmappeddet.DataSource = null;
                    rptrmappeddet.DataBind();
                }
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, "error");
            }
        }

        protected void btnsave_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dtSelectedValue = new DataTable("location");
                dtSelectedValue.Columns.Add("locationid", typeof(int));
                foreach (System.Web.UI.WebControls.ListItem li in lstlocation.Items)
                {
                    if (li.Selected == true)
                    {
                        dtSelectedValue.Rows.Add(Convert.ToInt32(li.Value));
                    }
                }
                foreach (RepeaterItem riLocDet in rptrmappeddet.Items)
                {
                    HiddenField hdnlocationid = (HiddenField)riLocDet.FindControl("hdnlocationid");
                    CheckBox chcksave = (CheckBox)riLocDet.FindControl("chcksave");
                    if (hdnlocationid != null && chcksave != null)
                    {
                        if (chcksave.Checked)
                        {
                            dtSelectedValue.Rows.Add(Convert.ToInt32(hdnlocationid.Value));
                        }
                    }
                }

                string xmlString = string.Empty;
                using (TextWriter writer = new StringWriter())
                {
                    dtSelectedValue.TableName = "location";
                    dtSelectedValue.WriteXml(writer);
                    xmlString = writer.ToString();
                }
                int sectorid = 0;
                Common.DDVal(ddlsector, out sectorid);

                string[] result = objgrpcmp.InsertSecLocMapping(sectorid, xmlString, UserSession.UserID);
                if (result[1] != "-1")
                {
                    ddlsector_SelectedIndexChanged(null, null);
                    NotifyMessages(result[0], "info");
                }
                else
                {
                    NotifyMessages(result[0], "error");
                }

            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, "error");
            }
        }

        public void NotifyMessages(string message, string errortype)
        {
            divmsg.Visible = true;
            lblError.Text = message;
            if (errortype == "info")
            {
                divmsg.Attributes.Add("class", "alert alert-success fade in");
            }
            else if (errortype == "error")
            {
                divmsg.Attributes.Add("class", "alert alert-danger fade in");
            }
        }
    }
}