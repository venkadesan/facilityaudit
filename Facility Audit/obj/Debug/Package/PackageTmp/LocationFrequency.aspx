﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Mainpage.Master" AutoEventWireup="true"
    CodeBehind="LocationFrequency.aspx.cs" Inherits="Facility_Audit.LocationFrequency" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>

        function OnSelectAll() {
            var monthid = $("[id*=ddlselectall]").val();
            //alert(monthid);
            $(".months").each(function () {
                //alert($(this).val());
                $(this).val(monthid);
            })
            return false;
        }
    </script>
    <script>
        function updateFormValues() {
            $('#tblCustomers > tbody  > tr').each(function (i, row) {
                var $row = $(row),
                 $checkedBox = $row.find('input:checked');
                $dropdown = $row.find('select');
                //   if ($(checkedBox).length)
                if ($(row).find('input:checked').length) {
                    var monthid = $("[id*=ddlselectall]").val();
                    $dropdown.val(monthid);
                }
            });
            return false;
        }
    </script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script type="text/javascript">
        var np = $.noConflict();
        np(function () {
            np("#tblCustomers [id*=chkHeader]").click(function () {
                if (np(this).is(":checked")) {
                    np("#tblCustomers [id*=chksave]").attr("checked", "checked");
                } else {
                    np("#tblCustomers [id*=chksave]").removeAttr("checked");
                }
            });
            np("#tblCustomers [id*=chksave]").click(function () {
                if (np("#tblCustomers [id*=chksave]").length == np("#tblCustomers [id*=chksave]:checked").length) {
                    np("#tblCustomers [id*=chkHeader]").attr("checked", "checked");
                } else {
                    np("#tblCustomers [id*=chkHeader]").removeAttr("checked");
                }
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="submenu">
        <h1>
            Category - Location Mapping</h1>
    </div>
    <div class="main">
        <div class=" col-md-5 col-xs-offset-4 alert alert-big alert-danger alert-dismissable fade in margin-top-10"
            runat="server" id="divmsg" visible="false">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                ×</button>
            <asp:Label runat="server" ID="lblError"></asp:Label>
        </div>
        <div class="row">
            <div class="col-md-12">
                <section class="tile">
                 <section class="tile cornered margin-top-10">
        	    	<div class="tile-body nobottompadding">
                      <div class="row form-horizontal">
                        <div class="col-lg-12">
                            <div class="form-group">
                                   <label class="col-sm-2 control-label" for="">Category</label>
                               <div class="col-sm-10">
                                   <asp:DropDownList ID="ddlcategory" runat="server"  AutoPostBack="True"  
                                     CssClass="form-control chosen-select" 
                                       onselectedindexchanged="ddlcategory_SelectedIndexChanged">
                                   </asp:DropDownList>
                               </div>
                            </div>
                        </div> 
                      
                        

                          <div class="col-lg-12">
                            <div class="form-group">                                
                              <div class="col-sm-9"></div>
                               <div class="col-sm-2">
                                   <asp:DropDownList ID="ddlselectall" class="form-control" runat="server">
                                                 <asp:ListItem Text="Jan" Value="1"></asp:ListItem>
                                                 <asp:ListItem Text="Feb" Value="2"></asp:ListItem>
                                                 <asp:ListItem Text="Mar" Value="3"></asp:ListItem>
                                                 <asp:ListItem Text="Apr" Value="4"></asp:ListItem>
                                                 <asp:ListItem Text="May" Value="5"></asp:ListItem>
                                                 <asp:ListItem Text="Jun" Value="6"></asp:ListItem>
                                                 <asp:ListItem Text="Jul" Value="7"></asp:ListItem>
                                                 <asp:ListItem Text="Aug" Value="8"></asp:ListItem>
                                                 <asp:ListItem Text="Sep" Value="9"></asp:ListItem>
                                                 <asp:ListItem Text="Oct" Value="10"></asp:ListItem>
                                                 <asp:ListItem Text="Nov" Value="11"></asp:ListItem>
                                                 <asp:ListItem Text="Dec" Value="12"></asp:ListItem>
                                                 </asp:DropDownList> 
                               </div>
                               <div class="col-sm-1">
                                 <asp:Button ID="btnsaveall" OnClientClick="return updateFormValues();" class="btn btn-primary" runat="server" Text="All" 
                                           />
                               </div>

                            </div>
                        </div> 
                                        <div class="col-md-12">
                        <div class="tile-body nopadding margin-top-10">
                            <div class="table-responsive">



                             <asp:Repeater ID="rptrlocations" DataSourceID="" runat="server" 
                                                onitemdatabound="rptrlocations_ItemDataBound">
    <HeaderTemplate>
        <table id="tblCustomers" width="100%" border="0" cellspacing="0" class="table table-bordered" cellpadding="0">
            <thead>
                <tr>
                 <th width="5%">Sl.No</th>                                                   
                 <th>Location Name</th>
                 <th>Month</th> 
                 <th><asp:CheckBox ID="chkHeader" runat="server" /></th>                   
                </tr>
            </thead>
    </HeaderTemplate>
    <ItemTemplate>
        <tbody>
            <tr>
                  <td>
                                                <asp:Label ID="Label2" runat="server" Text='<%#(((RepeaterItem)Container).ItemIndex+1).ToString() %>' />
                                            </td>                                            
                                            <td>
                                                <asp:Label ID="LocationName" runat="server" Text='<%# Eval("LocationName") %>' />
                                            </td>                                            
                                            <td>                                              
                                                 <asp:DropDownList ID="ddlmoths" class="form-control months" runat="server">
                                                 <asp:ListItem Text="Jan" Value="1"></asp:ListItem>
                                                 <asp:ListItem Text="Feb" Value="2"></asp:ListItem>
                                                 <asp:ListItem Text="Mar" Value="3"></asp:ListItem>
                                                 <asp:ListItem Text="Apr" Value="4"></asp:ListItem>
                                                 <asp:ListItem Text="May" Value="5"></asp:ListItem>
                                                 <asp:ListItem Text="Jun" Value="6"></asp:ListItem>
                                                 <asp:ListItem Text="Jul" Value="7"></asp:ListItem>
                                                 <asp:ListItem Text="Aug" Value="8"></asp:ListItem>
                                                 <asp:ListItem Text="Sep" Value="9"></asp:ListItem>
                                                 <asp:ListItem Text="Oct" Value="10"></asp:ListItem>
                                                 <asp:ListItem Text="Nov" Value="11"></asp:ListItem>
                                                 <asp:ListItem Text="Dec" Value="12"></asp:ListItem>
                                                 </asp:DropDownList>                      
                                            </td>
                                            <td>
                                            <asp:CheckBox ID="chksave" runat="server"></asp:CheckBox>
                                            </td>
                                            <asp:HiddenField ID="hdnlocationid" Value='<%# Eval("locationid") %>' runat="server" />   
                                           <asp:HiddenField ID="hdnmonthid" Value='<%# Eval("monthid") %>' runat="server" /> 
            </tr>
        </tbody>
    </ItemTemplate>
    <FooterTemplate>
        </table>
    </FooterTemplate>
</asp:Repeater>                               
                                    
                            </div>
                        </div>
                    </div>  
                      </div>
                    </div>
                 </section>

                 <div class="tile-footer color cyan text-muted">
                           <div class="row">
                              <div class="col-md-5">
                                 <div class="col-md-offset-5">
                                    <asp:Button ID="btnsave" class="btn btn-primary" runat="server" Text="Save" 
                                         onclick="btnsave_Click"  />
                                   
                                 </div>
                              </div>
                           </div>
                       </div>
                 </section>
            </div>
        </div>
</asp:Content>
