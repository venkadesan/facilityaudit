﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="Facility_Audit.WebForm1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="flipcss/css/style.css" rel="stylesheet">
</head>
<body>
    <form id="form1" runat="server">
    <div id="wrap">
        <!-- Make page fluid -->
        <div class="row">
            <div class="form-box-content">
                <div class="row border-bottom margin-vertical-15">
                    <div class="col-md-8 col-xs-offset-4">
                        <div id="basicvalidations" class="form-horizontal">
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="">
                                </label>
                                <div class="col-sm-1">
                                </div>
                                <label class="col-sm-1 control-label" for="">
                                    From</label>
                                <div class="col-sm-3">
                                    <asp:TextBox ID="txtfromdate" CssClass="form-control input-datepicker" runat="server"></asp:TextBox>
                                </div>
                                <label class="col-sm-1 control-label margin-left15" for="">
                                    To</label>
                                <div class="col-sm-3">
                                    <asp:TextBox ID="txttodate" CssClass="form-control input-datepicker" runat="server"></asp:TextBox>
                                </div>
                                <asp:Button ID="btnsearch" CssClass="btn btn-primary" runat="server" Text="Go" />
                                <asp:HiddenField ID="hdnmonth" Value="0" runat="server" />
                                <asp:HiddenField ID="hdntype" runat="server" />
                                <asp:HiddenField ID="hdnstatusid" runat="server" />
                                <asp:HiddenField ID="hdnvehicleid" runat="server" />
                                <asp:HiddenField ID="hdndriverid" runat="server" />
                                <asp:HiddenField ID="hdnvehiclecontainer" runat="server" />
                                <asp:Button ID="btnrefresh" runat="server" Text="Button" Style="display: none;" />
                                <asp:Button ID="btnstatus" runat="server" Text="Button" Style="display: none;" />
                                <asp:Button ID="btnvehicleContainer" runat="server" Text="Button" Style="display: none;" />
                                <asp:Button ID="btndriversummary" runat="server" Text="Button" Style="display: none;" />
                                <asp:Button ID="btnidlevehicle" runat="server" Text="Button" Style="display: none;" />
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="row cards">
                    <asp:Repeater ID="rptrsbuwise" DataSourceID="" runat="server" OnItemDataBound="rptrlocationwise_ItemDataBound">
                        <ItemTemplate>
                            <div class="card-container col-lg-3 col-md-6 col-sm-12">
                                <div class="card card-cyan hover">
                                    <div class="front">
                                        <h1>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Eval("sbu") %>' /></h1>
                                        <p  id="sales-count" class="animation-fadeIn360">
                                            <asp:Label ID="score" runat="server" Text='<%# Eval("Score") %>'></asp:Label>
                                            -
                                            <asp:Image ID="imgicon" runat="server" /></p>
                                    </div>
                                    <div class="back nopadding">
                                    </div>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
