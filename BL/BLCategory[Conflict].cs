﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using FacilityAudit.DA;
using DA;

namespace BL
{
  public   class BLCategory
    {

      DACategory objcategory = new DACategory();

      public int saveauditcategory(int auditid, string categoryname, string emailid, int cuid, DateTime cdate)
        {
            return objcategory.saveauditcategory(auditid, categoryname, emailid, cuid, cdate);


        }
      public int updatemauditcategory(int auditid, int categoryid, string categoryname, string emailid,
        int muid, DateTime mdate)
        {
            return objcategory.updatemauditcategory(auditid, categoryid, categoryname, emailid, muid, mdate);

        }
      public int deletemauditcategory(int categoryid)
        {
            return objcategory.deletemauditcategory(categoryid);

        }

      public DataSet getmauditcategory(int auditid, int categoryid, int id)
        {
            DataSet ds = new DataSet();
            ds = objcategory.getmauditcategory(auditid, categoryid,id);
            return ds;
        }
      public DataSet getquestionbyauditid(int auditid, DateTime date, int sectorid)
      {
          DataSet ds = new DataSet();
          ds = objcategory.getquestionbyauditid(auditid, date, sectorid);
          return ds;
      }

      public int addloctionmaildetails(int companyid, int locationid, string regionname, string regionmailid, string statename,
                                        string statemailid, string sbuname, string sbumailid, string omname,
                                        string ommailid, string aomname, string aommailid, string clientname,
                                        string clientmailid, int cuid, DateTime cdate)
      {
          return objcategory.addloctionmaildetails(companyid, locationid, regionname, regionmailid, statename,
                                        statemailid, sbuname, sbumailid, omname,
                                        ommailid, aomname, aommailid, clientname,
                                        clientmailid, cuid, cdate);


      }

      public DataSet getlocationmailid(int id, int companyid, int locationid, int locationmailid)
      {
          DataSet ds = new DataSet();
          ds = objcategory.getlocationmailid(id, companyid, locationid,  locationmailid);
          return ds;
      }

      public int updatemlocationmail(int companyid, int locationid, string regionname, string regionmailid, string statename,
                                        string statemailid, string sbuname, string sbumailid, string omname,
                                        string ommailid, string aomname, string aommailid, string clientname,
                                        string clientmailid, int cuid, DateTime cdate,int locationmailid)
      {
          return objcategory.updatemlocationmail( companyid,  locationid, regionname, regionmailid, statename,
                                        statemailid, sbuname, sbumailid, omname,
                                        ommailid, aomname, aommailid, clientname,
                                        clientmailid,  cuid,  cdate, locationmailid);

      }

    }
}

