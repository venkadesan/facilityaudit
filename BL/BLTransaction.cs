﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using FacilityAudit.DA;
using DA;

namespace BL
{
    public class BLTransaction
    {

        DATransaction objtransaction = new DATransaction();

        public DataSet gettransaction(int companyid, int locationid, int auditid, DateTime auditdate)
        {
            DataSet ds = new DataSet();
            ds = objtransaction.gettransaction(companyid, locationid, auditid, auditdate);
            return ds;
        }

        public DataSet getallauditscore(int regionid, int sbuid, int companyid, int locationid, DateTime frmdate, DateTime todate)
        {
            DataSet ds = new DataSet();
            ds = objtransaction.getallauditscore(regionid, sbuid, companyid, locationid, frmdate, todate);
            return ds;
        }

        public DataSet gettransactionbycategoryid(int companyid, int locationid, int auditid, DateTime auditdate, int categoryid)
        {
            DataSet ds = new DataSet();
            ds = objtransaction.gettransactionbycategoryid(companyid, locationid, auditid, auditdate, categoryid);
            return ds;
        }

        public DataSet gettotalrating(int auditid, int sbuid, int companyid, int locationid, DateTime auditdate)
        {
            DataSet ds = new DataSet();
            ds = objtransaction.gettotalrating(auditid, sbuid, companyid, locationid, auditdate);
            return ds;
        }

        public DataSet getratingsumbycategory(int auditid, int categoryid, int sbuid, int companyid, int locationid, DateTime auditdate)
        {
            DataSet ds = new DataSet();
            ds = objtransaction.getratingsumbycategory(auditid, categoryid, sbuid, companyid, locationid, auditdate);
            return ds;
        }

        public DataSet gettotalweightage(int auditid, int sbuid, int companyid, int locationid, DateTime auditdate)
        {
            DataSet ds = new DataSet();
            ds = objtransaction.gettotalweightage(auditid, sbuid, companyid, locationid, auditdate);
            return ds;
        }

        public DataSet getweightagesumbycategoryid(int auditid, int categoryid, int sbuid, int companyid, int locationid, DateTime auditdate)
        {
            DataSet ds = new DataSet();
            ds = objtransaction.getweightagesumbycategoryid(auditid, categoryid, sbuid, companyid, locationid, auditdate);
            return ds;
        }

        public DataSet gettotalscore(int auditid, int sbuid, int companyid, int locationid, DateTime auditdate)
        {
            DataSet ds = new DataSet();
            ds = objtransaction.gettotalscore(auditid, sbuid, companyid, locationid, auditdate);
            return ds;
        }

        public DataSet getscoresumbycategory(int auditid, int categoryid, int sbuid, int companyid, int locationid, DateTime auditdate)
        {
            DataSet ds = new DataSet();
            ds = objtransaction.getscoresumbycategory(auditid, categoryid, sbuid, companyid, locationid, auditdate);
            return ds;
        }

        public DataSet GetDateWiseReport(int locationid, DateTime dtfdate, DateTime dttdate)
        {
            DataSet ds = new DataSet();
            ds = objtransaction.GetDateWiseReport(locationid, dtfdate, dttdate);
            return ds;
        }

        public DataSet GetClientFeedbackDetails(DateTime dtfdate, DateTime dttdate)
        {
            DataSet ds = new DataSet();
            ds = objtransaction.GetClientFeedbackDetails(dtfdate, dttdate);
            return ds;
        }

        public DataSet GetClientFeedbackDetails_Group(DateTime dtfdate, DateTime dttdate, int groupid)
        {
            DataSet ds = new DataSet();
            ds = objtransaction.GetClientFeedbackDetails_Group(dtfdate, dttdate,groupid);
            return ds;
        }

        public DataSet GetMailTrackingDetails(DateTime dtfdate, DateTime dttdate)
        {
            DataSet ds = new DataSet();
            ds = objtransaction.GetMailTrackingDetails(dtfdate, dttdate);
            return ds;
        }

        public DataSet GetRawDatasDateWiseReport(DateTime auditdate, int locationid, int sbuid, int sectorid, string tower)
        {
            DataSet ds = new DataSet();
            ds = objtransaction.GetRawDatasDateWiseReport(auditdate, locationid, sbuid, sectorid, tower);
            return ds;
        }

        public DataSet GetSendMailDetialsDateWiseReport(DateTime auditdate, int locationid, int sbuid, int sectorid)
        {
            DataSet ds = new DataSet();
            ds = objtransaction.GetSendMailDetialsDateWiseReport(auditdate, locationid, sbuid, sectorid);
            return ds;
        }

        public DataSet getlocationmailid(int companyid, int locationid, bool training, bool hr, bool scm, bool compliance, bool operations)
        {
            DataSet ds = new DataSet();
            ds = objtransaction.getlocationmailid(companyid, locationid, training, hr, scm, compliance, operations);
            return ds;
        }

        #region "Report - 1"

        public DataSet GetRegionWiseScore(int month, int year,int groupid)
        {
            DataSet ds = new DataSet();
            ds = objtransaction.GetRegionWiseScore(month, year, groupid);
            return ds;
        }

        public DataSet GetSbuWiseScore(int month, int year, int regionid, int groupid)
        {
            DataSet ds = new DataSet();
            ds = objtransaction.GetSbuWiseScore(month, year, regionid, groupid);
            return ds;
        }

        public DataSet GetLocationwiseScore(int sbuid, int month, int year, int groupid)
        {
            DataSet ds = new DataSet();
            ds = objtransaction.GetLocationwiseScore(sbuid, month, year, groupid);
            return ds;
        }

        public DataSet GetDateWiseReportByLocation(int locationid, int month, int year)
        {
            DataSet ds = new DataSet();
            ds = objtransaction.GetDateWiseReportByLocation(locationid, month, year);
            return ds;
        }

        #endregion

        #region "Report - 2"

        public DataSet GetRegionWiseScoreReport2(int month, int year, int regionid)
        {
            DataSet ds = new DataSet();
            ds = objtransaction.GetRegionWiseScoreReport2(month, year, regionid);
            return ds;
        }

        public DataSet GetCategoryWiseScoreRegionReport2(int month, int year, int regionid, int AuditId)
        {
            DataSet ds = new DataSet();
            ds = objtransaction.GetCategoryWiseScoreRegionReport2(month, year, regionid, AuditId);
            return ds;
        }


        public DataSet GetAuditWiseScoreReport(int sbuid, int month, int year)
        {
            DataSet ds = new DataSet();
            ds = objtransaction.GetAuditWiseScoreReport(sbuid, month, year);
            return ds;
        }

        public DataSet GetCategoryWiseScoreReport(int sbuid, int month, int year, int auditid)
        {
            DataSet ds = new DataSet();
            ds = objtransaction.GetCategoryWiseScoreReport(sbuid, month, year, auditid);
            return ds;
        }

        #endregion

        #region "report -1 Image list"

        public DataSet GetImageListByTransactionId(int transactionid)
        {
            DataSet ds = new DataSet();
            ds = objtransaction.GetImageListByTransactionId(transactionid);
            return ds;
        }

        #endregion

        #region "With GroupID"

        public DataSet GetDateWiseReport_Group(int locationid, DateTime dtfdate, DateTime dttdate, int groupid)
        {
            DataSet ds = new DataSet();
            ds = objtransaction.GetDateWiseReport_Group(locationid, dtfdate, dttdate, groupid);
            return ds;
        }

        public DataSet GetMailTrackingDetails_Group(DateTime dtfdate, DateTime dttdate, int groupid)
        {
            DataSet ds = new DataSet();
            ds = objtransaction.GetMailTrackingDetails_Group(dtfdate, dttdate, groupid);
            return ds;
        }

        public DataSet GenerateTraingNdevelopment_Report_Group(DateTime dtfdate, DateTime dttdate, int groupid)
        {
            DataSet ds = new DataSet();
            ds = objtransaction.GenerateTraingNdevelopment_Report_Group(dtfdate, dttdate, groupid);
            return ds;
        }

        #endregion

        

    }
}
