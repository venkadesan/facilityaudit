﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;
using System.Globalization;

namespace DA
{
    public class DAContacts
    {
        public DataSet GetRegionContacts(int RegionId)
        {

            DataSet ds = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("ConnectionString");
            DbCommand cmd = db.GetStoredProcCommand("Qry_GetRegionWiseContacts");
            db.AddInParameter(cmd, "RegionID", DbType.Int32, RegionId);
            ds = db.ExecuteDataSet(cmd);
            return ds;
        }

        public string[] InsertUpdateRegionContacts(int Regionid, string TrainingMail, string PayrollMail,
            string HRMail, string SCMMail, string ComplianceMail, string QAMail, string OperationsMail, string PanIndiaMails, int userid)
        {
            try
            {
                string[] Values = new string[3];
                Database db = DatabaseFactory.CreateDatabase("ConnectionString");
                DbCommand cmd = db.GetStoredProcCommand("SP_RegionWiseContacts");
                db.AddInParameter(cmd, "@RegionID", DbType.Int32, Regionid);
                db.AddInParameter(cmd, "@TrainingMail", DbType.String, TrainingMail);
                db.AddInParameter(cmd, "@PayrollMail", DbType.String, PayrollMail);
                db.AddInParameter(cmd, "@HRMail", DbType.String, HRMail);
                db.AddInParameter(cmd, "@SCMMail", DbType.String, SCMMail);
                db.AddInParameter(cmd, "@ComplianceMail", DbType.String, ComplianceMail);
                db.AddInParameter(cmd, "@QAMail", DbType.String, QAMail);
                db.AddInParameter(cmd, "@OperationsMail", DbType.String, OperationsMail);
                db.AddInParameter(cmd, "@PanIndiaMails", DbType.String, PanIndiaMails);
                db.AddInParameter(cmd, "@Uid", DbType.Int32, userid);
                db.AddOutParameter(cmd, "@ErrorMessage", DbType.String, 1000);
                db.AddOutParameter(cmd, "@Status", DbType.String, 10);
                db.ExecuteNonQuery(cmd);
                Values[0] = string.Format(CultureInfo.CurrentCulture, "{0}",
                db.GetParameterValue(cmd, "@ErrorMessage"));
                Values[1] = db.GetParameterValue(cmd, "@Status").ToString();
                return Values;
            }
            catch (Exception)
            {
                throw;
            }
        }

        #region "Sbu Contacts"

        public DataSet GetSbuContacts(int RegionId)
        {

            DataSet ds = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("ConnectionString");
            DbCommand cmd = db.GetStoredProcCommand("Qry_GetSbuContactDet");
            db.AddInParameter(cmd, "RegionID", DbType.Int32, RegionId);
            ds = db.ExecuteDataSet(cmd);
            return ds;
        }



        public string[] InsertUpdateSbuContacts(int Regionid, string ContactXML)
        {
            try
            {
                string[] Values = new string[3];
                Database db = DatabaseFactory.CreateDatabase("ConnectionString");
                DbCommand cmd = db.GetStoredProcCommand("SP_AddSbuContacts");
                db.AddInParameter(cmd, "@RegionID", DbType.Int32, Regionid);
                db.AddInParameter(cmd, "@SbuDetXML", DbType.String, ContactXML);
                db.AddOutParameter(cmd, "@ErrorMessage", DbType.String, 1000);
                db.AddOutParameter(cmd, "@Status", DbType.String, 10);
                db.ExecuteNonQuery(cmd);
                Values[0] = string.Format(CultureInfo.CurrentCulture, "{0}",
                db.GetParameterValue(cmd, "@ErrorMessage"));
                Values[1] = db.GetParameterValue(cmd, "@Status").ToString();
                return Values;
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region "With Group"
        public DataSet GetRegionContacts_Group(int RegionId, int Groupid)
        {

            DataSet ds = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("ConnectionString");
            DbCommand cmd = db.GetStoredProcCommand("Qry_GetRegionWiseContacts_Group");
            db.AddInParameter(cmd, "RegionID", DbType.Int32, RegionId);
            db.AddInParameter(cmd, "Groupid", DbType.Int32, Groupid);
            ds = db.ExecuteDataSet(cmd);
            return ds;
        }


        public string[] InsertUpdateRegionContacts_Group(int Regionid, string TrainingMail, string PayrollMail,
            string HRMail, string SCMMail, string ComplianceMail, string QAMail, string OperationsMail, string PanIndiaMails, int userid,
            int groupid)
        {
            try
            {
                string[] Values = new string[3];
                Database db = DatabaseFactory.CreateDatabase("ConnectionString");
                DbCommand cmd = db.GetStoredProcCommand("SP_RegionWiseContacts_Group");
                db.AddInParameter(cmd, "@RegionID", DbType.Int32, Regionid);
                db.AddInParameter(cmd, "@TrainingMail", DbType.String, TrainingMail);
                db.AddInParameter(cmd, "@PayrollMail", DbType.String, PayrollMail);
                db.AddInParameter(cmd, "@HRMail", DbType.String, HRMail);
                db.AddInParameter(cmd, "@SCMMail", DbType.String, SCMMail);
                db.AddInParameter(cmd, "@ComplianceMail", DbType.String, ComplianceMail);
                db.AddInParameter(cmd, "@QAMail", DbType.String, QAMail);
                db.AddInParameter(cmd, "@OperationsMail", DbType.String, OperationsMail);
                db.AddInParameter(cmd, "@PanIndiaMails", DbType.String, PanIndiaMails);
                db.AddInParameter(cmd, "@Uid", DbType.Int32, userid);
                db.AddInParameter(cmd, "@GroupId", DbType.Int32, groupid);
                db.AddOutParameter(cmd, "@ErrorMessage", DbType.String, 1000);
                db.AddOutParameter(cmd, "@Status", DbType.String, 10);
                db.ExecuteNonQuery(cmd);
                Values[0] = string.Format(CultureInfo.CurrentCulture, "{0}",
                db.GetParameterValue(cmd, "@ErrorMessage"));
                Values[1] = db.GetParameterValue(cmd, "@Status").ToString();
                return Values;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataSet GetSbuContacts_Group(int RegionId, int groupid)
        {

            DataSet ds = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("ConnectionString");
            DbCommand cmd = db.GetStoredProcCommand("Qry_GetSbuContactDet_Group");
            db.AddInParameter(cmd, "RegionID", DbType.Int32, RegionId);
            db.AddInParameter(cmd, "groupid", DbType.Int32, groupid);
            ds = db.ExecuteDataSet(cmd);
            return ds;
        }

        public string[] InsertUpdateSbuContacts_Group(int Regionid, string ContactXML, int groupid)
        {
            try
            {
                string[] Values = new string[3];
                Database db = DatabaseFactory.CreateDatabase("ConnectionString");
                DbCommand cmd = db.GetStoredProcCommand("SP_AddSbuContacts_Group");
                db.AddInParameter(cmd, "@RegionID", DbType.Int32, Regionid);
                db.AddInParameter(cmd, "@groupid", DbType.Int32, groupid);
                db.AddInParameter(cmd, "@SbuDetXML", DbType.String, ContactXML);
                db.AddOutParameter(cmd, "@ErrorMessage", DbType.String, 1000);
                db.AddOutParameter(cmd, "@Status", DbType.String, 10);
                db.ExecuteNonQuery(cmd);
                Values[0] = string.Format(CultureInfo.CurrentCulture, "{0}",
                db.GetParameterValue(cmd, "@ErrorMessage"));
                Values[1] = db.GetParameterValue(cmd, "@Status").ToString();
                return Values;
            }
            catch (Exception)
            {
                throw;
            }
        }


        #endregion

        #region "Mail templates"

        public DataSet GetMailTemplate(int groupid)
        {

            DataSet ds = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("ConnectionString");
            DbCommand cmd = db.GetStoredProcCommand("Qry_GetMailTemplate");
            db.AddInParameter(cmd, "groupid", DbType.Int32, groupid);
            ds = db.ExecuteDataSet(cmd);
            return ds;
        }

        public string[] InsertMailTemplate(int groupid, string mailsubject, string mailbody, string trainingsubject, string trainingbody, string trainingmailid, int userid)
        {
            try
            {
                string[] Values = new string[3];
                Database db = DatabaseFactory.CreateDatabase("ConnectionString");
                DbCommand cmd = db.GetStoredProcCommand("SP_InsertMailTemplate");
                db.AddInParameter(cmd, "@groupid", DbType.Int32, groupid);
                db.AddInParameter(cmd, "@mailsubject", DbType.String, mailsubject);
                db.AddInParameter(cmd, "@mailbody", DbType.String, mailbody);
                db.AddInParameter(cmd, "@trainingsubject", DbType.String, trainingsubject);
                db.AddInParameter(cmd, "@trainingbody", DbType.String, trainingbody);
                db.AddInParameter(cmd, "@trainingtomail", DbType.String, trainingmailid);
                db.AddInParameter(cmd, "@uid", DbType.Int32, userid);
                db.AddOutParameter(cmd, "@ErrorMessage", DbType.String, 1000);
                db.AddOutParameter(cmd, "@Status", DbType.String, 10);
                db.ExecuteNonQuery(cmd);
                Values[0] = string.Format(CultureInfo.CurrentCulture, "{0}",
                db.GetParameterValue(cmd, "@ErrorMessage"));
                Values[1] = db.GetParameterValue(cmd, "@Status").ToString();
                return Values;
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion


    }
}
