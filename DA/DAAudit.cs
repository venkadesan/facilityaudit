﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DA
{
   public class DAAudit
    {
       public int saveaudit(int sectorid, string auditname, string auditdescription, DateTime startdate, DateTime enddate,
   int cuid, DateTime cdate)
       {
           Database db = DatabaseFactory.CreateDatabase("ConnectionString");
           DbCommand cmd = db.GetStoredProcCommand("addaudit");
           db.AddInParameter(cmd, "sectorid", DbType.Int32, sectorid);
           db.AddInParameter(cmd, "auditname", DbType.String, auditname);
           db.AddInParameter(cmd, "auditdescription", DbType.String, auditdescription);
           db.AddInParameter(cmd, "startdate", DbType.DateTime, startdate);
           db.AddInParameter(cmd, "enddate", DbType.DateTime, enddate);
           db.AddInParameter(cmd, "cuid", DbType.Int32, cuid);
           db.AddInParameter(cmd, "cdate", DbType.DateTime, cdate);
           return Convert.ToInt32(db.ExecuteScalar(cmd));
       }
        public int updateaudit(int auditid, int sectorid, string auditname,  string auditdescription,
	DateTime startdate, DateTime enddate , int muid, DateTime mdate)
        {
            Database db = DatabaseFactory.CreateDatabase("ConnectionString");
            DbCommand cmd = db.GetStoredProcCommand("updatemaudit");
            db.AddInParameter(cmd, "auditid", DbType.Int32, auditid);
            db.AddInParameter(cmd, "sectorid", DbType.Int32, sectorid);
            db.AddInParameter(cmd, "auditname", DbType.String, auditname);
            db.AddInParameter(cmd, "auditdescription", DbType.String, auditdescription);
            db.AddInParameter(cmd, "startdate", DbType.DateTime, startdate);
            db.AddInParameter(cmd, "enddate", DbType.DateTime, enddate);
            db.AddInParameter(cmd, "muid", DbType.Int32, muid);
            db.AddInParameter(cmd, "mdate", DbType.DateTime, mdate);
            return Convert.ToInt32(db.ExecuteScalar(cmd));
        }
        public int deletemaudit(int auditid)
        {
            Database db = DatabaseFactory.CreateDatabase("ConnectionString");
            DbCommand cmd = db.GetStoredProcCommand("deletemaudit");
            db.AddInParameter(cmd, "auditid", DbType.Int32, auditid);
            return Convert.ToInt32(db.ExecuteScalar(cmd));
        }

        public DataSet getmaudit( int auditid, int id,int sectorid)
        {
            DataSet ds = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("ConnectionString");
            DbCommand cmd = db.GetStoredProcCommand("getmaudit");
            db.AddInParameter(cmd, "auditid", DbType.Int32, auditid);
            db.AddInParameter(cmd, "id", DbType.Int32, id);
            db.AddInParameter(cmd, "sectorid", DbType.Int32, sectorid);
            ds = db.ExecuteDataSet(cmd);
            return ds;
        }

        public DataSet getscorefordashbaord(int regionid, int sbuid, int companyid,int locationid, DateTime frmdate, DateTime todate)
        {
            DataSet ds = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("ConnectionString");
            DbCommand cmd = db.GetStoredProcCommand("getscorefordashbaord");
            db.AddInParameter(cmd, "regionid", DbType.Int32, regionid);
            db.AddInParameter(cmd, "sbuid", DbType.Int32, sbuid);
            db.AddInParameter(cmd, "companyid", DbType.Int32, companyid);
            db.AddInParameter(cmd, "locationid", DbType.Int32, locationid);
            db.AddInParameter(cmd, "frmdate", DbType.DateTime, frmdate);
            db.AddInParameter(cmd, "todate", DbType.DateTime, todate);
            ds = db.ExecuteDataSet(cmd);
            return ds;
        }

        public DataSet getdashscorebyaudit(int regionid, int sbuid, int companyid, int locationid, DateTime frmdate, DateTime todate)
        {
            DataSet ds = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("ConnectionString");
            DbCommand cmd = db.GetStoredProcCommand("getdashscorebyaudit");
            db.AddInParameter(cmd, "regionid", DbType.Int32, regionid);
            db.AddInParameter(cmd, "sbuid", DbType.Int32, sbuid);
            db.AddInParameter(cmd, "companyid", DbType.Int32, companyid);
            db.AddInParameter(cmd, "locationid", DbType.Int32, locationid);
            db.AddInParameter(cmd, "frmdate", DbType.DateTime, frmdate);
            db.AddInParameter(cmd, "todate", DbType.DateTime, todate);
            ds = db.ExecuteDataSet(cmd);
            return ds;
        }

        public DataSet getdashscorebycategory(int regionid, int sbuid, int companyid, int locationid, DateTime frmdate, DateTime todate, int auditid)
        {
            DataSet ds = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("ConnectionString");
            DbCommand cmd = db.GetStoredProcCommand("getdashscorebycategory");
            db.AddInParameter(cmd, "regionid", DbType.Int32, regionid);
            db.AddInParameter(cmd, "sbuid", DbType.Int32, sbuid);
            db.AddInParameter(cmd, "companyid", DbType.Int32, companyid);
            db.AddInParameter(cmd, "locationid", DbType.Int32, locationid);
            db.AddInParameter(cmd, "frmdate", DbType.DateTime, frmdate);
            db.AddInParameter(cmd, "todate", DbType.DateTime, todate);
            db.AddInParameter(cmd, "auditid", DbType.Int32, auditid);
            ds = db.ExecuteDataSet(cmd);
            return ds;
        }



        public DataSet gettrendreport1(int regionid, int sbuid, string incharge, string auditorname, int sectorid, DateTime frmdate, DateTime todate, int customerid, int locationid)
        {
            DataSet ds = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("ConnectionString");
            DbCommand cmd = db.GetStoredProcCommand("gettrendreport1");
            db.AddInParameter(cmd, "regionid", DbType.Int32, regionid);
            db.AddInParameter(cmd, "sbuid", DbType.Int32, sbuid);
            db.AddInParameter(cmd, "incharge", DbType.String, incharge);
            db.AddInParameter(cmd, "auditorname", DbType.String, auditorname);
            db.AddInParameter(cmd, "sectorid", DbType.Int32, sectorid);
            db.AddInParameter(cmd, "frmdate", DbType.DateTime, frmdate);
            db.AddInParameter(cmd, "todate", DbType.DateTime, todate);
            db.AddInParameter(cmd, "customerid", DbType.Int32, customerid);
            db.AddInParameter(cmd, "locationid", DbType.Int32, locationid);
            ds = db.ExecuteDataSet(cmd);
            return ds;
        }
        public DataSet gettrendreport2(int regionid, int sbuid, string incharge, string auditorname, int sectorid, DateTime fromdate, DateTime Todate,int customerid,int locationid,int categoryid)
        {
            DataSet ds = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("ConnectionString");
            DbCommand cmd = db.GetStoredProcCommand("gettrendreport2");
            db.AddInParameter(cmd, "regionid", DbType.Int32, regionid);
            db.AddInParameter(cmd, "sbuid", DbType.Int32, sbuid);
            db.AddInParameter(cmd, "incharge", DbType.String, incharge);
            db.AddInParameter(cmd, "auditorname", DbType.String, auditorname);
            db.AddInParameter(cmd, "sectorid", DbType.Int32, sectorid);
            db.AddInParameter(cmd, "FromDate", DbType.DateTime, fromdate);
            db.AddInParameter(cmd, "ToDate", DbType.DateTime, Todate);
            db.AddInParameter(cmd, "customerid", DbType.Int32, customerid);
            db.AddInParameter(cmd, "locationid", DbType.Int32, locationid);
            db.AddInParameter(cmd, "Categoryid", DbType.Int32, categoryid);
            ds = db.ExecuteDataSet(cmd);
            return ds;
        }

        #region "Reports Group"

        public DataSet gettrendreport1_Group(int regionid, int sbuid, string incharge, string auditorname, int sectorid, DateTime frmdate, DateTime todate, int customerid, int locationid, int groupid)
        {
            DataSet ds = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("ConnectionString");
            DbCommand cmd = db.GetStoredProcCommand("gettrendreport1_Group");
            db.AddInParameter(cmd, "regionid", DbType.Int32, regionid);
            db.AddInParameter(cmd, "sbuid", DbType.Int32, sbuid);
            db.AddInParameter(cmd, "incharge", DbType.String, incharge);
            db.AddInParameter(cmd, "auditorname", DbType.String, auditorname);
            db.AddInParameter(cmd, "sectorid", DbType.Int32, sectorid);
            db.AddInParameter(cmd, "frmdate", DbType.DateTime, frmdate);
            db.AddInParameter(cmd, "todate", DbType.DateTime, todate);
            db.AddInParameter(cmd, "customerid", DbType.Int32, customerid);
            db.AddInParameter(cmd, "locationid", DbType.Int32, locationid);
            db.AddInParameter(cmd, "groupid", DbType.Int32, groupid);
            ds = db.ExecuteDataSet(cmd);
            return ds;
        }
        public DataSet gettrendreport2_Group(int regionid, int sbuid, string incharge, string auditorname, int sectorid, DateTime fromdate, DateTime Todate, int customerid, int locationid, int categoryid, int groupid)
        {
            DataSet ds = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("ConnectionString");
            DbCommand cmd = db.GetStoredProcCommand("gettrendreport2_Group");
            db.AddInParameter(cmd, "regionid", DbType.Int32, regionid);
            db.AddInParameter(cmd, "sbuid", DbType.Int32, sbuid);
            db.AddInParameter(cmd, "incharge", DbType.String, incharge);
            db.AddInParameter(cmd, "auditorname", DbType.String, auditorname);
            db.AddInParameter(cmd, "sectorid", DbType.Int32, sectorid);
            db.AddInParameter(cmd, "FromDate", DbType.DateTime, fromdate);
            db.AddInParameter(cmd, "ToDate", DbType.DateTime, Todate);
            db.AddInParameter(cmd, "customerid", DbType.Int32, customerid);
            db.AddInParameter(cmd, "locationid", DbType.Int32, locationid);
            db.AddInParameter(cmd, "Categoryid", DbType.Int32, categoryid);
            db.AddInParameter(cmd, "groupid", DbType.Int32, groupid);
            ds = db.ExecuteDataSet(cmd);
            return ds;
        }

        #endregion

    }
}
