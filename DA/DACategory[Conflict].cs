﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DA
{
   public  class DACategory
    {
    

        public int saveauditcategory(int auditid, string categoryname, string emailid, int cuid, DateTime cdate)
        {
            Database db = DatabaseFactory.CreateDatabase("ConnectionString");
            DbCommand cmd = db.GetStoredProcCommand("addauditcategory");
            db.AddInParameter(cmd, "auditid", DbType.Int32, auditid);
            db.AddInParameter(cmd, "categoryname", DbType.String, categoryname);
            db.AddInParameter(cmd, "emailid", DbType.String, emailid);
            db.AddInParameter(cmd, "cuid", DbType.Int32, cuid);
            db.AddInParameter(cmd, "cdate", DbType.DateTime, cdate);
            return Convert.ToInt32(db.ExecuteScalar(cmd));
        }
       	

        public int updatemauditcategory(int auditid, int categoryid, string categoryname, string emailid,
        int muid, DateTime mdate)
        {
            Database db = DatabaseFactory.CreateDatabase("ConnectionString");
            DbCommand cmd = db.GetStoredProcCommand("updatemauditcategory");
            db.AddInParameter(cmd, "auditid", DbType.Int32, auditid);
            db.AddInParameter(cmd, "categoryid", DbType.Int32, categoryid);
            db.AddInParameter(cmd, "categoryname", DbType.String, categoryname);
            db.AddInParameter(cmd, "emailid", DbType.String, emailid);
            db.AddInParameter(cmd, "muid", DbType.Int32, muid);
            db.AddInParameter(cmd, "mdate", DbType.DateTime, mdate);
            return Convert.ToInt32(db.ExecuteScalar(cmd));
        }
        public int deletemauditcategory(int categoryid)
        {
            Database db = DatabaseFactory.CreateDatabase("ConnectionString");
            DbCommand cmd = db.GetStoredProcCommand("deletemauditcategory");
            db.AddInParameter(cmd, "categoryid", DbType.Int32, categoryid);
            return Convert.ToInt32(db.ExecuteScalar(cmd));
        }

        public DataSet getmauditcategory(int auditid, int categoryid , int id)
        {
           
            DataSet ds = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("ConnectionString");
            DbCommand cmd = db.GetStoredProcCommand("getmauditcategory");
            db.AddInParameter(cmd, "auditid", DbType.Int32, auditid);
            db.AddInParameter(cmd, "categoryid", DbType.Int32, categoryid);
            db.AddInParameter(cmd, "id", DbType.Int32, id);
            ds = db.ExecuteDataSet(cmd);
            return ds;
        }

        public DataSet getquestionbyauditid(int auditid, DateTime date, int sectorid)
        {

            DataSet ds = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("ConnectionString");
            DbCommand cmd = db.GetStoredProcCommand("getquestionbyauditid");
            db.AddInParameter(cmd, "auditid", DbType.Int32, auditid);
            db.AddInParameter(cmd, "date", DbType.DateTime, date);
            db.AddInParameter(cmd, "sectorid", DbType.Int32, sectorid);
            ds = db.ExecuteDataSet(cmd);
            return ds;
        }

        public int addloctionmaildetails(int companyid, int locationid, string regionname, string regionmailid, string statename,
                                        string statemailid, string sbuname, string sbumailid, string omname,
                                        string ommailid, string aomname, string aommailid, string clientname,
                                        string clientmailid, int cuid, DateTime cdate)
        {
            Database db = DatabaseFactory.CreateDatabase("ConnectionString");
            DbCommand cmd = db.GetStoredProcCommand("addloctionmaildetails");
            db.AddInParameter(cmd, "companyid", DbType.Int32, companyid);
            db.AddInParameter(cmd, "locationid", DbType.Int32, locationid);
            db.AddInParameter(cmd, "regionname", DbType.String, regionname);
            db.AddInParameter(cmd, "regionmailid", DbType.String, regionmailid);
            db.AddInParameter(cmd, "statename", DbType.String, statename);
            db.AddInParameter(cmd, "statemailid", DbType.String, statemailid);
            db.AddInParameter(cmd, "sbuname", DbType.String, sbuname);
            db.AddInParameter(cmd, "sbumailid", DbType.String, sbumailid);
            db.AddInParameter(cmd, "omname", DbType.String, omname);
            db.AddInParameter(cmd, "ommailid", DbType.String, ommailid);
            db.AddInParameter(cmd, "aomname", DbType.String, aomname);
            db.AddInParameter(cmd, "aommailid", DbType.String, aommailid);
            db.AddInParameter(cmd, "clientname", DbType.String, clientname);
            db.AddInParameter(cmd, "clientmailid", DbType.String, clientmailid);
            db.AddInParameter(cmd, "cuid", DbType.Int32, cuid);
            db.AddInParameter(cmd, "cdate", DbType.DateTime, cdate);
            return Convert.ToInt32(db.ExecuteScalar(cmd));
        }

        public DataSet getlocationmailid(int id, int  companyid, int locationid, int  locationmailid)
        {

            DataSet ds = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("ConnectionString");
            DbCommand cmd = db.GetStoredProcCommand("getlocationmailid");
            db.AddInParameter(cmd, "id", DbType.Int32, id);
            db.AddInParameter(cmd, "companyid", DbType.Int32, companyid);
            db.AddInParameter(cmd, "locationid", DbType.Int32, locationid);
            db.AddInParameter(cmd, "locationmailid", DbType.Int32, locationmailid);
        
            ds = db.ExecuteDataSet(cmd);
            return ds;
        }

        public int updatemlocationmail(int companyid, int locationid, string regionname, string regionmailid, string statename,
                                        string statemailid, string sbuname, string sbumailid, string omname,
                                        string ommailid, string aomname, string aommailid, string clientname,
                                        string clientmailid, int cuid, DateTime cdate,int locationmailid)
        {
            Database db = DatabaseFactory.CreateDatabase("ConnectionString");
            DbCommand cmd = db.GetStoredProcCommand("updatemlocationmail");
            db.AddInParameter(cmd, "companyid", DbType.Int32, companyid);
            db.AddInParameter(cmd, "locationid", DbType.Int32, locationid);
            db.AddInParameter(cmd, "regionname", DbType.String, regionname);
            db.AddInParameter(cmd, "regionmailid", DbType.String, regionmailid);
            db.AddInParameter(cmd, "statename", DbType.String, statename);
            db.AddInParameter(cmd, "statemailid", DbType.String, statemailid);
            db.AddInParameter(cmd, "sbuname", DbType.String, sbuname);
            db.AddInParameter(cmd, "sbumailid", DbType.String, sbumailid);
            db.AddInParameter(cmd, "omname", DbType.String, omname);
            db.AddInParameter(cmd, "ommailid", DbType.String, ommailid);
            db.AddInParameter(cmd, "aomname", DbType.String, aomname);
            db.AddInParameter(cmd, "aommailid", DbType.String, aommailid);
            db.AddInParameter(cmd, "clientname", DbType.String, clientname);
            db.AddInParameter(cmd, "clientmailid", DbType.String, clientmailid);
            db.AddInParameter(cmd, "cuid", DbType.Int32, cuid);
            db.AddInParameter(cmd, "cdate", DbType.DateTime, cdate);
            db.AddInParameter(cmd, "locationmailid", DbType.Int32, locationmailid);
            return Convert.ToInt32(db.ExecuteScalar(cmd));
   }


    }
}
